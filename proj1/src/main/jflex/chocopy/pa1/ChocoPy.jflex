package chocopy.pa1;
import java_cup.runtime.*;
import java.util.ArrayDeque;
import java.math.BigInteger;
import java.io.*;
%%

/*** Do not change the flags below unless you know what you are doing. ***/

%unicode
%line
%column

%class ChocoPyLexer
%public

%cupsym ChocoPyTokens
%cup
%cupdebug

%eofclose false

/*** Do not change the flags above unless you know what you are doing. ***/

%init{
indentionStack.push(0);
%init}

/* The following code section is copied verbatim to the
 * generated lexer class. */
%{
    /* The code below includes some convenience methods to create tokens
     * of a given type and optionally a value that the CUP parser can
     * understand. Specifically, a lot of the logic below deals with
     * embedded information about where in the source code a given token
     * was recognized, so that the parser can report errors accurately.
     * (It need not be modified for this project.) */

    /** Producer of token-related values for the parser. */
    final ComplexSymbolFactory symbolFactory = new ComplexSymbolFactory();

    /** An integer Stack for keeping track of indention Level, as described in 3.1.5
    of ChocoPy reference manual. Lexer emits INDENT and push if current value
    higher is lower and DEDENT if not and pops all that aren't. See manual. */
    ArrayDeque<Integer> indentionStack = new ArrayDeque();

    /** Return a terminal symbol of syntactic category TYPE and no
     *  semantic value at the current source location. */
    private Symbol symbol(int type) {
        return symbol(type, yytext());
    }

    /** Return a terminal symbol of syntactic category TYPE and semantic
     *  value VALUE at the current source location. */
    private Symbol symbol(int type, Object value) {
        return symbolFactory.newSymbol(ChocoPyTokens.terminalNames[type], type,
            new ComplexSymbolFactory.Location(yyline + 1, yycolumn + 1),
            new ComplexSymbolFactory.Location(yyline + 1,yycolumn + yylength()),
            value);
    }
    private static int calculateIndention(String whitespace) {
        int space_count = 0;
        for (char c: whitespace.toCharArray()) {
            switch(c) {
                case ' ':
                    space_count++;
                    break;
                case '\t':
                    // tab tp space count definition: increment by at least one, and up to 8, such
                    // that up to and including tab is a multiple of 8.
                    space_count++;
                    space_count += 8 - (space_count % 8);
                    break;
                default:
                    throw new RuntimeException("Expected a space or tab, "+
                     "somehow got: \"" + c + "\". Perhaps check the definition of Whitespace?");
            }
        }
        return space_count;
    }

    private Symbol handleString(String raw) {
        StringBuffer text = new StringBuffer();
        boolean is_id = raw.length() > 2 && (Character.isLetter(raw.charAt(1)) || raw.charAt(1) == '_');
        for (int i = 1; i < raw.length()-1; i++) {
           char c = raw.charAt(i);
           is_id = is_id && (Character.isDigit(c) || Character.isLetter(c) || c== '_');
           if (c == '\\' ) {
               switch(raw.charAt(i+1)) {
                   case '\"':
                   case '\\':
                       text.append(raw.charAt(i+1));
                       i++;
                       break;
                   case 'n':
                       text.append('\n');
                       i++;
                       break;
                   case 't':
                       text.append('\t');
                       i++;
                       break;
                   default:
                       return symbol(ChocoPyTokens.UNRECOGNIZED, raw + " <bad escape>");
               }
           } else if (c == '\"') {
               yypushback(raw.length()-1-i);
               break;
           } else {
               text.append(c);
           }
        }
        String value = text.toString();
        return symbol(is_id ? ChocoPyTokens.IDSTRING : ChocoPyTokens.STRING, value);
    }
    private Symbol handleInteger(String raw) {
        BigInteger max = BigInteger.valueOf(2147483647);
        // we may not be able to handle size.
        BigInteger n = new BigInteger(raw);
        if (n.compareTo(max) > 0) {
            return symbol(ChocoPyTokens.UNRECOGNIZED, raw);
        }
        return symbol(ChocoPyTokens.NUMBER, Integer.parseInt(yytext()));
    }
%}

/* Macros (regexes used in rules below) */
WhiteSpace = [ \t]
LineBreak  = \r|\n|\r\n
IntegerLiteral = 0 | [1-9][0-9]*

// can't start with a number
Identifier = [a-zA-Z_][a-zA-Z0-9_]*

// ASCII end of text character, used to alert EOF
EndOfText = "\x03"
q = \"

// regex for matching ASCII chars 32-126 up to the first _"_ that isn't escaped
StringLiteral = {q}([ !#-~]|\\{q})*[ !#-\[\]-~]([ !#-\[\]-~]|\\{q}|\\\\)*{q}|{q}{q}|{q}([ !#-\[\]-~]|\\{q}|\\\\)*{q}

Comment = #~{LineBreak}

/* TrivialLine: Line we don't tokenize due to being whitespace */
TrivialLine =  {WhiteSpace}*({LineBreak}|{Comment})

/* Indention:  Whitespace that occurs on a line that isn't entirely whitespace */
Indention = {WhiteSpace}*

%state LINEREST
%state CLEANUP

%%
/** Initial state of program. We start here, check indention using stack,
   as described in 3.1.5, to decide whether to change indention level. We proceed
   to LINEREST to parse all other tokens in that line before returning here.*/
<YYINITIAL> {
    {TrivialLine} { /* do nothing, just consume the line*/}

    {Indention} {
        // For a whitespace only line, this is matched with a lower priority
        // than Trivial line, as the latter includes newline (so it's longer).
        //  Hence if we are here, the line is not trivial
          String capture_text = yytext();

          int space_count = calculateIndention(capture_text);
          int current_indention = indentionStack.peek();
	  
	  //indent
          if (space_count > current_indention) {
              indentionStack.push(space_count);
              yybegin(LINEREST);
              return symbol(ChocoPyTokens.INDENT);
          }

	  //dedent
          if (space_count < current_indention) {
            indentionStack.pop();
            if (indentionStack.peek() < space_count) {
	    	//inconsistent indentation
                return symbol(ChocoPyTokens.INDENTION_ERROR);
            }
            if (indentionStack.peek() > space_count) {
                yypushback(capture_text.length());
                yybegin(YYINITIAL);
            } else {
                yybegin(LINEREST);
            }
            return symbol(ChocoPyTokens.DEDENT);
          }
          yybegin(LINEREST);
    }
}

/* LINEREST - Handles "the rest of the line". I.e. everything but indentation & 'dedentation' */
<LINEREST> {

  /*  To keep this file clean, as it has logic in it,
      we'll include "boring" tokens in seperate files, sorted to:
      * Keywords  * Binary Operators  * Misc tokens
  */
  %include ChocoPyKeywords.jflex
  %include ChocoPyBinaryOps.jflex
  %include ChocoPyMiscTokens.jflex

  /* Process the rest of the line until we hit a newline. Comments also count their newline.
   We do nothing with whitespace when on a nontrivial line. */
   {LineBreak}|{Comment}       { yybegin(YYINITIAL); return symbol(ChocoPyTokens.NEWLINE); }
   {WhiteSpace}                { /* Welcome to the chill zone*/ }
   {Identifier}                { return symbol(ChocoPyTokens.ID, yytext()); }
   {IntegerLiteral}            { return handleInteger(yytext()); }
   {EndOfText}                 { yybegin(CLEANUP); }
   {StringLiteral}             { return handleString(yytext()); }
  /* Error fallback. */
   [^]                         { return symbol(ChocoPyTokens.UNRECOGNIZED); }
}

//handle the EOF; emit dedents for nonzero elements of the indentation stack
<CLEANUP> {
    "" {
        if (indentionStack.peek() != 0) {
            indentionStack.pop();
            yybegin(CLEANUP);
            return symbol(ChocoPyTokens.DEDENT);
        } else {
            return symbol(ChocoPyTokens.EOF);
        }
    }
}
<<EOF>> { return symbol(ChocoPyTokens.EOF); }
