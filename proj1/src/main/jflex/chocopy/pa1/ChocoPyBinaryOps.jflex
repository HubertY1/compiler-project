/* Binary Operator Definitions used by jflex (for ChocoPy). Used in ChocoPy.jflex,
   kept here for cleanliness
   "is" is in keywords, since it is a reserved "word"
*/
    "+"   { return symbol(ChocoPyTokens.PLUS, "+");   }
    "-"   { return symbol(ChocoPyTokens.MINUS);       }
    "*"   { return symbol(ChocoPyTokens.ASTER);       }
    "//"  { return symbol(ChocoPyTokens.FLOOR_DIV);   }
    "%"   { return symbol(ChocoPyTokens.MODULO);      }
    "<"   { return symbol(ChocoPyTokens.LT);          }
    ">"   { return symbol(ChocoPyTokens.GT);          }
    "<="  { return symbol(ChocoPyTokens.LEQ);         }
    ">="  { return symbol(ChocoPyTokens.GEQ);         }
    "=="  { return symbol(ChocoPyTokens.CMP_EQ);     }
    "!="  { return symbol(ChocoPyTokens.NOT_EQ);      }
    "="   { return symbol(ChocoPyTokens.ASSIGN_EQ);   }