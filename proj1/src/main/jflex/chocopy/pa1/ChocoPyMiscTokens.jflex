// terminal LPAR, RPAR, LBRACKET, RBRACKET;
// terminal COMMA, COLON, PERIOD, GIVES;
    "("   { return symbol(ChocoPyTokens.LPAR);           }
    ")"   { return symbol(ChocoPyTokens.RPAR);           }
    "["   { return symbol(ChocoPyTokens.LBRACKET);       }
    "]"   { return symbol(ChocoPyTokens.RBRACKET);       }
    ","   { return symbol(ChocoPyTokens.COMMA);          }
    ":"   { return symbol(ChocoPyTokens.COLON);          }
    "."   { return symbol(ChocoPyTokens.PERIOD);          }
    "->"   { return symbol(ChocoPyTokens.GIVES);         }
