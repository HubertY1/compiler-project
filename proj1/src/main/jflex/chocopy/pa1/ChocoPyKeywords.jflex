/* ChocoPy Keyword handling definitions for jflex. Used in ChocoPy.jflex,
   kept here for cleanliness
*/
    "except"   { return symbol(ChocoPyTokens.EXCEPT);   }
    "with"     { return symbol(ChocoPyTokens.EXCEPT);   }
    "yield"    { return symbol(ChocoPyTokens.YIELD);    }
    "continue" { return symbol(ChocoPyTokens.CONTINUE); }
    "for"      { return symbol(ChocoPyTokens.FOR);      }
    "assert"   { return symbol(ChocoPyTokens.ASSERT);   }
    "not"      { return symbol(ChocoPyTokens.NOT);      }
    "is"       { return symbol(ChocoPyTokens.IS);       }
    "and"      { return symbol(ChocoPyTokens.AND);      }
    "pass"     { return symbol(ChocoPyTokens.PASS);     }
    "in"       { return symbol(ChocoPyTokens.IN);       }
    "or"       { return symbol(ChocoPyTokens.OR);       }
    "class"    { return symbol(ChocoPyTokens.CLASS);    }
    "nonlocal" { return symbol(ChocoPyTokens.NONLOCAL); }
    "if"       { return symbol(ChocoPyTokens.IF);       }
    "return"   { return symbol(ChocoPyTokens.RETURN);   }
    "True"     { return symbol(ChocoPyTokens.TRUE);     }
    "lambda"   { return symbol(ChocoPyTokens.LAMBDA);   }
    "try"      { return symbol(ChocoPyTokens.TRY);      }
    "global"   { return symbol(ChocoPyTokens.GLOBAL);   }
    "break"    { return symbol(ChocoPyTokens.BREAK);    }
    "finally"  { return symbol(ChocoPyTokens.FINALLY);  }
    "from"     { return symbol(ChocoPyTokens.FROM);     }
    "import"   { return symbol(ChocoPyTokens.IMPORT);   }
    "None"     { return symbol(ChocoPyTokens.NONE);     }
    "await"    { return symbol(ChocoPyTokens.AWAIT);    }
    "else"     { return symbol(ChocoPyTokens.ELSE);     }
    "while"    { return symbol(ChocoPyTokens.WHILE);    }
    "del"      { return symbol(ChocoPyTokens.DEL);      }
    "async"    { return symbol(ChocoPyTokens.ASYNC);    }
    "def"      { return symbol(ChocoPyTokens.DEF);      }
    "False"    { return symbol(ChocoPyTokens.FALSE);    }
    "as"       { return symbol(ChocoPyTokens.AS);       }
    "elif"     { return symbol(ChocoPyTokens.ELIF);     }
    "raise"    { return symbol(ChocoPyTokens.RAISE);    }