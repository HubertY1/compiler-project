package chocopy.common.astnodes;
import chocopy.common.analysis.NodeAnalyzer;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import java_cup.runtime.ComplexSymbolFactory;

public @JsonIgnoreType class PassDec extends Declaration {
    public PassDec(ComplexSymbolFactory.Location left, ComplexSymbolFactory.Location right) {
        super(left, right);
    }
    @Override
    public Identifier getIdentifier() {
        return null;
    }
    public <T> T dispatch(NodeAnalyzer<T> analyzer) {
        return analyzer.defaultAction(this);
    }
}
