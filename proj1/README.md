
# CS 164: Programming Assignment 1

[PA1 Specification]: http://inst.eecs.berkeley.edu/~cs164/fa20/hw/PA1.pdf
[ChocoPy Specification]: http://inst.eecs.berkeley.edu/~cs164/fa20/chocopy_language_reference.pdf

## Getting started

Run the following command to generate and compile your parser, and then run all the provided tests:

    mvn clean package

    java -cp "chocopy-ref.jar:target/assignment.jar" chocopy.ChocoPy --pass=s --test --dir src/test/data/pa1/sample/

In the starter code, only one test should pass. Your objective is to build a parser that passes all the provided tests and meets the assignment specifications.

To manually observe the output of your parser when run on a given input ChocoPy program, run the following command (replace the last argument to change the input file):

    java -cp "chocopy-ref.jar:target/assignment.jar" chocopy.ChocoPy --pass=s src/test/data/pa1/sample/expr_plus.py

You can check the output produced by the staff-provided reference implementation on the same input file, as follows:

    java -cp "chocopy-ref.jar:target/assignment.jar" chocopy.ChocoPy --pass=r src/test/data/pa1/sample/expr_plus.py

Try this with another input file as well, such as `src/test/data/pa1/sample/coverage.py`, to see what happens when the results disagree.

## Assignment specifications

See the [PA1 specification][] on the course
website for a detailed specification of the assignment.

Refer to the [ChocoPy Specification][] on the CS164 web site
for the specification of the ChocoPy language. 

## Receiving updates to this repository

Add the `upstream` repository remotes (you only need to do this once in your local clone):

    git remote add upstream https://github.com/cs164berkeley/pa1-chocopy-parser.git

To sync with updates upstream:

    git pull upstream master


## Submission writeup

Team member 1: Richard Lettich
Team member 2: Matthew Nevling
Team member 3: Hubert Yuan
Team member 4: Thomas Norell

Collaboration or Outside Help: Cup and Flex documentation

Late Hours Consumed: 0

1. **What strategy did you use to emit INDENT and DEDENT tokens correctly? Mention the filename and the line number(s) for the core part of your solution.**
    INDENT and DEDENT tokens are handled in `ChocoPy.jflex` at lines 148 - 176 and 206 - 216.
    We refer a line's leading whitespace (in spaces) as its *indentation*. We use a stack to keep track of the indentation of previous lines. The stack is initialized with the value 0 and is strictly increasing. For each line of input:
    - If the indentation is greater than the value at the top of the stack, the indentation pushed onto the stack and we emit a INDENT token.
    - If the indentation is less than the value at the top of the stack, we recursively pop values off the stack, emitting a DEDENT token for each step until the current indentation exactly equals the value popped off the stack. The leading whitespace must be consistent with a previous indent; if the indentation level popped off the stack is ever less than the current indentation level, we report an indentation error has occurred. 
    - If we reach EOF with a positive indentation, we recursively pop values off the stack until the initial indentation of 0 is reached, emitting a DEDENT token for each value.

2. **What was the hardest language feature (not including indentation) to implement in this assignment? Why was it challenging? Mention the filename and line number(s) for the core part of your solution.**
   The hardest language feature we implemented was strings. Our initial approach was to use the lexer state machine to match the string character by character. However, the state machine grew very complicated and we had trouble correctly processing escape sequences. At first we were reluctant to refactor and attempted to get it working without doing so but this caused a long slowdown where we had a lot of difficulty making progress. We ended up using regex to match the body of a string from the opening quote to an unescaped close quote, and handle escape sequences in Java. This was additionally challenging because we needed to work through the exact characters allowed and the regex grew increasingly complicated. We found and fixed a particular edge case by exhaustively testing against all four-length strings using the characters `"`, `n`, `t`, `\ `, `a`.
   In `ChocoPy.jflex` lines 81 - 112, 128, 201
   In `ChocoPy.cup` lines 380 - 381

