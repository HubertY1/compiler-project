package chocopy.pa3.internalTesting;

import chocopy.pa3.IR.RegisterAllocator.Graph;
import org.junit.Assert;
import org.junit.Test;

import java.util.Random;


public class GraphTest {

    @Test
    public void testGraph() {
        Random seed = new Random(3);
        Graph<String> conn = new Graph<>();
        String[] registers = new String[40];
        for (int i = 0; i < 40; i++) {
            registers[i] = "x" + i;
        }
        for (String reg : registers) {
            int randAttach = seed.nextInt(40);
            conn.connect(reg, registers[randAttach]);
            Assert.assertTrue(conn.are_connected(reg, registers[randAttach]));
        }
//        conn.adjList.forEach((key, Node) ->{
//            Assert
//        });
    }
}
