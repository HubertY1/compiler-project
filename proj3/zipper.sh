[[ -d lib ]] || mkdir lib
cd lib
[[ -d META-INF ]] || mkdir META-INF
echo "Main-Class: chocopy.ChocoPy" > META-INF/MANIFEST.MF 
[[ -d temp ]] || mkdir temp
cd temp
# if built file is older than the script, rebuild zipped script
if [[ ../../target/assignment.jar -nt ../../$0 ]]; then
  jar -xf ../../target/assignment.jar
  jar -xf ../../chocopy-ref.jar
  jar -cmf ../META-INF/MANIFEST.MF ../zipped_jar.jar .
  touch "$0"
  #update age of script
fi
