name=$1
astName="${name}.ast"
astTypedName="${name}.ast.typed"
astTypedSName="${name}.ast.typed.s"
java -cp 'chocopy-ref.jar:target/assignment.jar' chocopy.ChocoPy --pass=r $1 --out $astName
java -cp 'chocopy-ref.jar:target/assignment.jar' chocopy.ChocoPy --pass=rr $1 --out $astTypedName
java -cp 'chocopy-ref.jar:target/assignment.jar' chocopy.ChocoPy --pass=rrr $1 --out $astTypedSName
cp $name src/test/data/pa3/student_contributed
mv $astName src/test/data/pa3/student_contributed
mv $astTypedName src/test/data/pa3/student_contributed
mv $astTypedSName src/test/data/pa3/student_contributed
