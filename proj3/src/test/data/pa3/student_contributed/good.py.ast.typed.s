  .equiv @sbrk, 9
  .equiv @print_string, 4
  .equiv @print_char, 11
  .equiv @print_int, 1
  .equiv @exit2, 17
  .equiv @read_string, 8
  .equiv @fill_line_buffer, 18
  .equiv @.__obj_size__, 4
  .equiv @.__len__, 12
  .equiv @.__int__, 12
  .equiv @.__bool__, 12
  .equiv @.__str__, 16
  .equiv @.__elts__, 16
  .equiv @error_div_zero, 2
  .equiv @error_arg, 1
  .equiv @error_oob, 3
  .equiv @error_none, 4
  .equiv @error_oom, 5
  .equiv @error_nyi, 6
  .equiv @listHeaderWords, 4
  .equiv @strHeaderWords, 4
  .equiv @bool.True, const_1
  .equiv @bool.False, const_0

.data

.globl $object$prototype
$object$prototype:
  .word 0                                  # Type tag for class: object
  .word 3                                  # Object size
  .word $object$dispatchTable              # Pointer to dispatch table
  .align 2

.globl $int$prototype
$int$prototype:
  .word 1                                  # Type tag for class: int
  .word 4                                  # Object size
  .word $int$dispatchTable                 # Pointer to dispatch table
  .word 0                                  # Initial value of attribute: __int__
  .align 2

.globl $bool$prototype
$bool$prototype:
  .word 2                                  # Type tag for class: bool
  .word 4                                  # Object size
  .word $bool$dispatchTable                # Pointer to dispatch table
  .word 0                                  # Initial value of attribute: __bool__
  .align 2

.globl $str$prototype
$str$prototype:
  .word 3                                  # Type tag for class: str
  .word 5                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 0                                  # Initial value of attribute: __len__
  .word 0                                  # Initial value of attribute: __str__
  .align 2

.globl $.list$prototype
$.list$prototype:
  .word -1                                 # Type tag for class: .list
  .word 4                                  # Object size
  .word $.list$dispatchTable               # Pointer to dispatch table
  .word 0                                  # Initial value of attribute: __len__
  .align 2

.globl $Dog$prototype
$Dog$prototype:
  .word 4                                  # Type tag for class: Dog
  .word 3                                  # Object size
  .word $Dog$dispatchTable                 # Pointer to dispatch table
  .align 2

.globl $A$prototype
$A$prototype:
  .word 5                                  # Type tag for class: A
  .word 3                                  # Object size
  .word $A$dispatchTable                   # Pointer to dispatch table
  .align 2

.globl $B$prototype
$B$prototype:
  .word 6                                  # Type tag for class: B
  .word 3                                  # Object size
  .word $B$dispatchTable                   # Pointer to dispatch table
  .align 2

.globl $C$prototype
$C$prototype:
  .word 7                                  # Type tag for class: C
  .word 3                                  # Object size
  .word $C$dispatchTable                   # Pointer to dispatch table
  .align 2

.globl $object$dispatchTable
$object$dispatchTable:
  .word $object.__init__                   # Implementation for method: object.__init__

.globl $int$dispatchTable
$int$dispatchTable:
  .word $object.__init__                   # Implementation for method: int.__init__

.globl $bool$dispatchTable
$bool$dispatchTable:
  .word $object.__init__                   # Implementation for method: bool.__init__

.globl $str$dispatchTable
$str$dispatchTable:
  .word $object.__init__                   # Implementation for method: str.__init__

.globl $.list$dispatchTable
$.list$dispatchTable:
  .word $object.__init__                   # Implementation for method: .list.__init__

.globl $Dog$dispatchTable
$Dog$dispatchTable:
  .word $object.__init__                   # Implementation for method: Dog.__init__

.globl $A$dispatchTable
$A$dispatchTable:
  .word $object.__init__                   # Implementation for method: A.__init__
  .word $A.simple                          # Implementation for method: A.simple
  .word $A.simple_return                   # Implementation for method: A.simple_return
  .word $A.order_eval                      # Implementation for method: A.order_eval
  .word $A.override                        # Implementation for method: A.override
  .word $A.downCastInt                     # Implementation for method: A.downCastInt
  .word $A.downCastBool                    # Implementation for method: A.downCastBool

.globl $B$dispatchTable
$B$dispatchTable:
  .word $object.__init__                   # Implementation for method: B.__init__
  .word $A.simple                          # Implementation for method: B.simple
  .word $A.simple_return                   # Implementation for method: B.simple_return
  .word $A.order_eval                      # Implementation for method: B.order_eval
  .word $B.override                        # Implementation for method: B.override
  .word $A.downCastInt                     # Implementation for method: B.downCastInt
  .word $A.downCastBool                    # Implementation for method: B.downCastBool

.globl $C$dispatchTable
$C$dispatchTable:
  .word $object.__init__                   # Implementation for method: C.__init__
  .word $A.simple                          # Implementation for method: C.simple
  .word $A.simple_return                   # Implementation for method: C.simple_return
  .word $A.order_eval                      # Implementation for method: C.order_eval
  .word $B.override                        # Implementation for method: C.override
  .word $A.downCastInt                     # Implementation for method: C.downCastInt
  .word $A.downCastBool                    # Implementation for method: C.downCastBool

.globl $i
$i:
  .word 0                                  # Initial value of global var: i

.globl $j
$j:
  .word const_2                            # Initial value of global var: j

.globl $fido
$fido:
  .word 0                                  # Initial value of global var: fido

.globl $next
$next:
  .word 0                                  # Initial value of global var: next

.globl $x
$x:
  .word 1                                  # Initial value of global var: x

.globl $a
$a:
  .word 0                                  # Initial value of global var: a

.globl $b
$b:
  .word 0                                  # Initial value of global var: b

.globl $c
$c:
  .word 0                                  # Initial value of global var: c

.text

.globl main
main:
  lui a0, 8192                             # Initialize heap size (in multiples of 4KB)
  add s11, s11, a0                         # Save heap size
  jal heap.init                            # Call heap.init routine
  mv gp, a0                                # Initialize heap pointer
  mv s10, gp                               # Set beginning of heap
  add s11, s10, s11                        # Set end of heap (= start of heap + heap size)
  mv ra, zero                              # No normal return from main program.
  mv fp, zero                              # No preceding frame.
  addi sp, sp, -@..main.size               # Reserve space for stack frame.
  sw ra, @..main.size-4(sp)                # return address
  sw fp, @..main.size-8(sp)                # control link
  addi fp, sp, @..main.size                # New fp is at old SP.
  jal initchars                            # Initialize one-character strings.
  la a0, $A$prototype                      # Load pointer to prototype of: A
  jal alloc                                # Allocate new object in A0
  sw a0, -12(fp)                           # Push on stack slot 3
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 0(a1)                             # Load address of method: A.__init__
  jalr a1                                  # Invoke method: A.__init__
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, -12(fp)                           # Pop stack slot 3
  sw a0, $a, t0                            # Assign global: a (using tmp register)
  lw a0, $a                                # Load global: a
  bnez a0, label_1                         # Ensure not None
  j error.None                             # Go to error handler
label_1:                                   # Not None
  sw a0, -16(fp)                           # Push argument 0 from last.
  lw a0, -16(fp)                           # Peek stack slot 3
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 4(a1)                             # Load address of method: A.simple
  addi sp, fp, -16                         # Set SP to last argument.
  jalr a1                                  # Invoke method: A.simple
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, $a                                # Load global: a
  bnez a0, label_2                         # Ensure not None
  j error.None                             # Go to error handler
label_2:                                   # Not None
  sw a0, -32(fp)                           # Push argument 0 from last.
  lw a0, -32(fp)                           # Peek stack slot 7
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 8(a1)                             # Load address of method: A.simple_return
  addi sp, fp, -32                         # Set SP to last argument.
  jalr a1                                  # Invoke method: A.simple_return
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  jal makeint                              # Box integer
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, $a                                # Load global: a
  bnez a0, label_3                         # Ensure not None
  j error.None                             # Go to error handler
label_3:                                   # Not None
  sw a0, -16(fp)                           # Push argument 0 from last.
  lw a0, -16(fp)                           # Peek stack slot 3
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 16(a1)                            # Load address of method: A.override
  addi sp, fp, -16                         # Set SP to last argument.
  jalr a1                                  # Invoke method: A.override
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  la a0, $B$prototype                      # Load pointer to prototype of: B
  jal alloc                                # Allocate new object in A0
  sw a0, -12(fp)                           # Push on stack slot 3
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 0(a1)                             # Load address of method: B.__init__
  jalr a1                                  # Invoke method: B.__init__
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, -12(fp)                           # Pop stack slot 3
  sw a0, $b, t0                            # Assign global: b (using tmp register)
  lw a0, $b                                # Load global: b
  bnez a0, label_4                         # Ensure not None
  j error.None                             # Go to error handler
label_4:                                   # Not None
  sw a0, -16(fp)                           # Push argument 0 from last.
  lw a0, -16(fp)                           # Peek stack slot 3
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 16(a1)                            # Load address of method: B.override
  addi sp, fp, -16                         # Set SP to last argument.
  jalr a1                                  # Invoke method: B.override
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, $b                                # Load global: b
  bnez a0, label_5                         # Ensure not None
  j error.None                             # Go to error handler
label_5:                                   # Not None
  sw a0, -16(fp)                           # Push argument 0 from last.
  lw a0, -16(fp)                           # Peek stack slot 3
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 4(a1)                             # Load address of method: B.simple
  addi sp, fp, -16                         # Set SP to last argument.
  jalr a1                                  # Invoke method: B.simple
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, $b                                # Load global: b
  sw a0, $a, t0                            # Assign global: a (using tmp register)
  lw a0, $b                                # Load global: b
  bnez a0, label_6                         # Ensure not None
  j error.None                             # Go to error handler
label_6:                                   # Not None
  sw a0, -16(fp)                           # Push argument 0 from last.
  lw a0, -16(fp)                           # Peek stack slot 3
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 4(a1)                             # Load address of method: B.simple
  addi sp, fp, -16                         # Set SP to last argument.
  jalr a1                                  # Invoke method: B.simple
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, $b                                # Load global: b
  bnez a0, label_7                         # Ensure not None
  j error.None                             # Go to error handler
label_7:                                   # Not None
  sw a0, -16(fp)                           # Push argument 0 from last.
  lw a0, -16(fp)                           # Peek stack slot 3
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 16(a1)                            # Load address of method: B.override
  addi sp, fp, -16                         # Set SP to last argument.
  jalr a1                                  # Invoke method: B.override
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  la a0, $C$prototype                      # Load pointer to prototype of: C
  jal alloc                                # Allocate new object in A0
  sw a0, -12(fp)                           # Push on stack slot 3
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 0(a1)                             # Load address of method: C.__init__
  jalr a1                                  # Invoke method: C.__init__
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, -12(fp)                           # Pop stack slot 3
  sw a0, $c, t0                            # Assign global: c (using tmp register)
  lw a0, $c                                # Load global: c
  bnez a0, label_8                         # Ensure not None
  j error.None                             # Go to error handler
label_8:                                   # Not None
  sw a0, -16(fp)                           # Push argument 0 from last.
  lw a0, -16(fp)                           # Peek stack slot 3
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 16(a1)                            # Load address of method: C.override
  addi sp, fp, -16                         # Set SP to last argument.
  jalr a1                                  # Invoke method: C.override
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, $c                                # Load global: c
  bnez a0, label_9                         # Ensure not None
  j error.None                             # Go to error handler
label_9:                                   # Not None
  sw a0, -16(fp)                           # Push argument 0 from last.
  lw a0, -16(fp)                           # Peek stack slot 3
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 4(a1)                             # Load address of method: C.simple
  addi sp, fp, -16                         # Set SP to last argument.
  jalr a1                                  # Invoke method: C.simple
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, $c                                # Load global: c
  sw a0, $a, t0                            # Assign global: a (using tmp register)
  lw a0, $c                                # Load global: c
  bnez a0, label_10                        # Ensure not None
  j error.None                             # Go to error handler
label_10:                                  # Not None
  sw a0, -16(fp)                           # Push argument 0 from last.
  lw a0, -16(fp)                           # Peek stack slot 3
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 4(a1)                             # Load address of method: C.simple
  addi sp, fp, -16                         # Set SP to last argument.
  jalr a1                                  # Invoke method: C.simple
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, $a                                # Load global: a
  bnez a0, label_11                        # Ensure not None
  j error.None                             # Go to error handler
label_11:                                  # Not None
  sw a0, -20(fp)                           # Push argument 3 from last.
  addi sp, fp, -32                         # Set SP to last argument.
  jal $one                                 # Invoke function: one
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -24(fp)                           # Push argument 2 from last.
  addi sp, fp, -32                         # Set SP to last argument.
  jal $two                                 # Invoke function: two
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -28(fp)                           # Push argument 1 from last.
  addi sp, fp, -32                         # Set SP to last argument.
  jal $three                               # Invoke function: three
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -32(fp)                           # Push argument 0 from last.
  lw a0, -20(fp)                           # Peek stack slot 4
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 12(a1)                            # Load address of method: A.order_eval
  addi sp, fp, -32                         # Set SP to last argument.
  jalr a1                                  # Invoke method: A.order_eval
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, $a                                # Load global: a
  bnez a0, label_12                        # Ensure not None
  j error.None                             # Go to error handler
label_12:                                  # Not None
  sw a0, -32(fp)                           # Push argument 0 from last.
  lw a0, -32(fp)                           # Peek stack slot 7
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 24(a1)                            # Load address of method: A.downCastBool
  addi sp, fp, -32                         # Set SP to last argument.
  jalr a1                                  # Invoke method: A.downCastBool
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, $a                                # Load global: a
  bnez a0, label_13                        # Ensure not None
  j error.None                             # Go to error handler
label_13:                                  # Not None
  sw a0, -32(fp)                           # Push argument 0 from last.
  lw a0, -32(fp)                           # Peek stack slot 7
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 20(a1)                            # Load address of method: A.downCastInt
  addi sp, fp, -32                         # Set SP to last argument.
  jalr a1                                  # Invoke method: A.downCastInt
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $make_list                           # Invoke function: make_list
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -20(fp)                           # Push on stack slot 5
  addi sp, fp, -32                         # Set SP to last argument.
  jal $next_int                            # Invoke function: next_int
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -24(fp)                           # Push on stack slot 6
  li a0, 3                                 # Load integer literal 3
  lw t0, -24(fp)                           # Pop stack slot 6
  sub a0, t0, a0                           # Operator -
  lw a1, -20(fp)                           # Pop stack slot 5
  bnez a1, label_14                        # Ensure not None
  j error.None                             # Go to error handler
label_14:                                  # Not None
  lw t0, 12(a1)                            # Load attribute: __len__
  bltu a0, t0, label_15                    # Ensure 0 <= index < len
  j error.OOB                              # Go to error handler
label_15:                                  # Index within bounds
  addi a0, a0, 4                           # Compute list element offset in words
  li t0, 4                                 # Word size in bytes
  mul a0, a0, t0                           # Compute list element offset in bytes
  add a0, a1, a0                           # Pointer to list element
  lw a0, 0(a0)                             # Get list element
  jal makeint                              # Box integer
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  la a0, $Dog$prototype                    # Load pointer to prototype of: Dog
  jal alloc                                # Allocate new object in A0
  sw a0, -12(fp)                           # Push on stack slot 3
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  lw a1, 8(a0)                             # Load address of object's dispatch table
  lw a1, 0(a1)                             # Load address of method: Dog.__init__
  jalr a1                                  # Invoke method: Dog.__init__
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  lw a0, -12(fp)                           # Pop stack slot 3
  sw a0, $fido, t0                         # Assign global: fido (using tmp register)
  li a0, 2                                 # Load integer literal 2
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $foo                                 # Invoke function: foo
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $goo                                 # Invoke function: goo
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  la a0, const_3                           # Load string literal
  sw a0, -44(fp)                           # Push argument 1 from last.
  la a0, const_4                           # Load string literal
  sw a0, -48(fp)                           # Push argument 0 from last.
  addi sp, fp, -48                         # Set SP to last argument.
  jal strcat                               # Call string concatenation function
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -28(fp)                           # Push argument 1 from last.
  la a0, const_5                           # Load string literal
  sw a0, -32(fp)                           # Push argument 0 from last.
  addi sp, fp, -32                         # Set SP to last argument.
  jal strcat                               # Call string concatenation function
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  la a0, const_6                           # Load string literal
  sw a0, -32(fp)                           # Push argument 0 from last.
  addi sp, fp, -32                         # Set SP to last argument.
  jal $len                                 # Invoke function: len
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  jal makeint                              # Box integer
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  la t0, noconv                            # Identity conversion
  sw t0, -20(fp)                           # Push argument 3 from last.
  la t0, noconv                            # Identity conversion
  sw t0, -24(fp)                           # Push argument 2 from last.
  la t0, noconv                            # Identity conversion
  sw t0, -36(fp)                           # Push argument 3 from last.
  la t0, noconv                            # Identity conversion
  sw t0, -40(fp)                           # Push argument 2 from last.
  li a0, 1                                 # Load integer literal 1
  sw a0, -52(fp)                           # Push argument 3 from last.
  li a0, 2                                 # Load integer literal 2
  sw a0, -56(fp)                           # Push argument 2 from last.
  li a0, 3                                 # Load integer literal 3
  sw a0, -60(fp)                           # Push argument 1 from last.
  li a0, 3                                 # Pass list length
  sw a0, -64(fp)                           # Push argument 0 from last.
  addi sp, fp, -64                         # Set SP to last argument.
  jal conslist                             # Move values to new list object
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -44(fp)                           # Push argument 1 from last.
  li a0, 4                                 # Load integer literal 4
  sw a0, -52(fp)                           # Push argument 3 from last.
  li a0, 5                                 # Load integer literal 5
  sw a0, -56(fp)                           # Push argument 2 from last.
  li a0, 6                                 # Load integer literal 6
  sw a0, -60(fp)                           # Push argument 1 from last.
  li a0, 3                                 # Pass list length
  sw a0, -64(fp)                           # Push argument 0 from last.
  addi sp, fp, -64                         # Set SP to last argument.
  jal conslist                             # Move values to new list object
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -48(fp)                           # Push argument 0 from last.
  addi sp, fp, -48                         # Set SP to last argument.
  jal concat                               # Call runtime concatenation routine.
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -28(fp)                           # Push argument 1 from last.
  li a0, 7                                 # Load integer literal 7
  sw a0, -36(fp)                           # Push argument 3 from last.
  li a0, 8                                 # Load integer literal 8
  sw a0, -40(fp)                           # Push argument 2 from last.
  li a0, 9                                 # Load integer literal 9
  sw a0, -44(fp)                           # Push argument 1 from last.
  li a0, 3                                 # Pass list length
  sw a0, -48(fp)                           # Push argument 0 from last.
  addi sp, fp, -48                         # Set SP to last argument.
  jal conslist                             # Move values to new list object
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  sw a0, -32(fp)                           # Push argument 0 from last.
  addi sp, fp, -32                         # Set SP to last argument.
  jal concat                               # Call runtime concatenation routine.
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  bnez a0, label_16                        # Ensure not None
  j error.None                             # Go to error handler
label_16:                                  # Not None
  sw a0, -12(fp)                           # Push on stack slot 3
  mv t1, zero                              # Initialize for-loop index
  sw t1, -16(fp)                           # Push on stack slot 4
label_17:                                  # for-loop header
  lw t1, -16(fp)                           # Pop stack slot 4
  lw t0, -12(fp)                           # Peek stack slot 2
  lw t2, 12(t0)                            # Get attribute __len__
  bgeu t1, t2, label_18                    # Exit loop if idx >= len(iter)
  addi t1, t1, 1                           # Increment idx
  sw t1, -16(fp)                           # Push on stack slot 4
  addi t1, t1, 3                           # Compute list element offset in words
  li t2, 4                                 # Word size in bytes
  mul t1, t1, t2                           # Compute list element offset in bytes
  add t1, t0, t1                           # Pointer to list element
  lw t0, 0(t1)                             # Get list element
  sw t0, $i, t1                            # Assign global: i (using tmp register)
  lw a0, $i                                # Load global: i
  jal makeint                              # Box integer
  sw a0, -32(fp)                           # Push argument 0 from last.
  addi sp, fp, -32                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  j label_17                               # Loop back to header
label_18:                                  # for-loop footer
  la a0, const_7                           # Load string literal
  sw a0, -16(fp)                           # Push on stack slot 4
  mv t1, zero                              # Initialize for-loop index
  sw t1, -20(fp)                           # Push on stack slot 5
label_19:                                  # for-loop header
  lw t1, -20(fp)                           # Peek stack slot 4
  lw t0, -16(fp)                           # Peek stack slot 3
  lw t2, 12(t0)                            # Get attribute __len__
  bgeu t1, t2, label_20                    # Exit loop if idx >= len(iter)
  lw t0, -20(fp)                           # Pop stack slot 5
  lw a1, -16(fp)                           # Peek stack slot 3
  addi t1, t0, 1                           # Increment index for next iteration
  sw t1, -20(fp)                           # Push on stack slot 5
  addi t0, t0, 16                          # Convert index to offset to char in bytes
  add t0, a1, t0                           # Get pointer to char
  lbu t0, 0(t0)                            # Load character
  li t1, 20
  mul t0, t0, t1                           # Multiply by size of string object
  la a0, allChars                          # Index into single-char table
  add a0, a0, t0
  sw a0, $j, t0                            # Assign global: j (using tmp register)
  lw a0, $j                                # Load global: j
  sw a0, -32(fp)                           # Push argument 0 from last.
  addi sp, fp, -32                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  j label_19                               # Loop back to header
label_20:                                  # for-loop footer
  li a0, 1                                 # Load boolean literal: true
  seqz a0, a0                              # Logical not
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  li a0, 1                                 # Load boolean literal: true
  seqz a0, a0                              # Logical not
  seqz a0, a0                              # Logical not
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  li a0, 0                                 # Load boolean literal: false
  seqz a0, a0                              # Logical not
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  li a0, 0                                 # Load boolean literal: false
  seqz a0, a0                              # Logical not
  seqz a0, a0                              # Logical not
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $zoo                                 # Invoke function: zoo
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  beqz a0, label_21                        # Operator and: short-circuit left operand
  addi sp, fp, -16                         # Set SP to last argument.
  jal $mar                                 # Invoke function: mar
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
label_21:                                  # Done evaluating operator: and
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $mar                                 # Invoke function: mar
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  beqz a0, label_22                        # Operator and: short-circuit left operand
  addi sp, fp, -16                         # Set SP to last argument.
  jal $zoo                                 # Invoke function: zoo
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
label_22:                                  # Done evaluating operator: and
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $zoo                                 # Invoke function: zoo
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  bnez a0, label_23                        # Operator or: short-circuit left operand
  addi sp, fp, -16                         # Set SP to last argument.
  jal $mar                                 # Invoke function: mar
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
label_23:                                  # Done evaluating operator: or
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $mar                                 # Invoke function: mar
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  bnez a0, label_24                        # Operator or: short-circuit left operand
  addi sp, fp, -16                         # Set SP to last argument.
  jal $zoo                                 # Invoke function: zoo
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
label_24:                                  # Done evaluating operator: or
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $zoo                                 # Invoke function: zoo
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  mv a0, a0                                # Set result from 'then' expression
  j label_26
label_25:                                  # Else part
  addi sp, fp, -16                         # Set SP to last argument.
  jal $mar                                 # Invoke function: mar
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  mv a0, a0                                # Set result from 'else' expression
label_26:                                  # End of if-else expression
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $mar                                 # Invoke function: mar
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  mv a0, a0                                # Set result from 'then' expression
  j label_28
label_27:                                  # Else part
  addi sp, fp, -16                         # Set SP to last argument.
  jal $zoo                                 # Invoke function: zoo
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  mv a0, a0                                # Set result from 'else' expression
label_28:                                  # End of if-else expression
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  j label_29                               # Branch on false.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $zoo                                 # Invoke function: zoo
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  mv a0, a0                                # Set result from 'then' expression
  j label_30
label_29:                                  # Else part
  addi sp, fp, -16                         # Set SP to last argument.
  jal $mar                                 # Invoke function: mar
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  mv a0, a0                                # Set result from 'else' expression
label_30:                                  # End of if-else expression
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  j label_31                               # Branch on false.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $mar                                 # Invoke function: mar
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  mv a0, a0                                # Set result from 'then' expression
  j label_32
label_31:                                  # Else part
  addi sp, fp, -16                         # Set SP to last argument.
  jal $zoo                                 # Invoke function: zoo
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  mv a0, a0                                # Set result from 'else' expression
label_32:                                  # End of if-else expression
  jal makebool                             # Box boolean
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@..main.size               # Set SP to stack frame top.
  .equiv @..main.size, 64
label_0:                                   # End of program
  li a0, 10                                # Code for ecall: exit
  ecall

.globl $object.__init__
$object.__init__:
# Init method for type object.	
  mv a0, zero                              # `None` constant
  jr ra                                    # Return

.globl $print
$print:
# Function print
  lw a0, 0(sp)                             # Load arg
  beq a0, zero, print_6                    # None is an illegal argument
  lw t0, 0(a0)                             # Get type tag of arg
  li t1, 1                                 # Load type tag of `int`
  beq t0, t1, print_7                      # Go to print(int)
  li t1, 3                                 # Load type tag of `str`
  beq t0, t1, print_8                      # Go to print(str)
  li t1, 2                                 # Load type tag of `bool`
  beq t0, t1, print_9                      # Go to print(bool)
print_6:                                   # Invalid argument
  li a0, 1                                 # Exit code for: Invalid argument
  la a1, const_8                           # Load error message as str
  addi a1, a1, @.__str__                   # Load address of attribute __str__
  j abort                                  # Abort

# Printing bools
print_9:                                   # Print bool object in A0
  lw a0, @.__bool__(a0)                    # Load attribute __bool__
  beq a0, zero, print_10                   # Go to: print(False)
  la a0, const_9                           # String representation: True
  j print_8                                # Go to: print(str)
print_10:                                  # Print False object in A0
  la a0, const_10                          # String representation: False
  j print_8                                # Go to: print(str)

# Printing strs.
print_8:                                   # Print str object in A0
  addi a1, a0, @.__str__                   # Load address of attribute __str__
  j print_11                               # Print the null-terminated string is now in A1
  mv a0, zero                              # Load None
  j print_5                                # Go to return
print_11:                                  # Print null-terminated string in A1
  li a0, @print_string                     # Code for ecall: print_string
  ecall                                    # Print string
  li a1, 10                                # Load newline character
  li a0, @print_char                       # Code for ecall: print_char
  ecall                                    # Print character
  j print_5                                # Go to return

# Printing ints.
print_7:                                   # Print int object in A0
  lw a1, @.__int__(a0)                     # Load attribute __int__
  li a0, @print_int                        # Code for ecall: print_int
  ecall                                    # Print integer
  li a1, 10                                # Load newline character
  li a0, 11                                # Code for ecall: print_char
  ecall                                    # Print character

print_5:                                   # End of function
  mv a0, zero                              # Load None
  jr ra                                    # Return to caller

.globl $len
$len:
# Function len
      # We do not save/restore fp/ra for this function
      # because we know that it does not use the stack or does not
      # call other functions.

  lw a0, 0(sp)                             # Load arg
  beq a0, zero, len_12                     # None is an illegal argument
  lw t0, 0(a0)                             # Get type tag of arg
  li t1, 3                                 # Load type tag of `str`
  beq t0, t1, len_13                       # Go to len(str)
  li t1, -1                                # Load type tag for list objects
  beq t0, t1, len_13                       # Go to len(list)
len_12:                                    # Invalid argument
  li a0, @error_arg                        # Exit code for: Invalid argument
  la a1, const_8                           # Load error message as str
  addi a1, a1, @.__str__                   # Load address of attribute __str__
  j abort                                  # Abort
len_13:                                    # Get length of string
  lw a0, @.__len__(a0)                     # Load attribute: __len__
  jr ra                                    # Return to caller

.globl $input
$input:
# Function input
  addi sp, sp, -16                         # Reserve stack	
  sw ra, 12(sp)                            # Save registers
  sw fp, 8(sp)	
  sw s1, 4(sp)
  addi fp, sp, 16                          # Set fp

  li a0, @fill_line_buffer                 # Fill the internal line buffer.
  ecall
  bgez a0, input_nonempty                  # More input found
  la a0, $str$prototype                    # EOF: Return empty string.
  j input_done

input_nonempty:
  mv s1, a0
  addi t0, s1, 5                           # Compute bytes for string (+NL+NUL),
  addi t0, t0, @.__str__                   # Including header.
  srli a1, t0, 2                           # Convert to words.
  la a0, $str$prototype                    # Load address of string prototype.
  jal ra, alloc2                           # Allocate string.
  sw s1, @.__len__(a0)                     # Store string length.
  mv a2, s1                                # Pass length.
  mv s1, a0                                # Save string object address.
  addi a1, a0, @.__str__                   # Pass address of string data.
  li a0, @read_string                      # ecall to read from internal buffer.
  ecall
  addi a0, a0, 1                           # Actual length (including NL).
  sw a0, @.__len__(s1)                     # Store actual length.
  add t0, a0, s1
  li t1, 10                                # Store newline and null byte
  sb t1, @.__str__-1(t0)
  sb zero, @.__str__(t0)                   # Store null byte at end.
  mv a0, s1                                # Return string object.

input_done:
  lw s1, -12(fp)
  lw ra, -4(fp)
  lw fp, -8(fp)
  addi sp, sp, 16
  jr ra

.globl $foo
$foo:
  addi sp, sp, -@foo.size                  # Reserve space for stack frame.
  sw ra, @foo.size-4(sp)                   # return address
  sw fp, @foo.size-8(sp)                   # control link
  addi fp, sp, @foo.size                   # New fp is at old SP.
  li a0, 5                                 # Load integer literal 5
  sw a0, 0(fp)                             # Assign var: foo.x
  lw a0, 0(fp)                             # Load var: foo.x
  jal makeint                              # Box integer
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@foo.size                  # Set SP to stack frame top.
  mv a0, zero                              # Load None
  j label_34                               # Jump to function epilogue
label_34:                                  # Epilogue
  .equiv @foo.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @foo.size                   # Restore stack pointer
  jr ra                                    # Return to caller

.globl $next_int
$next_int:
  addi sp, sp, -@next_int.size             # Reserve space for stack frame.
  sw ra, @next_int.size-4(sp)              # return address
  sw fp, @next_int.size-8(sp)              # control link
  addi fp, sp, @next_int.size              # New fp is at old SP.
  lw a0, $next                             # Load global: next
  sw a0, -12(fp)                           # Push on stack slot 3
  li a0, 1                                 # Load integer literal 1
  lw t0, -12(fp)                           # Pop stack slot 3
  add a0, t0, a0                           # Operator +
  sw a0, $next, t0                         # Assign global: next (using tmp register)
  lw a0, $next                             # Load global: next
  j label_36                               # Go to return
  mv a0, zero                              # Load None
  j label_36                               # Jump to function epilogue
label_36:                                  # Epilogue
  .equiv @next_int.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @next_int.size              # Restore stack pointer
  jr ra                                    # Return to caller

.globl $make_list
$make_list:
  addi sp, sp, -@make_list.size            # Reserve space for stack frame.
  sw ra, @make_list.size-4(sp)             # return address
  sw fp, @make_list.size-8(sp)             # control link
  addi fp, sp, @make_list.size             # New fp is at old SP.
  addi sp, fp, -32                         # Set SP to last argument.
  jal $next_int                            # Invoke function: next_int
  addi sp, fp, -@make_list.size            # Set SP to stack frame top.
  sw a0, -20(fp)                           # Push argument 3 from last.
  addi sp, fp, -32                         # Set SP to last argument.
  jal $next_int                            # Invoke function: next_int
  addi sp, fp, -@make_list.size            # Set SP to stack frame top.
  sw a0, -24(fp)                           # Push argument 2 from last.
  addi sp, fp, -32                         # Set SP to last argument.
  jal $next_int                            # Invoke function: next_int
  addi sp, fp, -@make_list.size            # Set SP to stack frame top.
  sw a0, -28(fp)                           # Push argument 1 from last.
  li a0, 3                                 # Pass list length
  sw a0, -32(fp)                           # Push argument 0 from last.
  addi sp, fp, -32                         # Set SP to last argument.
  jal conslist                             # Move values to new list object
  addi sp, fp, -@make_list.size            # Set SP to stack frame top.
  j label_38                               # Go to return
  mv a0, zero                              # Load None
  j label_38                               # Jump to function epilogue
label_38:                                  # Epilogue
  .equiv @make_list.size, 32
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @make_list.size             # Restore stack pointer
  jr ra                                    # Return to caller

.globl $goo.bar
$goo.bar:
  addi sp, sp, -@goo.bar.size              # Reserve space for stack frame.
  sw ra, @goo.bar.size-4(sp)               # return address
  sw fp, @goo.bar.size-8(sp)               # control link
  addi fp, sp, @goo.bar.size               # New fp is at old SP.
  lw a0, $x                                # Load global: x
  sw a0, -12(fp)                           # Push on stack slot 3
  li a0, 10                                # Load integer literal 10
  lw t0, -12(fp)                           # Pop stack slot 3
  add a0, t0, a0                           # Operator +
  sw a0, $x, t0                            # Assign global: x (using tmp register)
  lw a0, $x                                # Load global: x
  jal makeint                              # Box integer
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@goo.bar.size              # Set SP to stack frame top.
  mv a0, zero                              # Load None
  j label_40                               # Jump to function epilogue
label_40:                                  # Epilogue
  .equiv @goo.bar.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @goo.bar.size               # Restore stack pointer
  jr ra                                    # Return to caller

.globl $goo
$goo:
  addi sp, sp, -@goo.size                  # Reserve space for stack frame.
  sw ra, @goo.size-4(sp)                   # return address
  sw fp, @goo.size-8(sp)                   # control link
  addi fp, sp, @goo.size                   # New fp is at old SP.
  li a0, 100                               # Load integer literal 100
  sw a0, -12(fp)                           # local variable x
  mv t0, fp                                # Get static link to goo
  sw t0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $goo.bar                             # Invoke function: goo.bar
  addi sp, fp, -@goo.size                  # Set SP to stack frame top.
  mv a0, zero                              # Load None
  j label_42                               # Jump to function epilogue
label_42:                                  # Epilogue
  .equiv @goo.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @goo.size                   # Restore stack pointer
  jr ra                                    # Return to caller

.globl $A.simple
$A.simple:
  addi sp, sp, -@A.simple.size             # Reserve space for stack frame.
  sw ra, @A.simple.size-4(sp)              # return address
  sw fp, @A.simple.size-8(sp)              # control link
  addi fp, sp, @A.simple.size              # New fp is at old SP.
  la a0, const_11                          # Load string literal
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@A.simple.size             # Set SP to stack frame top.
  mv a0, zero                              # Load None
  j label_44                               # Jump to function epilogue
label_44:                                  # Epilogue
  .equiv @A.simple.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @A.simple.size              # Restore stack pointer
  jr ra                                    # Return to caller

.globl $A.simple_return
$A.simple_return:
  addi sp, sp, -@A.simple_return.size      # Reserve space for stack frame.
  sw ra, @A.simple_return.size-4(sp)       # return address
  sw fp, @A.simple_return.size-8(sp)       # control link
  addi fp, sp, @A.simple_return.size       # New fp is at old SP.
  la a0, const_11                          # Load string literal
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@A.simple_return.size      # Set SP to stack frame top.
  li a0, 3                                 # Load integer literal 3
  j label_46                               # Go to return
  mv a0, zero                              # Load None
  j label_46                               # Jump to function epilogue
label_46:                                  # Epilogue
  .equiv @A.simple_return.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @A.simple_return.size       # Restore stack pointer
  jr ra                                    # Return to caller

.globl $A.order_eval
$A.order_eval:
  addi sp, sp, -@A.order_eval.size         # Reserve space for stack frame.
  sw ra, @A.order_eval.size-4(sp)          # return address
  sw fp, @A.order_eval.size-8(sp)          # control link
  addi fp, sp, @A.order_eval.size          # New fp is at old SP.
  lw a0, 8(fp)                             # Load var: A.order_eval.x
  sw a0, -20(fp)                           # Push on stack slot 5
  lw a0, 4(fp)                             # Load var: A.order_eval.y
  lw t0, -20(fp)                           # Pop stack slot 5
  add a0, t0, a0                           # Operator +
  sw a0, -20(fp)                           # Push on stack slot 5
  lw a0, 0(fp)                             # Load var: A.order_eval.z
  lw t0, -20(fp)                           # Pop stack slot 5
  add a0, t0, a0                           # Operator +
  jal makeint                              # Box integer
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@A.order_eval.size         # Set SP to stack frame top.
  mv a0, zero                              # Load None
  j label_48                               # Jump to function epilogue
label_48:                                  # Epilogue
  .equiv @A.order_eval.size, 32
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @A.order_eval.size          # Restore stack pointer
  jr ra                                    # Return to caller

.globl $A.override
$A.override:
  addi sp, sp, -@A.override.size           # Reserve space for stack frame.
  sw ra, @A.override.size-4(sp)            # return address
  sw fp, @A.override.size-8(sp)            # control link
  addi fp, sp, @A.override.size            # New fp is at old SP.
  la a0, const_12                          # Load string literal
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@A.override.size           # Set SP to stack frame top.
  mv a0, zero                              # Load None
  j label_50                               # Jump to function epilogue
label_50:                                  # Epilogue
  .equiv @A.override.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @A.override.size            # Restore stack pointer
  jr ra                                    # Return to caller

.globl $A.downCastInt
$A.downCastInt:
  addi sp, sp, -@A.downCastInt.size        # Reserve space for stack frame.
  sw ra, @A.downCastInt.size-4(sp)         # return address
  sw fp, @A.downCastInt.size-8(sp)         # control link
  addi fp, sp, @A.downCastInt.size         # New fp is at old SP.
  li a0, 1                                 # Load integer literal 1
  jal makeint                              # Box integer
  j label_52                               # Go to return
  mv a0, zero                              # Load None
  j label_52                               # Jump to function epilogue
label_52:                                  # Epilogue
  .equiv @A.downCastInt.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @A.downCastInt.size         # Restore stack pointer
  jr ra                                    # Return to caller

.globl $A.downCastBool
$A.downCastBool:
  addi sp, sp, -@A.downCastBool.size       # Reserve space for stack frame.
  sw ra, @A.downCastBool.size-4(sp)        # return address
  sw fp, @A.downCastBool.size-8(sp)        # control link
  addi fp, sp, @A.downCastBool.size        # New fp is at old SP.
  li a0, 0                                 # Load boolean literal: false
  jal makebool                             # Box boolean
  j label_54                               # Go to return
  mv a0, zero                              # Load None
  j label_54                               # Jump to function epilogue
label_54:                                  # Epilogue
  .equiv @A.downCastBool.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @A.downCastBool.size        # Restore stack pointer
  jr ra                                    # Return to caller

.globl $B.override
$B.override:
  addi sp, sp, -@B.override.size           # Reserve space for stack frame.
  sw ra, @B.override.size-4(sp)            # return address
  sw fp, @B.override.size-8(sp)            # control link
  addi fp, sp, @B.override.size            # New fp is at old SP.
  la a0, const_13                          # Load string literal
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@B.override.size           # Set SP to stack frame top.
  mv a0, zero                              # Load None
  j label_56                               # Jump to function epilogue
label_56:                                  # Epilogue
  .equiv @B.override.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @B.override.size            # Restore stack pointer
  jr ra                                    # Return to caller

.globl $one
$one:
  addi sp, sp, -@one.size                  # Reserve space for stack frame.
  sw ra, @one.size-4(sp)                   # return address
  sw fp, @one.size-8(sp)                   # control link
  addi fp, sp, @one.size                   # New fp is at old SP.
  la a0, const_3                           # Load string literal
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@one.size                  # Set SP to stack frame top.
  li a0, 1                                 # Load integer literal 1
  j label_58                               # Go to return
  mv a0, zero                              # Load None
  j label_58                               # Jump to function epilogue
label_58:                                  # Epilogue
  .equiv @one.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @one.size                   # Restore stack pointer
  jr ra                                    # Return to caller

.globl $two
$two:
  addi sp, sp, -@two.size                  # Reserve space for stack frame.
  sw ra, @two.size-4(sp)                   # return address
  sw fp, @two.size-8(sp)                   # control link
  addi fp, sp, @two.size                   # New fp is at old SP.
  la a0, const_4                           # Load string literal
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@two.size                  # Set SP to stack frame top.
  li a0, 2                                 # Load integer literal 2
  j label_60                               # Go to return
  mv a0, zero                              # Load None
  j label_60                               # Jump to function epilogue
label_60:                                  # Epilogue
  .equiv @two.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @two.size                   # Restore stack pointer
  jr ra                                    # Return to caller

.globl $three
$three:
  addi sp, sp, -@three.size                # Reserve space for stack frame.
  sw ra, @three.size-4(sp)                 # return address
  sw fp, @three.size-8(sp)                 # control link
  addi fp, sp, @three.size                 # New fp is at old SP.
  la a0, const_5                           # Load string literal
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@three.size                # Set SP to stack frame top.
  li a0, 3                                 # Load integer literal 3
  j label_62                               # Go to return
  mv a0, zero                              # Load None
  j label_62                               # Jump to function epilogue
label_62:                                  # Epilogue
  .equiv @three.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @three.size                 # Restore stack pointer
  jr ra                                    # Return to caller

.globl $zoo
$zoo:
  addi sp, sp, -@zoo.size                  # Reserve space for stack frame.
  sw ra, @zoo.size-4(sp)                   # return address
  sw fp, @zoo.size-8(sp)                   # control link
  addi fp, sp, @zoo.size                   # New fp is at old SP.
  la a0, const_14                          # Load string literal
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@zoo.size                  # Set SP to stack frame top.
  li a0, 1                                 # Load boolean literal: true
  j label_64                               # Go to return
  mv a0, zero                              # Load None
  j label_64                               # Jump to function epilogue
label_64:                                  # Epilogue
  .equiv @zoo.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @zoo.size                   # Restore stack pointer
  jr ra                                    # Return to caller

.globl $mar
$mar:
  addi sp, sp, -@mar.size                  # Reserve space for stack frame.
  sw ra, @mar.size-4(sp)                   # return address
  sw fp, @mar.size-8(sp)                   # control link
  addi fp, sp, @mar.size                   # New fp is at old SP.
  la a0, const_15                          # Load string literal
  sw a0, -16(fp)                           # Push argument 0 from last.
  addi sp, fp, -16                         # Set SP to last argument.
  jal $print                               # Invoke function: print
  addi sp, fp, -@mar.size                  # Set SP to stack frame top.
  li a0, 0                                 # Load boolean literal: false
  j label_66                               # Go to return
  mv a0, zero                              # Load None
  j label_66                               # Jump to function epilogue
label_66:                                  # Epilogue
  .equiv @mar.size, 16
  lw ra, -4(fp)                            # Get return address
  lw fp, -8(fp)                            # Use control link to restore caller's fp
  addi sp, sp, @mar.size                   # Restore stack pointer
  jr ra                                    # Return to caller

.globl alloc
alloc:
# Runtime support function alloc.
        # Prototype address is in a0.
  lw a1, 4(a0)                             # Get size of object in words
  j alloc2                                 # Allocate object with exact size

.globl alloc2
alloc2:
# Runtime support function alloc2 (realloc).
        # Prototype address is in a0.
        # Number of words to allocate is in a1.
  li a2, 4                                 # Word size in bytes
  mul a2, a1, a2                           # Calculate number of bytes to allocate
  add a2, gp, a2                           # Estimate where GP will move
  bgeu a2, s11, alloc2_15                  # Go to OOM handler if too large
  lw t0, @.__obj_size__(a0)                # Get size of object in words
  mv t2, a0                                # Initialize src ptr
  mv t3, gp                                # Initialize dest ptr
alloc2_16:                                 # Copy-loop header
  lw t1, 0(t2)                             # Load next word from src
  sw t1, 0(t3)                             # Store next word to dest
  addi t2, t2, 4                           # Increment src
  addi t3, t3, 4                           # Increment dest
  addi t0, t0, -1                          # Decrement counter
  bne t0, zero, alloc2_16                  # Loop if more words left to copy
  mv a0, gp                                # Save new object's address to return
  sw a1, @.__obj_size__(a0)                # Set size of new object in words
                                           # (same as requested size)
  mv gp, a2                                # Set next free slot in the heap
  jr ra                                    # Return to caller
alloc2_15:                                 # OOM handler
  li a0, @error_oom                        # Exit code for: Out of memory
  la a1, const_16                          # Load error message as str
  addi a1, a1, @.__str__                   # Load address of attribute __str__
  j abort                                  # Abort

.globl abort
abort:
# Runtime support function abort (does not return).
  mv t0, a0                                # Save exit code in temp
  li a0, @print_string                     # Code for print_string ecall
  ecall                                    # Print error message in a1
  li a1, 10                                # Load newline character
  li a0, @print_char                       # Code for print_char ecall
  ecall                                    # Print newline
  mv a1, t0                                # Move exit code to a1
  li a0, @exit2                            # Code for exit2 ecall
  ecall                                    # Exit with code
abort_17:                                  # Infinite loop
  j abort_17                               # Prevent fallthrough

.globl heap.init
heap.init:
# Runtime support function heap.init.
  mv a1, a0                                # Move requested size to A1
  li a0, @sbrk                             # Code for ecall: sbrk
  ecall                                    # Request A1 bytes
  jr ra                                    # Return to caller

.globl concat
concat:

        addi sp, sp, -32
        sw ra, 28(sp)
        sw fp, 24(sp)
        addi fp, sp, 32
	sw s1, -12(fp)
        sw s2, -16(fp)
        sw s3, -20(fp)
	sw s4, -24(fp)
        sw s5, -28(fp)
        lw t0, 4(fp)
        lw t1, 0(fp)
        beqz t0, concat_none
        beqz t1, concat_none
        lw t0, @.__len__(t0)
        lw t1, @.__len__(t1)
        add s5, t0, t1
        addi a1, s5, @listHeaderWords
        la a0, $.list$prototype
        jal alloc2
        sw s5, @.__len__(a0)
	mv s5, a0
        addi s3, s5, @.__elts__
        lw s1, 4(fp)
	lw s2, @.__len__(s1)
        addi s1, s1, @.__elts__
	lw s4, 12(fp)
concat_1:
        beqz s2, concat_2
        lw a0, 0(s1)
	jalr ra, s4, 0
        sw a0, 0(s3)
        addi s2, s2, -1
        addi s1, s1, 4
        addi s3, s3, 4
        j concat_1
concat_2:
        lw s1, 0(fp)
        lw s2, @.__len__(s1)
        addi s1, s1, @.__elts__
	lw s4, 8(fp)
concat_3:
        beqz s2, concat_4
        lw a0, 0(s1)
	jalr ra, s4, 0
        sw a0, 0(s3)
        addi s2, s2, -1
        addi s1, s1, 4
        addi s3, s3, 4
        j concat_3
concat_4:
	mv a0, s5
        lw s1, -12(fp)
        lw s2, -16(fp)
        lw s3, -20(fp)
	lw s4, -24(fp)
        lw s5, -28(fp)
        lw ra, -4(fp)
        lw fp, -8(fp)
        addi sp, sp, 32
        jr ra
concat_none:
        j error.None


.globl conslist
conslist:

        addi sp, sp, -8
        sw ra, 4(sp)
        sw fp, 0(sp)
        addi fp, sp, 8
        lw a1, 0(fp)
        la a0, $.list$prototype
        beqz a1, conslist_done
        addi a1, a1, @listHeaderWords
        jal alloc2
        lw t0, 0(fp)
        sw t0, @.__len__(a0)
        slli t1, t0, 2
        add t1, t1, fp
        addi t2, a0, @.__elts__
conslist_1:
        lw t3, 0(t1)
        sw t3, 0(t2)
        addi t1, t1, -4
        addi t2, t2, 4
        addi t0, t0, -1
        bnez t0, conslist_1
conslist_done:
        lw ra, -4(fp)
        lw fp, -8(fp)
        addi sp, sp, 8
        jr ra


.globl strcat
strcat:

        addi sp, sp, -12
        sw ra, 8(sp)
        sw fp, 4(sp)
        addi fp, sp, 12
        lw t0, 4(fp)
        lw t1, 0(fp)
        lw t0, @.__len__(t0)
        beqz t0, strcat_4
        lw t1, @.__len__(t1)
        beqz t1, strcat_5
        add t1, t0, t1
        sw t1, -12(fp)
        addi t1, t1, 4
        srli t1, t1, 2
        addi a1, t1, @listHeaderWords
        la a0, $str$prototype
        jal alloc2
        lw t0, -12(fp)
        sw t0, @.__len__(a0)
        addi t2, a0, 16
        lw t0, 4(fp)
        lw t1, @.__len__(t0)
        addi t0, t0, @.__str__
strcat_1:
        beqz t1, strcat_2
        lbu t3, 0(t0)
        sb t3, 0(t2)
        addi t1, t1, -1
        addi t0, t0, 1
        addi t2, t2, 1
        j strcat_1
strcat_2:
        lw t0, 0(fp)
        lw t1, 12(t0)
        addi t0, t0, 16
strcat_3:
        beqz t1, strcat_6
        lbu t3, 0(t0)
        sb t3, 0(t2)
        addi t1, t1, -1
        addi t0, t0, 1
        addi t2, t2, 1
        j strcat_3
strcat_4:
        lw a0, 0(fp)
        j strcat_7
strcat_5:
        lw a0, 4(fp)
        j strcat_7
strcat_6:
        sb zero, 0(t2)
strcat_7:
        lw ra, -4(fp)
        lw fp, -8(fp)
        addi sp, sp, 12
        jr ra


.globl streql
streql:

        addi sp, sp, -8
        sw ra, 4(sp)
        sw fp, 0(sp)
        addi fp, sp, 8
        lw a1, 4(fp)
        lw a2, 0(fp)
        lw t0, @.__len__(a1)
        lw t1, @.__len__(a2)
        bne t0, t1, streql_no
streql_1:
        lbu t2, @.__str__(a1)
        lbu t3, @.__str__(a2)
        bne t2, t3, streql_no
        addi a1, a1, 1
        addi a2, a2, 1
        addi t0, t0, -1
        bgtz t0, streql_1
        li a0, 1
        j streql_end
streql_no:
        xor a0, a0, a0
streql_end:
        lw ra, -4(fp)
        lw fp, -8(fp)
        addi sp, sp, 8
        jr ra


.globl strneql
strneql:

        addi sp, sp, -8
        sw ra, 4(sp)
        sw fp, 0(sp)
        addi fp, sp, 8
        lw a1, 4(fp)
        lw a2, 0(fp)
        lw t0, @.__len__(a1)
        lw t1, @.__len__(a2)
        bne t0, t1, strneql_yes
strneql_1:
        lbu t2, @.__str__(a1)
        lbu t3, @.__str__(a2)
        bne t2, t3, strneql_yes
        addi a1, a1, 1
        addi a2, a2, 1
        addi t0, t0, -1
        bgtz t0, strneql_1
        xor a0, a0, a0
        j strneql_end
strneql_yes:
        li a0, 1
strneql_end:
        lw ra, -4(fp)
        lw fp, -8(fp)
        addi sp, sp, 8
        jr ra


.globl makeint
makeint:

        addi sp, sp, -8
        sw ra, 4(sp)
        sw a0, 0(sp)
        la a0, $int$prototype
        jal ra, alloc
        lw t0, 0(sp)
        sw t0, @.__int__(a0)
        lw ra, 4(sp)
        addi sp, sp, 8
        jr ra


.globl makebool
makebool:

	slli a0, a0, 4
        la t0, @bool.False
        add a0, a0, t0
	jr ra


.globl noconv
noconv:

        jr ra


.globl initchars
initchars:

        la a0, $str$prototype
        lw t0, 0(a0)
        lw t1, 4(a0)
        lw t2, 8(a0)
        li t3, 1
        la a0, allChars
        li t4, 256
        mv t5, zero
initchars_1:
        sw t0, 0(a0)
        sw t1, 4(a0)
        sw t2, 8(a0)
        sw t3, 12(a0)
        sw t5, 16(a0)
        addi a0, a0, 20
        addi t5, t5, 1
        bne t4, t5, initchars_1
        jr  ra
        .data
        .align 2
        .globl allChars
allChars:
        .space 5120
        .text


.globl error.None
error.None:
  li a0, 4                                 # Exit code for: Operation on None
  la a1, const_17                          # Load error message as str
  addi a1, a1, 16                          # Load address of attribute __str__
  j abort                                  # Abort

.globl error.Div
error.Div:
  li a0, 2                                 # Exit code for: Division by zero
  la a1, const_18                          # Load error message as str
  addi a1, a1, 16                          # Load address of attribute __str__
  j abort                                  # Abort

.globl error.OOB
error.OOB:
  li a0, 3                                 # Exit code for: Index out of bounds
  la a1, const_19                          # Load error message as str
  addi a1, a1, 16                          # Load address of attribute __str__
  j abort                                  # Abort

.data

.globl const_0
const_0:
  .word 2                                  # Type tag for class: bool
  .word 4                                  # Object size
  .word $bool$dispatchTable                # Pointer to dispatch table
  .word 0                                  # Constant value of attribute: __bool__
  .align 2

.globl const_1
const_1:
  .word 2                                  # Type tag for class: bool
  .word 4                                  # Object size
  .word $bool$dispatchTable                # Pointer to dispatch table
  .word 1                                  # Constant value of attribute: __bool__
  .align 2

.globl const_2
const_2:
  .word 3                                  # Type tag for class: str
  .word 5                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 0                                  # Constant value of attribute: __len__
  .string ""                               # Constant value of attribute: __str__
  .align 2

.globl const_3
const_3:
  .word 3                                  # Type tag for class: str
  .word 5                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 1                                  # Constant value of attribute: __len__
  .string "a"                              # Constant value of attribute: __str__
  .align 2

.globl const_4
const_4:
  .word 3                                  # Type tag for class: str
  .word 5                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 1                                  # Constant value of attribute: __len__
  .string "b"                              # Constant value of attribute: __str__
  .align 2

.globl const_5
const_5:
  .word 3                                  # Type tag for class: str
  .word 5                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 1                                  # Constant value of attribute: __len__
  .string "c"                              # Constant value of attribute: __str__
  .align 2

.globl const_12
const_12:
  .word 3                                  # Type tag for class: str
  .word 7                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 8                                  # Constant value of attribute: __len__
  .string "original"                       # Constant value of attribute: __str__
  .align 2

.globl const_16
const_16:
  .word 3                                  # Type tag for class: str
  .word 8                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 13                                 # Constant value of attribute: __len__
  .string "Out of memory"                  # Constant value of attribute: __str__
  .align 2

.globl const_14
const_14:
  .word 3                                  # Type tag for class: str
  .word 5                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 3                                  # Constant value of attribute: __len__
  .string "foo"                            # Constant value of attribute: __str__
  .align 2

.globl const_9
const_9:
  .word 3                                  # Type tag for class: str
  .word 6                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 4                                  # Constant value of attribute: __len__
  .string "True"                           # Constant value of attribute: __str__
  .align 2

.globl const_11
const_11:
  .word 3                                  # Type tag for class: str
  .word 6                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 6                                  # Constant value of attribute: __len__
  .string "simple"                         # Constant value of attribute: __str__
  .align 2

.globl const_10
const_10:
  .word 3                                  # Type tag for class: str
  .word 6                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 5                                  # Constant value of attribute: __len__
  .string "False"                          # Constant value of attribute: __str__
  .align 2

.globl const_18
const_18:
  .word 3                                  # Type tag for class: str
  .word 9                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 16                                 # Constant value of attribute: __len__
  .string "Division by zero"               # Constant value of attribute: __str__
  .align 2

.globl const_6
const_6:
  .word 3                                  # Type tag for class: str
  .word 37                                 # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 130                                # Constant value of attribute: __len__
  .string "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" # Constant value of attribute: __str__
  .align 2

.globl const_15
const_15:
  .word 3                                  # Type tag for class: str
  .word 5                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 3                                  # Constant value of attribute: __len__
  .string "bar"                            # Constant value of attribute: __str__
  .align 2

.globl const_7
const_7:
  .word 3                                  # Type tag for class: str
  .word 6                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 6                                  # Constant value of attribute: __len__
  .string "potato"                         # Constant value of attribute: __str__
  .align 2

.globl const_19
const_19:
  .word 3                                  # Type tag for class: str
  .word 9                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 19                                 # Constant value of attribute: __len__
  .string "Index out of bounds"            # Constant value of attribute: __str__
  .align 2

.globl const_13
const_13:
  .word 3                                  # Type tag for class: str
  .word 8                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 12                                 # Constant value of attribute: __len__
  .string "not original"                   # Constant value of attribute: __str__
  .align 2

.globl const_17
const_17:
  .word 3                                  # Type tag for class: str
  .word 9                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 17                                 # Constant value of attribute: __len__
  .string "Operation on None"              # Constant value of attribute: __str__
  .align 2

.globl const_8
const_8:
  .word 3                                  # Type tag for class: str
  .word 9                                  # Object size
  .word $str$dispatchTable                 # Pointer to dispatch table
  .word 16                                 # Constant value of attribute: __len__
  .string "Invalid argument"               # Constant value of attribute: __str__
  .align 2
