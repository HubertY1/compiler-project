class Foo(object):
    x:int = 42
    def method(self: Foo):
        print(self.x)

x:Foo = None

x = Foo()
x = None
print(x.x)
