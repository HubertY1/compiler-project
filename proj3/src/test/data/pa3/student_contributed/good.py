i:int = 0
j:str = ""
def foo(x:int):
    x = 5
    print(x)
class Dog(object):
    pass
fido:Dog = None
next:int = 0

def next_int() -> int:
    global next
    next = next + 1
    return next

def make_list() -> [int]:
    return [next_int(), next_int(), next_int()]
x:int = 1

def goo():
    x:int = 100
    def bar():
        global x
        x = x+10
        print(x)
    bar()
class A(object):
    def simple(self:A):
        print("simple")
    def simple_return(self:A)-> int:
        print("simple")
        return 3
    def order_eval(self: A, x:int, y:int, z:int):
        print(x+ y+ z)
    def override(self: A):
        print("original")

    def downCastInt(self: A) -> object:
        return 1
    def downCastBool(self: A) -> object:
        return False


class B(A):
    def override(self: B):
        print("not original")

def one()-> int:
    print("a")
    return 1

def two()-> int:
    print("b")
    return 2

def three()-> int:
    print("c")
    return 3

class C(B):
    pass
def zoo()-> bool:
    print("foo")
    return True

def mar() -> bool:
    print("bar")
    return False


a :A  = None
b:B = None
c:C = None


a = A()

a.simple()

print(a.simple_return())
a.override()

b = B()

b.override()
b.simple()

a = b

b.simple()
b.override()

c = C()

c.override()
c.simple()

a = c

c.simple()

a.order_eval(one(), two(), three())

print(a.downCastBool())

print(a.downCastInt())

print(make_list()[next_int() - 3])

fido = Dog()
foo(2)
goo()
print("a" + "b" + "c")
print(len("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))
for i in [1,2,3] + [4,5,6] + [7,8,9]:
    print(i)
for j in "potato":
    print(j)

print(not True)

print(not not True)


print(not False)

print(not not False)


print(zoo() and mar())
print(mar() and zoo())

print(zoo() or mar())
print(mar() or zoo())

print(zoo() if True else mar())

print(mar() if True else zoo())

print(zoo() if False else mar())

print(mar() if False else zoo())
