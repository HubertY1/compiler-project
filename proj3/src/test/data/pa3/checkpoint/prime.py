# Get the n-th prime starting from 2
def get_prime(n:int) -> int:
    candidate:int = 2
    temp: bool = False
    found:int = 0
    print("new")
    while True:
        print("candidate 1")
        print(candidate)
        temp = is_prime(candidate)
        if  temp:
            if found+1 == n:
                return candidate
            found = found + 1
        candidate = candidate + 1
        print("candidate 2")
        print(candidate)
    return 0 # Never happens

def is_prime(x:int) -> bool:
    div:int = 2
    while div < x:
        if x % div == 0:
            return False
        div = div + 1
    return True

# Input parameter
n:int = 15

# Run [1, n]
i:int = 1

# Crunch
while i <= n:
    print(get_prime(i))
    i = i + 1
#print(is_prime(4))
