package chocopy.pa3.IR;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

/** A class for java deficits in the java language standard library*/
public class JavaDeficit {
    public static class Tup<A,B>  {
        A i0;
        B i1;

        public Tup(A a, B b) {
            this.i0 = a;
            this.i1 = b;
        }

        public  <C,D>  Tup<C,D> map( Function<A, C> f, Function<B, D> g) {
            return tup(f.apply(i0), g.apply(i1));
        }




    }
    /** shorthand creation*/
    public static <A,B>  Tup<A,B> tup(A a, B b) {
        return new Tup<A,B>(a,b);
    }

    public static <A,B,C,D> Function<Tup<A,B>, Tup<C,D>> tMap(Function<A,C> f, Function<B,D> g) {
        return t -> tup(f.apply(t.i0), g.apply(t.i1));
    }


    public static <A,B,C,D> Function<Tup<A,B>, D> tMap(BiFunction<A,B,D> f) {
        return t -> f.apply(t.i0, t.i1);
    }


    public static <A,B> TupStream<A,B> CartesianProduct(Collection<A> a_col, Collection<B> b_col) {
        //mfw monads
        return new TupStream(a_col.stream()
                    .flatMap( a -> b_col.stream()
                                        .map(b -> new Tup<A,B>(a, b))
                    ));
    }

    public static <A,B> TupStream<A,B> zipStream(List<A> a_col, List<B> b_col) {
        int size = Math.min(a_col.size(), b_col.size());

        return new TupStream<A,B>(IntStream.range(0, size)
          .mapToObj(i->  new Tup<A,B>(a_col.get(i), b_col.get(i))));
    }

    /** a hacky substitute for streams of natural tuples. */
    public static class TupStream<A,B>  {

        Stream<Tup<A,B>> str;

        public TupStream(Stream<Tup<A,B>> s) {
            str = s;
        }



        public TupStream<A,B> filter(BiPredicate<A, B> predicate) {
            return new TupStream<A,B>(str.filter(t -> predicate.test(t.i0, t.i1 )));
        }

        public <C, D> TupStream<C,D>  map(Function<A,C> f, Function<B,D> g) {
            return new TupStream<C,D>(str.map( t->  tup(f.apply(t.i0), g.apply(t.i1))));
        }


        public <C> Stream<C>  map(BiFunction<A,B,C> f) {
            return str.map(t-> f.apply(t.i0, t.i1));
        }


        public LongStream mapToLong(ToLongFunction<? super Tup<A, B>> mapper) {
            return null;
        }

        public DoubleStream mapToDouble(ToDoubleFunction<? super Tup<A, B>> mapper) {
            return null;
        }

        public <R> Stream<R> flatMap(Function<? super Tup<A, B>, ? extends Stream<? extends R>> mapper) {
            return null;
        }

        public IntStream flatMapToInt(Function<? super Tup<A, B>, ? extends IntStream> mapper) {
            return null;
        }

        public LongStream flatMapToLong(Function<? super Tup<A, B>, ? extends LongStream> mapper) {
            return null;
        }

        public DoubleStream flatMapToDouble(Function<? super Tup<A, B>, ? extends DoubleStream> mapper) {
            return null;
        }

        public Stream<Tup<A, B>> distinct() {
            return null;
        }

        public Stream<Tup<A, B>> sorted() {
            return null;
        }

        public Stream<Tup<A, B>> sorted(Comparator<? super Tup<A, B>> comparator) {
            return null;
        }

        public Stream<Tup<A, B>> peek(Consumer<? super Tup<A, B>> action) {
            return null;
        }

        public Stream<Tup<A, B>> limit(long maxSize) {
            return null;
        }

        public Stream<Tup<A, B>> skip(long n) {
            return null;
        }

        public void forEach(BiConsumer< A,  B> f) {
           str.forEach( t ->  f.accept(t.i0, t.i1));
        }

        public void forEachOrdered(BiConsumer< A,  B> f) {
            str.forEachOrdered( t ->  f.accept(t.i0, t.i1));
        }



        public Tup<A, B> reduce(Tup<A, B> identity, BinaryOperator<Tup<A, B>> accumulator) {
            return null;
        }


        public Optional<Tup<A, B>> reduce(BinaryOperator<Tup<A, B>> accumulator) {
            return Optional.empty();
        }

        public <U> U reduce(U identity, BiFunction<U, ? super Tup<A, B>, U> accumulator, BinaryOperator<U> combiner) {
            return null;
        }

        public <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super Tup<A, B>> accumulator, BiConsumer<R, R> combiner) {
            return null;
        }

        public Optional<Tup<A, B>> min(Comparator<? super Tup<A, B>> comparator) {
            return Optional.empty();
        }


        public Optional<Tup<A, B>> max(Comparator<? super Tup<A, B>> comparator) {
            return Optional.empty();
        }

        public long count() {
            return 0;
        }

        public boolean anyMatch(Predicate<? super Tup<A, B>> predicate) {
            return false;
        }

        public boolean allMatch(Predicate<? super Tup<A, B>> predicate) {
            return false;
        }

        public boolean noneMatch(Predicate<? super Tup<A, B>> predicate) {
            return false;
        }

        public Optional<Tup<A, B>> findFirst() {
            return Optional.empty();
        }

        public Optional<Tup<A, B>> findAny() {
            return Optional.empty();
        }

        public <R> R collect(Collector<? super Tup<A, B>, A, R> collector) {
            return null;
        }

        public Iterator<Tup<A, B>> iterator() {
            return null;
        }

        public Spliterator<Tup<A, B>> spliterator() {
            return null;
        }

        public boolean isParallel() {
            return false;
        }

        public Stream<Tup<A, B>> sequential() {
            return null;
        }

        public Stream<Tup<A, B>> parallel() {
            return null;
        }

        public Stream<Tup<A, B>> unordered() {
            return null;
        }

        public Stream<Tup<A, B>> onClose(Runnable closeHandler) {
            return null;
        }

        public void close() {

        }
    }
}

