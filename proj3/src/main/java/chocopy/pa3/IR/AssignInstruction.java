package chocopy.pa3.IR;

import chocopy.common.codegen.RiscVBackend;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;

/**
 * an instruction that can be bound, or who's value mutates a Pseudo-register
 */
public abstract class AssignInstruction extends Instruction {
    protected Optional<RiscVBackend.Register> modifies_registers = Optional.empty();

    @Override
    public Optional<PR> getModifies() {
        return modifies;
    }

    public void setModifies( PR modifies) {
        this.modifies = Optional.of(modifies);
    }

    @Override
    public RiscVBackend.Register getRD() {
        assert modifies_registers.isPresent() : "Modified register not present";
        return modifies_registers.orElse(null);
    }

    public void replace(HashMap<PR, RiscVBackend.Register> m) {
        assert dependents.isPresent() : "dependents were not present When ReplaceDependents called on instruction:"
                + toString() + " of class " + this.getClass().getSimpleName();
        assert modifies.isPresent() : "modifies were not present When ReplaceDependents called on instruction:"
                + toString() + " of class " + this.getClass().getSimpleName();


        registers = dependents.map(p -> Arrays.stream(p)
                .map(m::get)
                .toArray(RiscVBackend.Register[]::new));

        modifies_registers = modifies.map(m::get);
        this.markedForDeletion = !modifies_registers.isPresent();
    }
}
