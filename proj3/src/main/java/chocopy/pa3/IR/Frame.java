package chocopy.pa3.IR;

import chocopy.common.codegen.FuncInfo;
import chocopy.common.codegen.Label;
import chocopy.common.codegen.RiscVBackend;
import chocopy.common.codegen.RiscVBackend.Register;
import chocopy.common.codegen.StackVarInfo;
import chocopy.pa3.CodeGenImpl;
import chocopy.pa3.IR.Instructions.CustomInstructions;
import chocopy.pa3.IR.Instructions.CustomInstructions.PREJAL;
import chocopy.pa3.IR.Instructions.LabelInst;
import chocopy.pa3.IR.Instructions.iInst.ADDI;
import chocopy.pa3.IR.Instructions.iInst.LW;
import chocopy.pa3.IR.Instructions.pseudoInst.MV;
import chocopy.pa3.IR.Instructions.sInst.SW;
import chocopy.pa3.IR.Instructions.sbInst;
import chocopy.pa3.IR.Instructions.ujInst;
import chocopy.pa3.IR.Instructions.ujInst.J;
import chocopy.pa3.IR.RegisterAllocator.LivenessAnalysis;

import java.util.*;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static chocopy.common.codegen.RiscVBackend.Register.*;
import static chocopy.pa3.IR.JavaDeficit.CartesianProduct;

//import static chocopy.common.codegen.RiscVBackend.Register.A0;


public class Frame implements Iterable<Instruction> {

    /* start of leave parameter storage in words; (leave room for control link)*/
    private static final Register[] registerArray = {
      A2, A3, A4, A5, A6, A7, S2, S3, S4, S5, S6,
      S7, S8, S9, S10, S11, T0, T1, T2, T3, T4, T5, T6
    };
    static int lbl_uuid = 0;
    int prologue_end, epilogue_start = 0;
    /**
     * -----Instructions (list of pseudo-instructions) ---
     * segmented by "prologue_end", "epilogue_start" into
     * * prologue
     * * body
     * * epilogue
     */
    ArrayList<Instruction> instructions;
    LabelInst epilogue_label = new LabelInst("$$Epilogue_Lbl_" + lbl_uuid++);
    /**
     * Passed in parameters to frame, if any (if we're in a function call - mostly debug info)
     */
    ArrayList<String> parameters;
    Logger logger = Logger.getLogger(Frame.class.getName());
    FuncInfo funcinfo;
    boolean isGlobalFrame = false;
    //HashMap<PR, Register> registerHashMap = new HashMap<>();
    /**
     * Maps (declared variable names) -> (Psuedo-registers), for convenient use when converting
     * the AST to IR-Code.
     */
    HashMap<String, PR> regMap = new HashMap<>();
    /**
     * offset of variables from frame pointer
     */
    HashMap<PR, Integer> offSetMap = new HashMap<>();
    int next_param_loc = 0;
    /**
     * Parameter and comment info
     */
    HashMap<PR, String> isParamLoaded = new HashMap();
    LivenessAnalysis lifetimes;
    /**
     * Global variables location hashmap
     **/
    HashMap<PR, Label> globalLabels = new HashMap<>();

    /**
     * A map of psuedo registers to the variables they represent;
     * *
     */
    HashMap<PR, String> debug_names = new HashMap<>();

    HashMap<PR, NonLocalVarInfo> nonLocalVars = new HashMap<>();
    RiscVBackend backend;
    int stack_pos;
    ADDI mv_stack;


    public Frame(Collection<String> params, RiscVBackend b, FuncInfo funcinfo) {
        this.parameters = new ArrayList<>(params);
        this.instructions = new ArrayList<>();
        this.backend = b;
        this.next_param_loc = 0;
        this.funcinfo = funcinfo;
        /* room for preamble */
        this.stack_pos = -3 * backend.getWordSize();

            /* add in reverse order due to way they are
             written to stack (last parameter is closest)  */
        Collections.reverse(this.parameters);

        for (String varName : parameters) {
            PR parameter = new PR(varName);
            regMap.put(varName, parameter);
            offSetMap.put(parameter, next_param_loc);
            isParamLoaded.put(parameter, varName);
            next_param_loc += b.getWordSize();
        }
        initStackVars();
    }

    public void setVar(String i, PR reg) {
        regMap.put(i, reg);
    }

    public void setGlobalVar(PR reg, Label lab) {
        globalLabels.put(reg, lab);
    }

    public void setGlobalFrame() {
        isGlobalFrame = true;
    }

    private Label getGlobalVar(PR reg) {
        return globalLabels.get(reg);
    }

    public PR getVar(String i) {
        return regMap.get(i);
    }

    public PR getVar(StackVarInfo i) {
        if (regMap.containsKey(i.getVarName())) {
            return regMap.get(i.getVarName());
        } else {
            int idx;
            if (i.getFuncInfo().getParams().contains(i.getVarName())) {
                int last = i.getFuncInfo().getParams().size() - 1;
                idx = (last - i.getFuncInfo().getVarIndex(i.getVarName())) * backend.getWordSize();
            } else {
                idx = -(i.getFuncInfo().getVarIndex(i.getVarName()) + 3) * backend.getWordSize();
            }
            int func_dept = i.getFuncInfo().getDepth();
            NonLocalVarInfo vi = new NonLocalVarInfo(func_dept, idx, i.getVarName());
            PR p = new PR();
            regMap.put(i.getVarName(), p);
            nonLocalVars.put(p, vi);
            return p;
        }
    }

    /**
     * Method to be called once frame is completed
     * * Translates IR -> RISC-V and emits
     **/

    public void generateASM() {
        /* Load params on their first use */
        initParameters();

        /* Save return address & control link, mv SP & FP */
        genPrologue();

        genEpilogue();

        if (!isGlobalFrame) {
            fixNonLocals();
        }

        fixLocals();


            /* calls load and saves from global pseudo registers, properly
            loading and saving their values.
             */
        readGlobals();
            /* Save needed regs to memory before & restore them after
            register poisonings (e.g. function calls ) */
        saveVolatiles();

        /* Replace Pseudo-registers with real registers */
        mapRegisters();

        mv_stack.imm = offSetMap.values()
            .stream()
            .min((i,j) -> i-j)
            .orElse(-3*backend.getWordSize()) - 3*backend.getWordSize();


        /* emit actual assembly */
        instructions.forEach(inst -> inst.emit(backend));
    }

    private void initStackVars() {
        if (funcinfo == null)
            return;
        for (StackVarInfo v : funcinfo.getLocals()) {
            PR r = new PR();
            regMap.put(v.getVarName(), r);
            offSetMap.put(r, -backend.getWordSize() * (3 + funcinfo.getVarIndex(v.getVarName())));
            debug_names.put(r, v.getVarName());

            ArrayList<String> info = debug_names.keySet()
              .stream()
              .map(pr -> debug_names.get(pr) + ": " + "offset " + offSetMap.get(pr) + " reg " + pr.toString() + " \n")
              .collect(Collectors.toCollection(ArrayList::new));


            String title = "====== " + (funcinfo == null ? "Global Frame?" : funcinfo.getFuncName()) + "| Stack info======\n";

            info.add(0, title);


//            debug((Object[]) info.toArray(new String[0]));

        }
    }


    private void readGlobals() {
        /* copy list, as we are mutating & iterating simultaneously */
        ArrayList<Instruction> insts_copy = new ArrayList<>(instructions);
        /*  For each instructions which reads some global variable (isDependent) represented by pseudoreg PR ,
            we add a load instruction to get the global from memory beforehand; We use a new pseudo register each load,
            as that's better for register allocation.   */
        CartesianProduct(insts_copy, globalLabels.keySet())
              .filter(Instruction::isDependent)
              .forEach((inst, pr) -> {
                      PR new_pr = new PR();
                      addBefore(inst,
                        new CustomInstructions.LA(new_pr, getGlobalVar(pr), "Loading global variable Location from label"),
                        new LW(new_pr, new_pr, 0, "Loading global variable value")
                      );
                      inst.replaceDependent(pr, new_pr);
              });
        /* For every instruction inst which writes to a  global variable PR ,
         * save said variable afterwords to it's location in memory */
        CartesianProduct(insts_copy, globalLabels.keySet())
              .filter(Instruction::doesModify)
              .forEach((inst, pr) -> {
                      PR tempReg = new PR();
                      addAfter(inst,
                        new CustomInstructions.LA(tempReg, getGlobalVar(pr), "Loading global variable location"),
                        new SW(pr, tempReg, 0, "Loading global variable val")
                      );
              });
    }

    /**
     * push a series of instructions to before the first (which currently exists in frame)
     */
    private void addBefore(Instruction curr, Instruction... newInst) {
        int idx = instructions.indexOf(curr);
        for (int j = newInst.length - 1; j >= 0; j--) {
            instructions.add(idx, newInst[j]);
        }
    }

    /**
     * adds a series of instructions follow the first, added in order passed in
     */
    private void addAfter(Instruction curr, Instruction... newInst) {
        int idx = instructions.indexOf(curr);
        for (int j = newInst.length - 1; j >= 0; j--) {
            instructions.add(idx + 1, newInst[j]);
        }
    }

    private void fixLocals() {
        if (funcinfo == null) { return; }
        /* copy list, as we are mutating & iterating simultaneously */
        ArrayList<Instruction> i_copy = new ArrayList<>(instructions);

        // If an Instruction inst modifies some Local variable PR , perform a save of said PR immediately following inst.
        CartesianProduct(i_copy, funcinfo.getLocals())
              .map(inst -> inst, svi -> regMap.get(svi.getVarName()))
              .filter((inst, pr) -> pr.equals(inst.getModifies().orElse(null))) //
              .forEach((inst, pr) -> {
                        addAfter(inst, new SW(pr, PR.pFP, offSetMap.get(pr), "load nonlocal variable"));
                    });
    }


    private void fixNonLocals() {
        /* copy instructions, as we are mutating & iterating simultaneously */
        ArrayList<Instruction> i_copy = new ArrayList<>(instructions);

        /* For every instruction "inst" which depends on some nonLocal's register "pr" as input,
                insert instructions before "inst" to load the nonlocal from memory beforehand;
                (We replace each instance of pr with a new register for better register allocation)
         */
        CartesianProduct(i_copy, nonLocalVars.keySet())
            .filter(Instruction::isDependent)
            .forEach((inst, pr) -> {
                  PR tempReg = new PR();
                  addBefore( inst,
                      new CustomInstructions.LA(tempReg, CodeGenImpl.displayArray, "load display_array"),
                      new LW(tempReg, tempReg, nonLocalVars.get(pr).depth * 4, "load fp"),
                      new LW(tempReg, tempReg, nonLocalVars.get(pr).position, "load nonlocal variable " + nonLocalVars.get(pr).name));
                  inst.replaceDependent(pr, tempReg);
            });
        /*  For all instructions "inst" which modifies on some nonLocal's register "pr",
                insert instructions after "inst" to save the nonlocal to memory;
         */
        CartesianProduct(i_copy, nonLocalVars.keySet())
            .filter(Instruction::doesModify)
            .forEach((inst, pr) -> {
                  PR tempReg = new PR();
                  addAfter( inst,
                       new CustomInstructions.LA(tempReg, CodeGenImpl.displayArray, "Loading global variable loc"),
                       new LW(tempReg, tempReg, nonLocalVars.get(pr).depth * 4, "Loading global variable val"),
                       new SW(pr, tempReg, nonLocalVars.get(pr).position, "load nonlocal variable"));
                });


    }

    /**
     * Use Liveness analysis to find which variables need to be restored after poisoning
     * e.g.
     * x = 1; y=2; foo(); z = x+1;
     * <p>
     * we need to save & restore X's register due to call to FOO.
     */
    private void saveVolatiles() {
        lifetimes = new LivenessAnalysis(this);
        /* copy list, as we are mutating & iterating simultaneously */
        ArrayList<Instruction> i_copy = new ArrayList<>(instructions);

//        debugLifetimeAnalysis();

        for (Instruction i : i_copy) {
            if (!i.poisonsRegisters())
                continue;

            int pos = i_copy.indexOf(i);
            /* pseudo-registers that are still needed after call (alive on the tail end) */
            HashSet<PR> interferes = lifetimes.getAliveOutOf(pos);

            /* position actual array */
            pos = instructions.indexOf(i);
            /* Save needed registers */

            Collection<PR> iter = interferes;
            iter.addAll(interferes);

            for (PR p : iter) {
                if (p.isConstant() || nonLocalVars.containsKey(p))
                    continue;
                /* parameters already have dedicated location on stack  other variables need a new location */
                if (isParamLoaded.get(p) != null) {
                    assert offSetMap.get(p) != null : "offset map should have all parameters";
                    int off = offSetMap.get(p);
                    instructions.add(pos, new SW(p, PR.pFP, off, "storing variable because we need it after func call: " + isParamLoaded.get(p)));
                } else {
                    /* save location in memory */
                    if (!offSetMap.containsKey(p)) {
                        while (offSetMap.containsValue(stack_pos)) {
                            stack_pos -= backend.getWordSize();
                        }
                        offSetMap.put(p, stack_pos);
                        stack_pos -= backend.getWordSize();
                    }
                    pos = instructions.indexOf(i);
                    String dn = debug_names.get(p);
                    instructions.add(pos, new SW(p, PR.pFP, offSetMap.get(p), "Storing volatile reg we need after function call " + ((dn != null) ? dn : "")));
                    if (debug_names.containsKey(p)) {
                    }
                    //  stack_pos -= backend.getWordSize();
                    /* expand final stack size */
                }
            }
            /* get position AFTER poison instruction  again after everything's added */
            pos = instructions.indexOf(i) + 1;
            /* reload back registers */
            for (PR p : interferes) {
                if (p.isConstant() || nonLocalVars.containsKey(p))
                    continue;
                /* already allocated space for parameters */
                instructions.add(pos, new LW(p, PR.pFP, offSetMap.get(p), "Restore value after function call"));
                pos++;
            }
        }
    }

    /**
     * Returns the index of the instruction which first uses the pseudo-register p,
     * or -1 if that does not occur
     */
    private int firstOccurrence(PR p) {
        return IntStream.range(0, instructions.size())
          .filter(i -> Arrays.asList(getDependentsOf(i)).contains(p)
            || instructions.get(i) instanceof LabelInst
            || instructions.get(i) instanceof sbInst)
          .findFirst()
          .orElse(-1);
    }

    /**
     * returns the index of the first assignment to the pseudo-register p, or
     * INTEGER.MAX_VALUE otherwise
     */
    private int firstDef(PR p) {
        return IntStream.range(0, instructions.size())
          .filter(i -> getModifiesOf(i)
            .map(r -> r.equals(p))
            .orElse(false))
          .findFirst()
          .orElse(Integer.MAX_VALUE);
    }

    /**
     * Initializes parameters right before their detected first use.
     * if we did it all at once, it would be inefficient if we had an excessive number of variables
     */
    private void initParameters() {
        for (PR p : isParamLoaded.keySet()) {
            int first_occurrence = firstOccurrence(p);
            int first_def = firstDef(p);
            if (first_occurrence != -1 && first_def >= first_occurrence) {
                String msg = String.format("Loaded parameter %s for first use", isParamLoaded.get(p));
                instructions.add(first_occurrence, new LW(p, PR.pFP, offSetMap.get(p), msg));
            }
        }
    }

    /**
     * Save return address & control link, mv SP & FP
     */
    private void genPrologue() {
        appendPrologue(new SW(PR.pRA, PR.pSP, -backend.getWordSize(), "Store return address "));
        appendPrologue(new SW(PR.pFP, PR.pSP, -2 * backend.getWordSize(), "Store previous frame pointer "));
        appendPrologue(new MV(PR.pFP, PR.pSP, "Move frame pointer to last item in previous stack"));
        this.mv_stack = new ADDI(PR.pSP, PR.pSP, stack_pos + backend.getWordSize(), "Set new stack pointer to last item in frame");
        appendPrologue(mv_stack);
    }

    private void genEpilogue() {
        appendEpilogue(this.epilogue_label);
        appendEpilogue(new LW(PR.pRA, PR.pFP, -backend.getWordSize(), "Load Return Address"));
        appendEpilogue(new MV(PR.pSP, PR.pFP, "Restore Old Stack Pointer"));
        appendEpilogue(new LW(PR.pFP, PR.pFP, -2 * backend.getWordSize(), "Restore old frame Pointer"));
        if (isGlobalFrame) {
            appendEpilogue(new CustomInstructions.emitEXIT());
        } else {
            appendEpilogue(new ujInst.JR(PR.pRA, "Return from call"));
        }
    }

    public void addReturn(PR return_value) {
        addInst(MV::new, PR.A_0, return_value, "Move return value to a0");
        addInst(J::new, this.epilogue_label, "Jump To Epilogue for Early return");
    }


    //todo migrate all register allocation to another class.
    private void spillRegister(PR p) {
        ArrayList<Instruction> i_copy = new ArrayList<>(instructions);

        if (!offSetMap.containsKey(p)) {
            offSetMap.put(p, stack_pos);
            stack_pos -= backend.getWordSize();
        }
        PR new_reg = p;
        PR old_reg = p;
        for (Instruction i : i_copy) {
            if (Arrays.asList(i.getDependents()).contains(p)) {
                new_reg = new PR();
                PR[] tmp = i.dependents.orElse(null);
                for (int z = 0; z < tmp.length; z++) {
                    if (tmp[z].equals(old_reg)) {
                        tmp[z] = new_reg;
                    }
                }
                int pos = instructions.indexOf(i);
                addInst(pos, LW::new, new_reg, PR.pFP, offSetMap.get(p), "spill restore" );
            }
            if (p.equals(i.getModifies().orElse(null))) {
                int pos = instructions.indexOf(i) + 1;
                i.modifies = Optional.of(p);
                addInst(pos, SW::new, p, PR.pFP, offSetMap.get(p), "spill save");
            }
        }


    }


    /**
     * Returns an Hashmap for mapping registers using coloring, spilling registers when necessary
     */
    private HashMap<PR, Register> genRegisterMap() {
        int regs_needed;
        HashMap<PR, Integer> indexes;

        /* Do Generate Coloring; Spill registers while it doesn't fit into how many we have */
        do {
            indexes = new LivenessAnalysis(this)
                  .toGraph()
                  .minColor();

            regs_needed = (int) indexes.values().stream()
                  .distinct()
                  .count();
            /* more registers than available */
            if (regs_needed > registerArray.length) {
                PR problem_node = new LivenessAnalysis(this)
                      .toGraph()
                      .highestDegree()
                      .orElseThrow(IllegalStateException::new);

                spillRegister(problem_node);
            }
        } while (regs_needed > registerArray.length);

        HashMap<PR, Register> result = new HashMap<>();
        indexes.forEach((key, value) -> result.put(key, registerArray[value]));

        return result;
    }


    /**
     * Map Psuedo-registers to actual registers
     */
    private void mapRegisters() {
        /* graph coloring returns numbers, map that to indexes */
        HashMap<PR, Register> registerHashMap = genRegisterMap();

        /* map constant registers */
        registerHashMap.put(PR.pFP, FP);
        registerHashMap.put(PR.pSP, SP);
        registerHashMap.put(PR.pRA, RA);
        registerHashMap.put(PR.A_0, A0);
        registerHashMap.put(PR.A_1, A1);
        registerHashMap.put(PR.X_0, ZERO);

        instructions.forEach(i -> i.replace(registerHashMap));

        /* We need to remove instructions which received no register for rd (this happens when result is never used)*/

        instructions.removeIf(Instruction::isMarkedForDeletion);
    }

    public int stackNext() {
        return -stack_pos;
    }

    public void add(Instruction i) {
        instructions.add(epilogue_start++, i);
    }

    public void appendPrologue(Instruction i) {
        instructions.add(prologue_end++, i);
        epilogue_start++;
    }

    public void appendEpilogue(Instruction i) {
        instructions.add(i);
    }



    // you can't impose any requirements on constructors, so we use...intrinsics. Yeah I know
    // I know. But java enums are inflexible.

    public <T, U, V> PR push(FourArgConstr<T, U, V> cons, T arg2, U arg3, V arg4) {
        PR result = new PR();
        add(cons.constructor(result, arg2, arg3, arg4));
        return result;
    }

    public <T, U> PR push(ThreeArgConstr<T, U> cons, T arg2, U arg3) {
        PR result = new PR();
        add(cons.constructor(result, arg2, arg3));
        return result;
    }

    public <U> PR push(twoArgConstructor<PR, U> cons, U arg2) {
        PR result = new PR();
        add(cons.constructor(result, arg2));
        return result;
    }

    public PR addFuncCall(String funcName, List<PR> parameters) {
        // logger.info("Starting func call to " + funcName);
        int pos = -1;
        for (PR p : parameters) {
            String param_name = isParamLoaded.get(p);
            String comment = param_name == null
              ? String.format("%s: Store parameter #%s", funcName, -pos)
              : String.format("%s: Store %s as parameter #%s", funcName, param_name, -pos);
            this.add(new SW(p, PR.pSP, pos * backend.getWordSize(), comment));
            pos--;
        }

        int stack_shift = parameters.size() * backend.getWordSize();
        addInst(ADDI::new, PR.pSP, PR.pSP, -stack_shift, "adjust stack for params");
        addInst(PREJAL::new, new Label(funcName), "Call to " + funcName);
        addInst(ADDI::new, PR.pSP, PR.pSP, stack_shift, "adjust stack for params");
        // save a_0 to new pseduo-reg and leave
        return push(MV::new, PR.A_0, "save return form register a0" );
    }

    public PR addFuncCall(String funcName, List<PR> parameters, int depth) {

        int pos =  -1;
//        debug("Starting func call to ", funcName);

        for (PR p : parameters) {
            String param_name = debug_names.get(p);
            String comment = param_name == null
              ? String.format("%s: Store parameter #%s", funcName, -pos)
              : String.format("%s: Store %s as parameter #%s", funcName, param_name, -pos);
            addInst(SW::new, p, PR.pSP, pos * backend.getWordSize(), comment);
            pos--;
        }

        int stack_shift = parameters.size() * backend.getWordSize();

        addInst(ADDI::new, PR.pSP,  PR.pSP, -stack_shift, "adjust stack for params");

        PR array_address = this.push(CustomInstructions.LA::new, CodeGenImpl.displayArray, "load display address");
        PR old_value = push(LW::new, array_address, 0, "read old value");

        addInst(SW::new, PR.pSP, array_address, depth * 4, "update display");
        addInst(PREJAL::new, new Label(funcName), "Call to " + funcName);
        addInst(ADDI::new, PR.pSP, PR.pSP, stack_shift, "adjust stack for params");
        addInst(SW::new, old_value, array_address, 0, "restore old display");


        return push(MV::new, PR.A_0, "save return form register a0" );
    }


    /*use for method calls or errors; in case of method calls, display is always zero */
    public PR addRegisterCall(PR funcLocation, List<PR> parameters) {
        int pos = -1;
        for (PR p : parameters) {
            String param_name = debug_names.get(p);
            String comment = param_name == null
              ? String.format("%s: Store parameter #%s", funcLocation, -pos)
              : String.format("%s: Store %s as parameter #%s", funcLocation, param_name, -pos);
            addInst(SW::new, p, PR.pSP, pos * backend.getWordSize(), comment);
            pos--;
        }

        int stack_shift = parameters.size() * backend.getWordSize();



        PR array_address = this.push(CustomInstructions.LA::new, CodeGenImpl.displayArray, "load display address");
        PR old_value = push(LW::new, array_address, 0, "read old value");

        this.add(new ADDI(PR.pSP, PR.pSP, -stack_shift, "adjust stack for params"));


        addInst(SW::new, PR.pSP, array_address, 0, "update display");

        this.add(new ujInst.JALR(funcLocation, "Register call"));
        this.add(new ADDI(PR.pSP, PR.pSP, stack_shift, "adjust stack for params"));
        addInst(SW::new, old_value, array_address, 0, "restore old display");


        PR p = new PR();
        this.add(new MV(p, PR.A_0, "save return form register a0"));
        return p;
    }

    public void addOrStmt(PR condition, Runnable body, Runnable trueRun) {
        LabelInst trueOr = new LabelInst("$$orTrue" + "_" + lbl_uuid++ + "_");
        LabelInst endOr = new LabelInst("orEnd" + "_" + lbl_uuid++ + "_");
        add(new sbInst.BNEZ(condition, trueOr, "If the first cond is equal to one we short circuit"));
        body.run();
        this.add(new ujInst.J(endOr, "Non-Terminated Value assigned, skip to end"));
        this.add(trueOr);
        trueRun.run();
        this.add(endOr);
    }

    public void addAndStmt(PR condition, Runnable body, Runnable falseRun) {
        LabelInst falseAnd = new LabelInst("$$andFalse" + "_" + lbl_uuid++ + "_");
        LabelInst endAnd = new LabelInst("endAnd" + "_" + lbl_uuid++ + "_");
        add(new sbInst.BEQZ(condition, falseAnd, "If the first cond is False we short circuit"));
        body.run();
        this.add(new ujInst.J(endAnd, "Second Value assigned, skip to end"));
        this.add(falseAnd);
        falseRun.run();
        this.add(endAnd);
    }

    public void addIfStmt(PR condition, Runnable r) {
        LabelInst l = new LabelInst("$$if" + "_" + lbl_uuid++ + "_");
        LabelInst q = new LabelInst("$$if_j" + "_" + lbl_uuid++ + "_");
        add(new sbInst.BNEZ(condition, q, null));
        add(new ujInst.J(l , "jump to end of if stmt"));
        add(q);
        r.run();
        add(l);
    }

    public void addIfFalseStmt(PR condition, Runnable r) {
        LabelInst l = new LabelInst("$$if" + "_" + String.valueOf(lbl_uuid++) +"_");
        add(new sbInst.BNEZ(condition, l, null));
        r.run();
        add(l);
    }
    public void addIfElseStmt(PR condition, Runnable body, Runnable else_body ) {
        LabelInst start_else_lbl = new LabelInst("$$if" + "_" + String.valueOf(lbl_uuid++) +"_");
        LabelInst end_else_lbl = new LabelInst("$$else" + "_" + String.valueOf(lbl_uuid++) +"_");
        add(new sbInst.BEQZ(condition, start_else_lbl, null));
        body.run();
        this.add(new ujInst.J(end_else_lbl, "end of body, skip else"));
        this.add(start_else_lbl);
        else_body.run();
        add(end_else_lbl);
    }

    public void addWhileStmt(Supplier<PR> condition_eval, Runnable r) {
        LabelInst start_label = new LabelInst("$$while_start" + "_" + lbl_uuid++ + "_");
        LabelInst end_label = new LabelInst("$$while_end_inter" + "_" + lbl_uuid++ + "_");
        LabelInst q = new LabelInst("$$while_end" + "_" + lbl_uuid++ + "_");

        add(start_label);
        PR condition = condition_eval.get();
        add(new sbInst.BNEZ(condition, q, null));
        add(new ujInst.J(end_label, "jump to start of while loop"));
        add(q);
        r.run();
        add(new ujInst.J(start_label, "jump to start of while loop"));
        add(end_label);
    }

    public int size() {
        return instructions.size();
    }

    public PR[] getDependentsOf(int i) {
        return instructions.get(i).getDependents();
    }


        /* we need to reval the condition, which may be anonymous - not a variable,
        so we need the actual instruction  - using a Supplier makes it easier */

    public Optional<PR> getModifiesOf(int i) {
        return instructions.get(i).getModifies();
    }

    /* instructions that follow instruction i */
    public Collection<Integer> follows(int i) {
        ArrayList<Integer> res = new ArrayList<>();

        if ((i < size() - 1) && !instructions.get(i).isInterFunctionJump())
            res.add(i + 1);

        instructions.get(i)
              .branchTo()
              .map(instructions::indexOf)
              .ifPresent(res::add);

        return res;
    }

    private void debug(Object... things) {
        /*String s = "";
        for (Object o : things) {
            s += o.toString();
        }
        logger.info(s);*/
    }

    private void debugLifetimeAnalysis() {
      /*  String format = "|%15s|";

        LivenessAnalysis l = new LivenessAnalysis(this);
        String title = "\n======== " + ((funcinfo == null) ? "global" : funcinfo.getFuncName()) + " Liveness Analysis ============ \n";
        ArrayList<String> out = new ArrayList<>();
        out.add(title);
        for (int i = 0; i < instructions.size(); i++) {
            ArrayList<String> sub = new ArrayList<>();
            sub.add(String.format("|%3s|", i + ": "));
            for (PR p : l.getAliveOutOf(i)) {
                sub.add(String.format(format, (debug_names.containsKey(p) ? debug_names.get(p) + " " + " (" + p.toString() + ") " : p.toString()) + ""));
            }
            if (!sub.isEmpty())
                sub.add("\n");
            out.addAll(sub);
        }
        debug(out.toArray(new Object[0]));*/
    }



    @Override
    public Iterator<Instruction> iterator() {
        return instructions.iterator();
    }

    /**
     * returns psuedo register that results from applying @param RInstruction to
     * operand pseudo-registers @param rs0 and @param rs1
     * new pseudo register
     */

    @FunctionalInterface
    public interface ThreeArgConstr<T, U> {
        Instruction constructor(PR rd, T param2, U param3);
    }

    @FunctionalInterface
    public interface FourArgConstr<T, U, V> {
        Instruction constructor(PR rd, T Param2, U parm3, V param4);
    }

    @FunctionalInterface
    public interface twoArgConstructor<T, U> {
        Instruction constructor(T p0, U p1);
    }

    @FunctionalInterface
    public interface oneArgConstructor<T> {
        Instruction constructor( T Param2);
    }

    public <T> void addInst(oneArgConstructor<T> f, T param0) {
        add(f.constructor(param0));
    }

    public <T> void addInst(int idx, oneArgConstructor<T> f, T param0) {
        instructions.add(idx, f.constructor(param0));
    }


    public <T,U> void addInst(twoArgConstructor<T,U> f, T p0, U p1) {
        add(f.constructor(p0, p1));
    }

    public <T,U> void addInst(int idx, twoArgConstructor<T,U> f, T p0, U p1) {
        instructions.add(idx, f.constructor(p0, p1));
    }

    public <T,U> void addInst(ThreeArgConstr<T,U> f, PR rd, T param2, U param3) {
       add(f.constructor(rd, param2, param3));
    }

    public <T,U> void addInst(int idx, ThreeArgConstr<T,U> f, PR rd, T param2, U param3) {
        instructions.add(idx, f.constructor(rd, param2, param3));
    }
    public <T,U,V> void  addInst(FourArgConstr<T,U,V> f, PR rd, T param2, U param3, V param4) {
        add(f.constructor(rd, param2, param3, param4));
    }

    public <T,U,V> void  addInst(int idx, FourArgConstr<T,U,V> f, PR rd, T param2, U param3, V param4) {
        instructions.add(idx, f.constructor(rd, param2, param3, param4));
    }
}
