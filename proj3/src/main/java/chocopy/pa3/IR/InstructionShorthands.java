package chocopy.pa3.IR;

import chocopy.pa3.IR.Instructions.iInst;
import chocopy.pa3.IR.Instructions.rInst;

public class InstructionShorthands {
    public static final Class<rInst.ADD> pADD = rInst.ADD.class;
    public static final Class<iInst.ADDI> pADDI = iInst.ADDI.class;
}
