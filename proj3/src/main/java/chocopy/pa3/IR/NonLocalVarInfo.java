package chocopy.pa3.IR;

import java.util.Objects;

public class NonLocalVarInfo {
    int depth;
    int position;
    String name;

    public NonLocalVarInfo(int depth, int position, String name) {
        this.depth = depth;
        this.position = position;
        this.name = name;
    }

    public int getDepth() {
        return depth;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NonLocalVarInfo)) return false;
        NonLocalVarInfo that = (NonLocalVarInfo) o;
        return depth == that.depth &&
                position == that.position &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(depth, position, name);
    }

    @Override
    public String toString() {
        return "NonLocalVarInfo{" +
                "depth=" + depth +
                ", position=" + position +
                ", name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }
}
