package chocopy.pa3.IR.IRCode;

import chocopy.common.analysis.types.ClassValueType;
import chocopy.common.astnodes.Identifier;
import chocopy.common.astnodes.TypedVar;

public class Helpers {
    public static TypedVar newTypedVar(String name, ClassValueType classType) {
        return new TypedVar(null, null,
                new Identifier(null, null, name),
                null);
    }
}
