package chocopy.pa3.IR.IRCode;

import chocopy.common.codegen.Label;
import chocopy.common.codegen.RiscVBackend;
import chocopy.pa3.IR.Frame;
import chocopy.pa3.IR.Instructions.LI;
import chocopy.pa3.IR.Instructions.iInst;
import chocopy.pa3.IR.Instructions.rInst;
import chocopy.pa3.IR.Instructions.sInst;
import chocopy.pa3.IR.PR;

import java.util.ArrayList;

public class StdLibrary {
    public static Label MEM_COPY = new Label("$$BAD_MEM_COPY");
    public static Label STR_COMP = new Label("$$STR_COMP");

public static void emitMemCpy(RiscVBackend b) {
        ArrayList<String> parameters = new ArrayList<>();

        String dst = "destination_address";
        String src = "source_address";
        String len = "length_in_bytes";


        parameters.add(dst);
        parameters.add(src);
        parameters.add(len);

        b.emitGlobalLabel(MEM_COPY);

        Frame frame = new Frame(parameters, b, null);

        // init  loop variable
        PR i = frame.push(LI::new,0);


        PR new_src = frame.getVar(src);
        PR new_dst =  frame.getVar(dst);
        PR lenn = frame.getVar(len);

         frame.addInst(LI::new,i,0);

        frame.addWhileStmt(() -> {
            return frame.push(rInst.SLT::new, i, lenn, "i < len_words");
        }, () -> {
            PR from_adrr = frame.push(rInst.ADD::new,new_src, i , "sum from address");
            PR to_addr = frame.push(rInst.ADD::new, new_dst, i, "sum to address");
            PR word =  frame.push(iInst.LBU::new, from_adrr,0, "load word to copy from src");
            frame.addInst(sInst.SB::new, word, to_addr, 0, "store word to dst");
            frame.addInst(iInst.ADDI::new, i, i,1, "increment i");
        });

        frame.generateASM();
    }

    public static void emitStringCompare(RiscVBackend b) {
        ArrayList<String> parameters = new ArrayList<>();

        String s1 = "string_1";
        String s2 = "string_2";
        parameters.add(s1);
        parameters.add(s2);

        b.emitGlobalLabel(STR_COMP);
        Frame frame = new Frame(parameters, b, null);


        PR len_1 = frame.push(iInst.LW::new, frame.getVar(s1), 3*b.getWordSize(), "load size");
        PR len_2 = frame.push(iInst.LW::new, frame.getVar(s2), 3*b.getWordSize(), "load size");

        PR diff = frame.push(rInst.SUB::new, len_2, len_1, "String len diff");

        frame.addIfStmt(diff, () -> {
            frame.addReturn(PR.X_0);
        });

        PR i = frame.push(LI::new,0);

        frame.addInst(iInst.ADDI::new, frame.getVar(s1), frame.getVar(s1) ,4*b.getWordSize(), "adjust for start of buffer");
        frame.addInst(iInst.ADDI::new, frame.getVar(s2), frame.getVar(s2) ,4*b.getWordSize(), "adjust for start of buffer");

        frame.addWhileStmt(() -> frame.push(rInst.SLT::new, i, len_1, "loop compare"), () -> {
            PR char_1 = frame.push(iInst.LBU::new, frame.getVar(s1), 0, "load string1 char");
            PR char_2 = frame.push(iInst.LBU::new, frame.getVar(s2), 0, "load string2 char");

            PR diffr = frame.push(rInst.SUB::new, char_2, char_1, "String len diff");

            frame.addIfStmt(diffr, () -> {
                frame.addReturn(PR.X_0);
            });

            frame.addInst(iInst.ADDI::new, frame.getVar(s1), frame.getVar(s1), 1, "increment str1 ptr");
            frame.addInst(iInst.ADDI::new, frame.getVar(s2), frame.getVar(s2), 1, "increment str1 ptr");
            frame.addInst(iInst.ADDI::new, i, i, 1, "increment control var");
        });


        frame.addReturn(frame.push(LI::new, 1));

        frame.generateASM();
    }
}
