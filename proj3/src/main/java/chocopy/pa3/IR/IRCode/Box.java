package chocopy.pa3.IR.IRCode;

import chocopy.common.codegen.Label;
import chocopy.common.codegen.RiscVBackend;
import chocopy.pa3.IR.Frame;
import chocopy.pa3.IR.Instructions.CustomInstructions;
import chocopy.pa3.IR.Instructions.iInst;
import chocopy.pa3.IR.Instructions.pseudoInst.MV;
import chocopy.pa3.IR.Instructions.sInst;
import chocopy.pa3.IR.PR;

import java.util.ArrayList;

public class Box {
    public static final Label intWrapper = new Label("intWrapper");
    public static final Label boolWrapper = new Label("boolWrapper");
    public static final Label alloc2Wrapper = new Label("alloc2Wrapper");
    public static final Label newObject = new Label("newObject");



    /** A wrapper due to alloc breaking calling convention */
    public static void emitAllocWrap(RiscVBackend b, Label l, Label name) {
        ArrayList<String> parameters = new ArrayList<>();
        parameters.add("integer");
        String varName = parameters.get(0);
        b.emitGlobalLabel(name);
        Frame frame = new Frame(parameters, b,null);
        ArrayList<PR> pr =new ArrayList<>();
        frame.add(new CustomInstructions.LA(PR.A_0, l, "" ));
        frame.addFuncCall("alloc",pr);
        frame.add(new sInst.SW(frame.getVar(varName), PR.A_0, 3*b.getWordSize(), "Write old integer value"));
        frame.add(new CustomInstructions.RETURN(PR.A_0));
        frame.generateASM();
    }


    public static void emitNewObject(RiscVBackend b) {
        ArrayList<String> parameters = new ArrayList<>();
        parameters.add("PrototypeLabel");
        String varName = parameters.get(0);
        b.emitGlobalLabel(newObject);
        Frame frame = new Frame(parameters, b,null);
        ArrayList<PR> pr =new ArrayList<>();
        frame.add(new MV(PR.A_0, frame.getVar(varName), "load param into a0" ));
        frame.addFuncCall("alloc",pr);
        frame.add(new CustomInstructions.RETURN(PR.A_0));
        frame.generateASM();
    }


    /** A wrapper due to alloc2 breaking calling convention
     * enhanced such that the second parameter is size + 3 words, instead of size words.
     */
    public static void emitAlloc2Wrap(RiscVBackend b) {
        ArrayList<String> parameters = new ArrayList<>();
        parameters.add("PrototypeLabel");
        String prototypeLabel = parameters.get(0);

        parameters.add("len");
        String len = parameters.get(1);

        b.emitGlobalLabel(alloc2Wrapper);

        Frame frame = new Frame(parameters, b, null);
        ArrayList<PR> pr =new ArrayList<>();


        frame.add(new MV(PR.A_0, frame.getVar(prototypeLabel), "load label into a0" ));
        frame.add(new iInst.ADDI(PR.A_1, frame.getVar(len), 4, "Load len + 3 into a1" ));

        frame.addFuncCall("alloc2",pr);

        frame.add(new CustomInstructions.RETURN(PR.A_0));
        frame.generateASM();
    }



}
