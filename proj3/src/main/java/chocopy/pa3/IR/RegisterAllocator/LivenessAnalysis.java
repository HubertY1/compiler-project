package chocopy.pa3.IR.RegisterAllocator;

import chocopy.pa3.IR.Frame;
import chocopy.pa3.IR.Instruction;
import chocopy.pa3.IR.PR;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Performs Liveness analysis of PR - pesudo-registers
 * for purpose of real registers in their place.
 */
public class LivenessAnalysis {

    Frame ib;
    ArrayList<HashSet<PR>> in;
    ArrayList<HashSet<PR>> in_prime;
    ArrayList<HashSet<PR>> out;
    ArrayList<HashSet<PR>> out_prime;

    public LivenessAnalysis(Frame i) {
        this.ib = i;
        this.in = new ArrayList<>();
        this.in_prime = new ArrayList<>();
        this.out = new ArrayList<>();
        this.out_prime = new ArrayList<>();

        in.ensureCapacity(i.size());
        out.ensureCapacity(i.size());

        for (Instruction unused : i) {
            in.add(new HashSet<>());
            in_prime.add(new HashSet<>());
            out.add(new HashSet<>());
            out_prime.add(new HashSet<>());
        }
        compute();
    }

    /* returns pseudoregisters that are alive leaving an instruction */
    public HashSet<PR> getAliveOutOf(int i) {
        return out.get(i);
    }


    private void compute() {
        // pseudocode -  https://www.cs.colostate.edu/~mstrout/CS553/slides/lecture03.pdf
        do {
            for (int n = 0; n < ib.size(); n++) {
                in_prime.set(n, in.get(n));
                out_prime.set(n, out.get(n));

                // a register that instruction defines if it exists - see pseudocode
                PR n_defines_t = ib.getModifiesOf(n).orElse(null);
                PR n_defines = n_defines_t == null || n_defines_t.isConstant() ? null : n_defines_t;

                // in[n]   = use[n] U (out[n] - { n_defines } )
                HashSet<PR> new_in_n =
                        Stream.of(ib.getDependentsOf(n))
                                .filter(p -> !p.isConstant())
                                .collect(Collectors.toCollection(HashSet::new));

                out.get(n)
                        .stream()
                        .filter(p -> !p.equals(n_defines))
                        .filter(p -> !p.isConstant())
                        .forEach(new_in_n::add);
                in.set(n, new_in_n);

                // out[n]  =  { in[s] |  s in succ[n] }
                out.set(n, ib.follows(n).stream()
                        .map(in::get)
                        .flatMap(Collection::stream)
                        .filter(p -> !p.isConstant())
                        .collect(Collectors.toCollection(HashSet::new)));
            }
            // while not converged
        } while (!(in_prime.equals(in) && out_prime.equals(out)));
    }

    /**
     * Yields the Liveness graph, where two nodes are connected if and only if they have overlapping
     * liftimes
     *
     * @return Liveness graph
     */

    public Graph<PR> toGraph() {
        Graph<PR> result = new Graph<>();
        in.stream().forEach(result::connectAll);
        out.stream().forEach(result::connectAll);
        return result;
    }
}
