package chocopy.pa3.IR.RegisterAllocator;

import java.util.*;

public class Graph<T> {
    public HashMap<T, Node> adjList = new HashMap<>();

    public void connect(T a, T b) {
        if (!adjList.containsKey(a)) {
            Node temp = new Node(a, new HashSet<T>());
            adjList.put(a, temp);
        }
        if (!adjList.containsKey(b)) {
            Node temp = new Node(b, new HashSet<T>());
            adjList.put(b, temp);
        }
        adjList.get(a).neighbors.add(b);
        adjList.get(b).neighbors.add(a);
    }

    public boolean are_connected(T a, T b) {
        return (adjList.containsKey(a) && adjList.get(a).neighbors.contains(b));
    }

    public void connectAll(Collection<T> items) {
        for (T n1 : items) {
            for (T n2 : items) {
                if (!Objects.equals(n1, n2)) {
                    connect(n1, n2);
                } else if (!adjList.containsKey(n1)) {
                    Node temp = new Node(n1, new HashSet<T>());
                    adjList.put(n1, temp);
                }
            }
        }
    }

    public int degree(T a) {
        return !adjList.containsKey(a)
                ? -1
                : adjList.get(a).neighbors.size();
    }

    public Optional<T> highestDegree() {
        return adjList.values()
                .stream()
                .max((t1, t2) -> t1.neighbors.size() - t2.neighbors.size())
                .map(t -> t.reg);
    }

    public T getRandom() {
        return adjList.keySet().toArray((T[]) new Object[0])[(int) new Random().nextInt(adjList.size())];
    }

    //Performs an approimate minimal coloring upon the adjacency list
    //Mutates the adj list to add colors
    public HashMap<T, Integer> minColor() {
        PriorityQueue<Node> pq = new PriorityQueue<Node>(new dSaturComp());
        HashMap<T, Integer> outputColors = new HashMap<>();
        adjList.forEach((key, Node) -> {
            pq.add(Node);
        });
        while (!pq.isEmpty()) {
            Node curr = pq.poll();
            if (curr.color == -1) {
                int col = 0;
                while (curr.illegalColors.contains(col)) {
                    col += 1;
                }
                curr.color = col;
                outputColors.put(curr.reg, col);
                Iterator<T> it = curr.neighbors.iterator();
                while (it.hasNext()) {
                    T neighborId = it.next();
                    Node neighbor = adjList.get(neighborId);
                    //this removes the neighbors so that their updated
                    //values can be reentered to the PQ
                    pq.remove(neighbor);
                    neighbor.saturation += 1;
                    neighbor.illegalColors.add(col);
                    pq.add(neighbor);
                }
            }
        }
        return outputColors;
    }

    @Override
    public String toString() {
        return "Graph{" +
                "adjList=\n" + adjList +
                '}';
    }

    public class Node {
        T reg;
        int color;
        int saturation;
        ArrayList<Integer> illegalColors;
        HashSet<T> neighbors;

        Node(T reg, HashSet<T> neighbors) {
            this.reg = reg;
            this.color = -1;
            this.saturation = 0;
            this.neighbors = neighbors;
            this.illegalColors = new ArrayList<>();

        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return reg.equals(node.reg);
        }

        @Override
        public int hashCode() {
            return Objects.hash(reg);
        }

        @Override
        public String toString() {
            return "Node{" +
                    "reg=" + reg +
                    ", neighbors = " + neighbors.toString() +
                    "}\n";
        }
    }

    class dSaturComp implements Comparator<Node> {
        @Override
        public int compare(Node n1, Node n2) {
            if (n1.saturation < n2.saturation) {
                return 1;
            } else if (n1.saturation > n2.saturation) {
                return -1;
            }
            if (n1.neighbors.size() < n2.neighbors.size()) {
                return 1;
            } else if (n1.neighbors.size() > n2.neighbors.size()) {
                return -1;
            }
            return 0;
        }
    }
}
