package chocopy.pa3.IR;

import chocopy.common.codegen.RiscVBackend;
import chocopy.pa3.IR.Instructions.LabelInst;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;

/**
 * Intermediate representation Base Class
 */
public abstract class Instruction {

    protected Optional<PR> modifies = Optional.empty();
    /**
     * Register allocation will NOT assign registers to pseudo-registers
     * who's assigned value goes unused. e.g. set x:int = 2, then never using x.
     * Solution:
     * We can just delete the instruction, as all RISC-V
     * assignment instructions are pure outside of `rd`. Values that only appear in
     * rd are the only ones where this would occur
     * (wouldn't happen in sw, as it's not an assignment instruction).
     */
    protected boolean markedForDeletion = false;
    /**
     * psuedo registers the instruction depends on
     * We specifically don't provide a blank implementaiton for safety
     */
    Optional<PR[]> dependents = Optional.empty();
    Optional<RiscVBackend.Register[]> registers = Optional.empty();

    /**
     * If instruction poisons registers (func calls) - abstract so we don't forget to overrride
     */

    public abstract boolean poisonsRegisters();

    public boolean isDependent(PR p) {
        return dependents.map(r-> Arrays.asList(r).contains(p)).orElse(false);
    }
    
    /** replaces all occurnces of dependent before with dependent after */
    public void replaceDependent(PR before, PR after) {
        PR[] deps = dependents.orElse(null);
        if (deps == null)
            return;
        for (int i =0; i <  deps.length; i++) {
            if (deps[i].equals(before))
                deps[i] = after;
        }
    }

    public Optional<PR> getModifies() {
        return Optional.empty();
    }

    public boolean isMarkedForDeletion() {
        return markedForDeletion;
    }

    public boolean doesModify(PR p) {
        return getModifies().map(m -> m.equals(p)).orElse(false);
    }

    public RiscVBackend.Register[] getRegisters() {
        assert registers.isPresent() : "Did not replace registers";
        return registers.orElse(null);
    }

    public RiscVBackend.Register getRD() {
        return null;
    }

    public PR[] getDependents() {
        assert dependents.isPresent() : "dependents were not present When Dependents called on instruction:"
                + toString() + " of class " + this.getClass().getSimpleName();
        return dependents.orElse(new PR[0]);
    }

    public void setDependents(PR... deps) {
        this.dependents = Optional.of(deps);
    }

    public void replace(HashMap<PR, RiscVBackend.Register> m) {
        assert dependents.isPresent() : "dependents were not present When ReplaceDependents called on instruction:"
                + toString() + " of class " + this.getClass().getSimpleName();

        registers = Optional.of(Arrays.stream(getDependents())
                .map(m::get)
                .toArray(RiscVBackend.Register[]::new));
    }


    public boolean isInterFunctionJump() {
        return false;
    }

    /** instruction that this instruction may branch to */
    public Optional<LabelInst> branchTo() {
        return Optional.empty();
    }

    /**
     * Emit assembly instruction after registers are replaced
     */
    public abstract void emit(RiscVBackend b);
}
