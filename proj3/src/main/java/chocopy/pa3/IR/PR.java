package chocopy.pa3.IR;

import chocopy.common.astnodes.TypeAnnotation;


/**
 * Instead of registers, the intermediate representation uses
 * Bindings. The difference is we pretened we have unlimited bindings.
 */
public class PR {


    /** constant mapping to their literal registers, p-prefix prevents namespace  and confusion with Risc-V,
     *  (p is for pseudo) - pseudo monster
     */
    public static final PR pRA = new PR(-1);
    public static final PR CONTROL_LINK = new PR(-2);
    public static final PR pSL = new PR(-3);
    public static final PR pFP = new PR(-4);
    public static final PR pSP = new PR(-5);
    public static final PR X_0 = new PR(-6);
    public static final PR A_0 = new PR(-7);
    public static final PR A_1 = new PR(-8);
    static int Next_UUID = 0;
    public TypeAnnotation type;
    /**
     * Unique label
     */
    public int UUID;
    /**
     * name for reuse, if variable
     */
    String identifier = null;


    public PR() {
        this.UUID = Next_UUID++;
    }

    public PR(String n) {
        this.UUID = Next_UUID++;
        this.identifier = n;
    }



    private PR(int custom_id) {
        this.UUID = custom_id;
    }

    public static int getNext_UUID() {
        return Next_UUID;
    }

    public boolean isConstant() {
        return this.equals(pSP) ||
          this.equals(pFP) ||
          this.equals(X_0) ||
          this.equals(A_0)||
          this.equals(A_1)||
          this.equals(pRA);
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof PR
                && this.UUID == ((PR) other).UUID;
    }

    @Override
    public int hashCode() {
        return UUID;
    }

    @Override
    public String toString() {
        return "PR{" +
                "UUID=" + UUID +
                '}';
    }
}

