package chocopy.pa3.IR.Instructions;

import chocopy.common.codegen.RiscVBackend;
import chocopy.common.codegen.RiscVBackend.Register;
import chocopy.pa3.IR.AssignInstruction;
import chocopy.pa3.IR.PR;

/**
 * essentially an abstract class for R-instrucitons
 */
public class rInst extends AssignInstruction {
    protected PR rd, rs0, rs1;
    protected int imm;
    protected String comment;

    /**
     * method that calls emitter; should be changed by inherited class
     * we could make AssignInstruction abstract, but this is slightly cleaner
     */
    protected rInstFunc emitter = (d, rd, rs0, rs1, comment) -> {
        assert false : "Functionalthis should have been implemented for class: " + this.getClass().getSimpleName();
    };

    public rInst(PR rd, PR rs0, PR rs1, String com) {
        this.rd = rd;
        this.rs0 = rs0;
        this.rs1 = rs1;
        this.comment = com;
        // the pseudoregisters passed in/used. Provides info to register allocator.
        this.setDependents(rs0, rs1);
        // the pseudoregisters passed modified. Provided to register allocator
        this.setModifies(rd);
    }


    /**
     * If instruction poisons registers
     */
    @Override
    public boolean poisonsRegisters() {
        return false;
    }

    @Override
    public void emit(RiscVBackend b) {
        // These are the actual registers, use getRegisters.
        // they are filled during a pass, and returned in the array
        // in the same order set when setDependents was called
        RiscVBackend.Register[] regs = getRegisters();
        //Get RD does the same thing, but for registers you assign to.
        emitter.apply(b, getRD(), regs[0], regs[1], comment);
    }

    /**
     * R - instructions must extend with functional interface to emitter (); Implementation provided
     */
    @FunctionalInterface
    interface rInstFunc {
        void apply(RiscVBackend b, Register r0, Register r1, Register r2, String comment);
    }

    public static class ADD extends rInst {
        public ADD(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = (b, r0, r1, r2, s) -> {
                b.emitADD(r0, r1, r2, s);
            };
        }
    }

    public static class SUB extends rInst {
        public SUB(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = RiscVBackend::emitSUB;
        }
    }

    public static class MUL extends rInst {
        public MUL(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = RiscVBackend::emitMUL;
        }
    }

    public static class DIV extends rInst {
        public DIV(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = RiscVBackend::emitDIV;
        }
    }

    public static class AND extends rInst {
        public AND(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = RiscVBackend::emitAND;
        }
    }

    public static class OR extends rInst {
        public OR(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = RiscVBackend::emitOR;
        }
    }

    public static class SLT extends rInst {
        public SLT(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = RiscVBackend::emitSLT;
        }
    }

    public static class SLL extends rInst {
        public SLL(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = RiscVBackend::emitSLL;
        }
    }
/*
    public static class MV extends rInst {
        //fixme
        public MV(PR rd, PR rs0, String com) {
            super(rd, rs0, PR.X_0, com);
            emitter = (back, rd0, rs_0, _r, comm) -> back.emitMV(rd0, rs_0, comm);
        }

        @Override
        public String toString() {
            return "MV{}" + rd.toString() + " " + rs0.toString();
        }
    }*/

    public static class SRL extends rInst {
        public SRL(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = RiscVBackend::emitSRL;
        }
    }

    public static class SRA extends rInst {
        public SRA(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = RiscVBackend::emitSRA;
        }
    }

    public static class XOR extends rInst {
        public XOR(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = RiscVBackend::emitXOR;
        }
    }

    public static class REM extends rInst {
        public REM(PR rd, PR rs0, PR rs1, String com) {
            super(rd, rs0, rs1, com);
            emitter = RiscVBackend::emitREM;
        }
    }
}
