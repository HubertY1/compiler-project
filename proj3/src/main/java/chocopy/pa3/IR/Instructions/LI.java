package chocopy.pa3.IR.Instructions;

import chocopy.common.codegen.RiscVBackend;
import chocopy.pa3.IR.AssignInstruction;
import chocopy.pa3.IR.PR;

public class LI extends AssignInstruction {
    int imm;

    public LI(PR to_register, int i) {
        //  modifies = Optional.of(to_register);
        // if (to_register.type != Type.INT_TYPE) {
        //    throw new IllegalArgumentException();
        // }
        imm = i;
        setModifies(to_register);
        setDependents();

    }

    /**
     * If instruction poisons registers
     */
    @Override
    public boolean poisonsRegisters() {
        return false;
    }

    @Override
    public void emit(RiscVBackend b) {
        b.emitLI(getRD(), imm, "");
    }
}
