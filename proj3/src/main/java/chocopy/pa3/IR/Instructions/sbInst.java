package chocopy.pa3.IR.Instructions;

import chocopy.common.codegen.RiscVBackend;
import chocopy.pa3.IR.Instruction;
import chocopy.pa3.IR.PR;

import java.util.Optional;

public abstract class sbInst extends Instruction {

    protected PR rs0, rs1;
    protected LabelInst l;
    protected String comment;

    /**
     * method that calls emitter; should be changed by inherited class
     * we could make AssignInstruction abstract, but this is slightly cleaner
     */


    public sbInst(PR rs0, PR rs1, LabelInst l ,String com) {
        this.rs0 = rs0;
        this.rs1 = rs1;
        this.l = l;
        this.comment = com;
        // the pseudoregisters passed in/used. Provides info to register allocator.
        this.setDependents(rs0, rs1);
    }

    @Override
    public Optional<LabelInst> branchTo() {
        return Optional.of(l);
    }





    @Override
    public boolean poisonsRegisters() {
        return false;
    }

    public static class BNEZ extends sbInst {
        public BNEZ(PR rs0, LabelInst l, String comment) {
            super(rs0, PR.X_0, l, comment);
        }

        @Override
        public void emit(RiscVBackend b) {
            b.emitBNEZ(getRegisters()[0], l.lblName, comment);
        }
    }


    public static class BEQZ extends sbInst {
        public BEQZ(PR rs0, LabelInst l, String comment) {
            super(rs0, PR.X_0, l, comment);
        }

        @Override
        public void emit(RiscVBackend b) {
            b.emitBEQZ(getRegisters()[0], l.lblName, comment);
        }
    }


}
