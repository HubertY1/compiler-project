package chocopy.pa3.IR.Instructions;

import chocopy.common.codegen.RiscVBackend;
import chocopy.pa3.IR.Instruction;
import chocopy.pa3.IR.PR;

import java.util.Optional;

public abstract class ujInst extends Instruction {


    protected String comment;


    /**
     * method that calls emitter; should be changed by inherited class
     * we could make AssignInstruction abstract, but this is slightly cleaner
     */


    public ujInst() {

    }


    @Override
    public boolean poisonsRegisters() {
        return false;
    }

    public static class J extends ujInst {
        protected LabelInst l;


        public J(LabelInst l, String comment) {
            super();
            this.l = l;
            this.comment = comment;
            // the pseudoregisters passed in/used. Provides info to register allocator.
            this.setDependents();
        }


        @Override
        public boolean isInterFunctionJump() {
            return true;
        }

        @Override
        public Optional<LabelInst> branchTo() {
            return Optional.of(l);
        }

        @Override
        public void emit(RiscVBackend b) {
            b.emitJ(l.lblName, null);
        }
    }

    public static class JR extends ujInst {
        PR address;
        public JR(PR address, String comment) {
            super();
            this.comment = comment;
            this.address = address;
            this.setDependents(this.address);
        }

        /** Used only for returns; we "poison" the registers so non-locals/globals
         * are saved
         * @return true
         */
        @Override
        public boolean poisonsRegisters() {
            return true;
        }

        @Override
        public void emit(RiscVBackend b) {
            b.emitJR(getRegisters()[0], comment);
        }
    }


    public static class JALR extends ujInst {
        PR address;
        public JALR(PR address, String comment) {
            super();
            this.comment = comment;
            this.address = address;
            this.setDependents(this.address);
        }

        /** Used only for returns; we "poison" the registers so non-locals/globals
         * are saved
         * @return true
         */
        @Override
        public boolean poisonsRegisters() {
            return true;
        }

        @Override
        public void emit(RiscVBackend b) {
            b.emitJALR(getRegisters()[0], comment);
        }
    }



}
