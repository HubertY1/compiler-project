package chocopy.pa3.IR.Instructions;


import chocopy.common.codegen.RiscVBackend;
import chocopy.common.codegen.RiscVBackend.Register;
import chocopy.pa3.IR.AssignInstruction;
import chocopy.pa3.IR.PR;

/**
 * essentially an abstract class for R-instrucitons
 */
public class iInst extends AssignInstruction {
    protected PR rd, rs0;
    public int imm;
    protected String comment;

    /**
     * method that calls emitter; should be changed by inherited class
     * we could make AssignInstruction abstract, but this is slightly cleaner
     */
    protected iInstFunc emitter = (d, rd, rs0, imm, comment) -> {
        assert false : "Functionalshould have been implemented for class: " + this.getClass().getSimpleName();
    };

    public iInst(PR rd, PR rs0, int imm, String com) {
        this.rd = rd;
        this.rs0 = rs0;
        this.imm = imm;
        this.comment = com;
        // the pseudoregisters passed in/used. Provides info to register allocator.
        this.setDependents(this.rs0);
        // the pseudoregisters passed modified. Provided to register allocator
        this.setModifies(rd);
    }

    @Override
    public boolean poisonsRegisters() {
        return false;
    }

    @Override
    public void emit(RiscVBackend b) {
        // These are the actual registers, use getRegisters.
        // they are filled during a pass, and returned in the array
        // in the same order set when setDependents was called
        RiscVBackend.Register[] regs = getRegisters();
        //Get RD does the same thing, but for registers you assign to.
        emitter.apply(b, getRD(), regs[0], imm, comment);
    }

    /**
    /**s
     * I - instructions must extend with functional interface to emitter ()
     */
    @FunctionalInterface
    interface iInstFunc {
        void apply(RiscVBackend b, Register rd_0, Register rs_0, int imm, String Comment);
    }

    public static class LI extends iInst {
        public LI(PR rd, int imm, String com) {
          super(rd, PR.X_0, imm, com);
          emitter = (back, rd2, _e, imm2, comm) -> back.emitLI(rd2, imm2, comm);
        }
        //fixme null above
    }

    public static class LB extends iInst {
        public LB(PR rd, PR rs0, int imm, String com) {
            super(rd, rs0, imm, com);
            emitter = RiscVBackend::emitLB;
        }
    }

    public static class LBU extends iInst {
        public LBU(PR rd, PR rs0, int imm, String com) {
            super(rd, rs0, imm, com);
            emitter = RiscVBackend::emitLBU;
        }
    }

    public static class SLLI extends iInst {
        public SLLI(PR rd, PR rs0, int imm, String com) {
            super(rd, rs0, imm, com);
            emitter = RiscVBackend::emitSLLI;
        }
    }

    public static class SRLI extends iInst {
        public SRLI(PR rd, PR rs0, int imm, String com) {
            super(rd, rs0, imm, com);
            emitter = RiscVBackend::emitSRLI;
        }
    }

    public static class ADDI extends iInst {

        public ADDI(PR rd, PR rs0, int imm, String com) {
            super(rd, rs0, imm, com);
            emitter = (b, r0, r1, im, s) -> {
                b.emitADDI(r0, r1, im, s);
            };
        }
    }

    public static class XORI extends iInst {
        public XORI(PR rd, PR rs0, int imm, String com) {
            super(rd, rs0, imm, com);
            emitter = RiscVBackend::emitXORI;
        }
    }

    public static class ORI extends iInst {
        public ORI(PR rd, PR rs0, int imm, String com) {
            super(rd, rs0, imm, com);
            emitter = RiscVBackend::emitORI;
        }
    }

    public static class ANDI extends iInst {
        public ANDI(PR rd, PR rs0, int imm, String com) {
            super(rd, rs0, imm, com);
            emitter = RiscVBackend::emitANDI;
        }
    }

   /* public class SLTI extends iInst {
        iInstFunc emitter = RiscVBackend::emitSLTI;
        public SLTI(PR rd, PR rs0, int imm, String com) { super(rd, rs0, imm, com); }
    }*/

    public static class LW extends iInst {
        public LW(PR rd, PR rs0, int imm, String com) {
            super(rd, rs0, imm, com);
            emitter = RiscVBackend::emitLW;
        }
    }
}
