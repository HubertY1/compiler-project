package chocopy.pa3.IR.Instructions;


import chocopy.common.codegen.Label;
import chocopy.common.codegen.RiscVBackend;
import chocopy.pa3.IR.Instruction;
import chocopy.pa3.IR.PR;

public class LabelInst extends Instruction {
    Label lblName;

    public LabelInst(String labelname) {
        lblName = new Label(labelname);
    }
    public LabelInst(Label label) {
        lblName = label;
    }

    @Override
    public boolean poisonsRegisters() {
        return false;
    }

    @Override
    public void emit(RiscVBackend b) {
        b.emitLocalLabel(lblName, null );
    }
}
