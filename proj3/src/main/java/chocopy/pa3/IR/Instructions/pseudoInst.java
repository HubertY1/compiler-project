package chocopy.pa3.IR.Instructions;

import chocopy.common.codegen.RiscVBackend;
import chocopy.common.codegen.RiscVBackend.Register;
import chocopy.pa3.IR.AssignInstruction;
import chocopy.pa3.IR.PR;

public class pseudoInst extends AssignInstruction {
    protected PR rd, rs0;
    protected String comment;

    /**
     * method that calls emitter; should be changed by inherited class
     * we could make AssignInstruction abstract, but this is slightly cleaner
     */
    protected pseudoInstFunc emitter = (d, rd, rs0, comment) -> {
        assert false : "Functional this should have been implemented for class: " + this.getClass().getSimpleName();
    };

    public pseudoInst(PR rd, PR rs0, String com) {
        this.rd = rd;
        this.rs0 = rs0;
        this.comment = com;
        // the pseudoregisters passed in/used. Provides info to register allocator.
        this.setDependents(rs0);
        // the pseudoregisters passed modified. Provided to register allocator
        this.setModifies(rd);
    }
    /**
     * If instruction poisons registers
     */
    @Override
    public boolean poisonsRegisters() {
        return false;
    }

    @Override
    public void emit(RiscVBackend b) {
        // These are the actual registers, use getRegisters.
        // they are filled during a pass, and returned in the array
        // in the same order set when setDependents was called
        RiscVBackend.Register[] regs = getRegisters();
        //Get RD does the same thing, but for registers you assign to.
        emitter.apply(b, getRD(), regs[0], comment);
    }

    /**
     * R - instructions must extend with functional interface to emitter (); Implementation provided
     */
    @FunctionalInterface
    interface pseudoInstFunc {
        void apply(RiscVBackend b, RiscVBackend.Register r0, Register r1, String comment);
    }

    public static class MV extends pseudoInst {
        public MV(PR rd, PR rs0, String com) {
            super(rd, rs0, com);
            emitter = (b, r0, r1, s) -> {
                b.emitMV(r0, r1, s);
            };
        }
        @Override
        public String toString() {
            return "MV{}" + rd.toString() + " " + rs0.toString();
        }
    }

    public static class NEG extends pseudoInst {
        public NEG(PR rd, PR rs0, String com) {
            super(rd, rs0, com);
            emitter = (b, r0, r1, s) -> {
                b.emitSUB(r0, Register.ZERO, r1,  s);
            };
        }
    }

    public static class NOT extends pseudoInst {
        public NOT(PR rd, PR rs0, String com) {
            super(rd, rs0, com);
            emitter = (b, r0, r1, s) -> {
                b.emitXORI(r0, r1, -1,  s);
            };
        }
    }

    public static class SEQZ extends pseudoInst {
        public SEQZ(PR rd, PR rs0, String com) {
            super(rd, rs0, com);
            emitter = (b, r0, r1, s) -> {
                b.emitSEQZ(r0, r1, s);
            };
        }
    }
    public static class SNEZ extends pseudoInst {
        public SNEZ(PR rd, PR rs0, String com) {
            super(rd, rs0, com);
            emitter = (b, r0, r1, s) -> {
                b.emitSNEZ(r0, r1, s);
            };
        }
    }
}
