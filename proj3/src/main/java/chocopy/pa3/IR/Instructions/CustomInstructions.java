package chocopy.pa3.IR.Instructions;

import chocopy.common.codegen.Label;
import chocopy.common.codegen.RiscVBackend;
import chocopy.pa3.IR.AssignInstruction;
import chocopy.pa3.IR.Instruction;
import chocopy.pa3.IR.PR;

import java.util.HashMap;

import static chocopy.common.codegen.RiscVBackend.Register.*;

public abstract class CustomInstructions {


    /**
     * moves  value in return_reg to A_0, and exists. Doesn't poison registers due to exiting.
     */
    public static class RETURN extends AssignInstruction {
        rInst.ADD mv_inst;

        public RETURN(PR return_reg) {
            this.setDependents(return_reg);
            PR old_FP = new PR();
            this.setModifies(old_FP);
            mv_inst = new rInst.ADD(PR.A_0, return_reg, PR.X_0, "Store return value");
        }

        @Override
        public boolean poisonsRegisters() {
            return false;
        }

        @Override
        public void replace(HashMap<PR, RiscVBackend.Register> m) {
            super.replace(m);
            mv_inst.replace(m);
            markedForDeletion = false;
        }

        @Override
        public void emit(RiscVBackend b) {
            b.emitLW(RA, FP, -b.getWordSize(), "Load return address");
            b.emitMV(RiscVBackend.Register.SP, FP, "Restore sp");
            mv_inst.emit(b);
            b.emitLW(FP, FP, -2 * b.getWordSize(), "");
            b.emitJR(RiscVBackend.Register.RA, null);
        }
    }

    /** An instruction that is not meant for general use, as it does not conform to calling convention.
     * or provideany safety
     * As of writing, used for alloc's breaking of calling convention,
     * and as a sub instruction of FUNC_CALL  */
    public static class PREJAL extends AssignInstruction { // fixme inherit from something else?
        Label l;
        String comment;

        public PREJAL(Label label, String comment) {
            this.l = label;
            this.comment = comment;
            this.setDependents();
            this.setModifies(PR.A_0);
        }

        @Override
        public boolean poisonsRegisters() {
            return true;
        }

        @Override
        public void emit(RiscVBackend b) {
            b.emitJAL(l, comment);
        }
    }

    public static class emitEXIT extends Instruction {

        @Override
        public boolean poisonsRegisters() {
            return false;
        }
//fixme magic number
        @Override
        public void emit(RiscVBackend b) {
            b.emitLI(A0, 10, "Code for ecall: exit");
            b.emitEcall(null);
        }
    }

    public static class LA extends AssignInstruction { // fixme inherit from something else?
        Label l;
        String comment;
        PR rd ;

        public LA(PR rd, Label label, String comment) {
            this.l = label;
            this.comment = comment;
            this.rd = rd;
            this.setDependents();
            this.setModifies(rd);
        }

        @Override
        public boolean poisonsRegisters() {
            return false;
        }

        @Override
        public void emit(RiscVBackend b) {
            b.emitLA( getRD(), l, comment);
        }
    }
}
