package chocopy.pa3.IR.Instructions;


import chocopy.common.codegen.Label;
import chocopy.common.codegen.RiscVBackend;
import chocopy.common.codegen.RiscVBackend.Register;
import chocopy.pa3.IR.AssignInstruction;
import chocopy.pa3.IR.PR;

/**
 * essentially an abstract class for S-instrucitons
 */
public class sInst extends AssignInstruction {
    protected PR rs0, rs1;
    protected int imm;
    protected String comment;

    /**
     * method that calls emitter; should be changed by inherited class
     * we could make sInst abstract, but this is slightly cleaner
     */
    protected sInstFunc emitter = (d, rs0, rs1, imm, comment) -> {
        assert false : "Functional this should have been implemented for class: " + this.getClass().getSimpleName();
    };

    public sInst(PR rs0, PR rs1, int imm, String com) {
        this.rs0 = rs0;
        this.rs1 = rs1;
        this.imm = imm;
        this.comment = com;
        // the pseudoregisters passed in/used. Provides info to register allocator.
        this.setDependents(this.rs0, this.rs1);
        this.setModifies(PR.X_0);
        // the pseudoregisters passed modified. Provided to register allocator
    }

    @Override
    public boolean poisonsRegisters() {
        return false;
    }

    @Override
    public void emit(RiscVBackend b) {
        // These are the actual registers, use getRegisters.
        // they are filled during a pass, and returned in the array
        // in the same order set when setDependents was called
        RiscVBackend.Register[] regs = getRegisters();
        //Get RD does the same thing, but for registers you assign to.
        emitter.apply(b, regs[0], regs[1], imm, comment);
    }

    @FunctionalInterface
    interface sInstFunc {
        void apply(RiscVBackend b, Register rs0, Register rs1, int imm, String Comment);
    }

    public static class SB extends sInst {
        public SB(PR rs0, PR rs1, int imm, String com) {
            super(rs0, rs1, imm, com);
            emitter = RiscVBackend::emitSB;
        }
    }

    public static class SW extends sInst {
        public SW(PR rs0, PR rs1, int imm, String com) {
            super(rs0, rs1, imm, com);
            emitter = RiscVBackend::emitSW;
        }
    }


}