package chocopy.pa3;

import chocopy.common.analysis.AbstractNodeAnalyzer;
import chocopy.common.analysis.SymbolTable;
import chocopy.common.analysis.types.Type;
import chocopy.common.analysis.types.ValueType;
import chocopy.common.astnodes.*;
import chocopy.common.codegen.*;
import chocopy.pa3.IR.Frame;
import chocopy.pa3.IR.IRCode.Box;
import chocopy.pa3.IR.IRCode.StdLibrary;
import chocopy.pa3.IR.Instructions.*;
import chocopy.pa3.IR.Instructions.CustomInstructions.LA;
import chocopy.pa3.IR.Instructions.CustomInstructions.PREJAL;
import chocopy.pa3.IR.Instructions.pseudoInst.MV;
import chocopy.pa3.IR.Instructions.pseudoInst.SEQZ;
import chocopy.pa3.IR.Instructions.rInst.*;
import chocopy.pa3.IR.PR;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static chocopy.common.analysis.types.Type.*;
import static chocopy.common.codegen.RiscVBackend.Register.A0;
import static chocopy.common.codegen.RiscVBackend.Register.A1;
import static chocopy.pa3.IR.IRCode.Box.*;
import static chocopy.pa3.IR.JavaDeficit.zipStream;
import static chocopy.pa3.IR.PR.X_0;

/**
 * This is where the main implementation of PA3 will live.
 * <p>
 * A large part of the functionality has already been implemented in the base
 * class, CodeGenBase. Make sure to read through that class, since you will want
 * to use many of its fields and utility methods in this class when emitting
 * code.
 * <p>
 * Also read the PDF spec for details on what the base class does and what APIs
 * it exposes for its sub-class (this one). Of particular importance is knowing
 * what all the SymbolInfo classes contain.
 */
public class CodeGenImpl extends CodeGenBase {

    /**
     * Operation on None.
     */
    private final Label errorNone = new Label("error.None");
    /**
     * Division by zero.
     */
    private final Label errorDiv = new Label("error.Div");
    /**
     * Index out of bounds.
     */
    private final Label errorOob = new Label("error.OOB");
    /**
     * Offset for accessing object attributes from object layout
     */
    private final Integer objectIndexOffset = 3;

    Logger logger = Logger.getLogger(CodeGenImpl.class.getName());

    /**
     * A code generator emitting instructions to BACKEND.
     */
    public CodeGenImpl(RiscVBackend backend) {
        super(backend);
    }

    /**
     * Emits the top level of the program.
     * <p>
     * This method is invoked exactly once, and is surrounded by some boilerplate
     * code that: (1) initializes the heap before the top-level begins and (2) exits
     * after the top-level ends.
     * <p>
     * You only need to generate code for statements.
     *
     * @param statements top level statements
     */
    @Override
    protected void emitTopLevel(List<Stmt> statements) {
        StmtAnalyzer stmtAnalyzer = new StmtAnalyzer(null);
        for (Stmt stmt : statements) {
            stmt.dispatch(stmtAnalyzer);
        }
        stmtAnalyzer.frame.setGlobalFrame();
        stmtAnalyzer.frame.generateASM();
        backend.emitLI(A0, EXIT_ECALL, "Code for ecall: exit");
        backend.emitEcall(null);

    }

    /**
     * Emits the code for a function described by FUNCINFO.
     * <p>
     * This method is invoked once per function and method definition. At the code
     * generation stage, nested functions are emitted as separate functions of their
     * own. So if function `bar` is nested within function `foo`, you only emit
     * `foo`'s code for `foo` and only emit `bar`'s code for `bar`.
     */
    @Override
    protected void emitUserDefinedFunction(FuncInfo funcInfo) {
        backend.emitGlobalLabel(funcInfo.getCodeLabel());
        StmtAnalyzer stmtAnalyzer = new StmtAnalyzer(funcInfo);

        for (Stmt stmt : funcInfo.getStatements()) {
            stmt.dispatch(stmtAnalyzer);
        }

        stmtAnalyzer.frame.generateASM();
        // backend.emitMV(A0, ZERO, "Returning None implicitly");
        // backend.emitLocalLabel(stmtAnalyzer.epilogue, "Epilogue");
        // backend.emitJR(RA, "Return to caller");
    }

    /**
     * Emits custom code in the CODE segment.
     * <p>
     * This method is called after emitting the top level and the function bodies
     * for each function.
     * <p>
     * You can use this method to emit anything you want outside of the top level or
     * functions, e.g. custom routines that you may want to call from within your
     * code to do common tasks. This is not strictly needed. You might not modify
     * this at all and still complete the assignment.
     * <p>
     * To start you off, here is an implementation of three routines that will be
     * commonly needed from within the code you will generate for statements.
     * <p>
     * The routines are error handlers for operations on None, index out of bounds,
     * and division by zero. They never return to their caller. Just jump to one of
     * these routines to throw an error and exit the program. For example, to throw
     * an OOB error: backend.emitJ(errorOob, "Go to out-of-bounds error and abort");
     */
    @Override
    protected void emitCustomCode() {
        emitErrorFunc(errorNone, 4, "Operation on None");
        emitErrorFunc(errorDiv, 2, "Division by zero");
        emitErrorFunc(errorOob, 3, "Index out of bounds");

        Box.emitAllocWrap(backend, intClass.getPrototypeLabel(), intWrapper);
        Box.emitAllocWrap(backend, boolClass.getPrototypeLabel(), boolWrapper);
        Box.emitAlloc2Wrap(backend);
        Box.emitNewObject(backend);

        StdLibrary.emitMemCpy(backend);
        StdLibrary.emitStringCompare(backend);

        emitArray(256);
    }

    public static Label displayArray = new Label("$$DisplayArray");

    void emitArray(int size) {
        backend.emitGlobalLabel(displayArray);
        for (int i = 0; i < size; i++)
            backend.emitWordLiteral(0, null);
    }

    /**
     * Emit an error routine labeled ERRLABEL that aborts with message MSG.
     */
    private void emitErrorFunc(Label errLabel, int errNum, String msg) {
        backend.emitGlobalLabel(errLabel);
        backend.emitLI(A0, errNum, "Exit code for: " + msg);
        backend.emitLA(A1, constants.getStrConstant(msg), "Load error message as str");
        backend.emitADDI(A1, A1, getAttrOffset(strClass, "__str__"), "Load address of attribute __str__");
        backend.emitJ(abortLabel, "Abort");
    }

    /**
     * An analyzer that encapsulates code generation for statments.
     */
    private class StmtAnalyzer extends AbstractNodeAnalyzer<PR> {
        /*
         * The symbol table has all the info you need to determine what a given
         * identifier 'x' in the current scope is. You can use it as follows: SymbolInfo
         * x = sym.get("x");
         *
         * A SymbolInfo can be one the following: - ClassInfo: a descriptor for classes
         * - FuncInfo: a descriptor for functions/methods - AttrInfo: a descriptor for
         * attributes - GlobalVarInfo: a descriptor for global variables - StackVarInfo:
         * a descriptor for variables allocated on the stack, such as locals and
         * parameters
         *
         * Since the input program is assumed to be semantically valid and well-typed at
         * this stage, you can always assume that the symbol table contains valid
         * information. For example, in an expression `foo()` you KNOW that
         * sym.get("foo") will either be a FuncInfo or ClassInfo, but not any of the
         * other infos and never null.
         *
         * The symbol table in funcInfo has already been populated in the base class:
         * CodeGenBase. You do not need to add anything to the symbol table. Simply
         * query it with an identifier name to get a descriptor for a function, class,
         * variable, etc.
         *
         * The symbol table also maps nonlocal and global vars, so you only need to
         * lookup one symbol table and it will fetch the appropriate info for the var
         * that is currently in scope.
         */

        /**
         * Symbol table for my statements.
         */
        private final SymbolTable<SymbolInfo> sym;
        /**
         * The descriptor for the current function, or null at the top level.
         */
        private final FuncInfo funcInfo;
        public final Frame frame;
        /**
         * Label of code that exits from procedure.
         */
        protected Label epilogue;

        /**
         * An analyzer for the function described by FUNCINFO0, which is null for the
         * top level.
         */
        StmtAnalyzer(FuncInfo funcInfo0) {
            // System.out.println("New StmtAnalyzer call");
            funcInfo = funcInfo0;
            if (funcInfo == null) {
                sym = globalSymbols;
            } else {
                sym = funcInfo.getSymbolTable();
            }
            epilogue = generateLocalLabel();

            this.frame = new Frame(funcInfo == null ? new ArrayList<>() : funcInfo.getParams(), backend, funcInfo);
            if (funcInfo0 != null)
                funcInfo0.getLocals().stream().filter(StackVarInfo.class::isInstance)
                        .filter(i -> !funcInfo0.getParams().contains(i.getVarName())).forEach(svi -> {
                            frame.add(new MV(frame.getVar(svi), svi.getInitialValue().dispatch(this),
                                    "Init Local var " + svi.getVarName()));
                        });

            globalVars.stream().filter(i -> frame.getVar(i.getVarName()) == null)
                    .filter(i -> i.equals(sym.get(i.getVarName()))) // makes sure not shadowed
                    .forEach(gv -> {
                        PR p = new PR();
                        frame.setGlobalVar(p, gv.getLabel());
                        frame.setVar(gv.getVarName(), p);
                    });
        }

        @Override
        public PR analyze(IntegerLiteral i) {
            PR pr = new PR();
            frame.add(new iInst.LI(pr, i.value, "Loading integer immediate"));
            return pr;
        }

        @Override
        public PR analyze(ListExpr node) {
            Type type = node.getInferredType(); // is this required for anything?

            PR list_address = buildNewList(node.elements.size());

            for (int i = 0; i < node.elements.size(); i++) {
                writeMemoryWithWordOffset(list_address, i, node.elements.get(i).dispatch(this));
            }

            return list_address;
        }

        private PR buildNewList(int length) {
            PR lengthreg = frame.push(LI::new, length);
            return buildNewList(lengthreg);
        }

        private PR buildNewList(PR lengthreg) {
            PR label = frame.push(LA::new, listClass.getPrototypeLabel(), "Load list-protype label for listexpr");

            PR wordLength = frame.push(iInst.ADDI::new, lengthreg, 4, "The size of a list is 4+n");

            ArrayList<PR> params = new ArrayList<>();
            params.add(label);
            params.add(wordLength);

            PR ret = frame.addFuncCall(alloc2Wrapper.labelName, params);
            writeMemoryWithWordOffset(ret, -1, lengthreg);
            return ret;
        }

        private void writeMemoryWithWordOffset(PR list_address, int index, PR value_to_assign) {
            // likely fits in immediate, but no guarantees.
            PR indexreg = frame.push(LI::new, index);
            writeMemoryWithWordOffset(list_address, indexreg, value_to_assign);
        }

        // this has no bounds checking!
        private void writeMemoryWithWordOffset(PR list_address, PR indexreg, PR value_to_assign) {
            // likely fits in immediate, but no guarantees.
            PR byteOffset = frame.push(iInst.SLLI::new, indexreg, 2, "mul by wordsize");
            writeMemoryWithByteOffset(list_address, byteOffset, value_to_assign);
        }

        // this has no bounds checking!
        private void writeMemoryWithByteOffset(PR list_address, PR offsetReg, PR value_to_assign) {
            PR addrminus16 = frame.push(ADD::new, list_address, offsetReg, null);
            frame.add(new sInst.SW(value_to_assign, addrminus16, 16,
                    "store element with 16-byte offset for list/string"));
        }

        private PR readMemoryWithWordOffset(PR list_address, int index) {
            PR indexreg = frame.push(LI::new, index);
            return readMemoryWithWordOffset(list_address, indexreg);
        }

        private PR readMemoryWithWordOffset(PR list_address, PR indexreg) {
            // likely fits in immediate, but no guarantees.
            PR byteOffset = frame.push(iInst.SLLI::new, indexreg, 2, "mul by wordsize");
            return readMemoryWithByteOffset(list_address, byteOffset);
        }

        private PR readMemoryWithByteOffset(PR list_address, PR offsetReg) {
            // likely fits in immediate, but no guarantees.
            PR addrminus16 = frame.push(ADD::new, list_address, offsetReg, null);
            return frame.push(iInst.LW::new, addrminus16, 16, "read element with 16-byte offset for list/string");
        }

        // grab the length of a list (in items) or of a string (in characters)
        private PR readLength(PR list_address) {
            PR neg1 = frame.push(LI::new, -1);
            return readMemoryWithWordOffset(list_address, neg1);
        }

        @Override
        public PR analyze(MemberExpr node) {
            PR object = node.object.dispatch(this);
            nullCheck(object);

            ClassInfo objInfo = (ClassInfo) sym.get(node.object.getInferredType().className());
            int pos = backend.getWordSize() * (objInfo.getAttributeIndex(node.member.name) + objectIndexOffset);

            if (pos > 2047) {
                PR p = frame.push(LI::new, pos);
                p = frame.push(ADD::new, p, object,
                        String.format("get ptr for %s from object + offset", node.member.name));
                return frame.push(iInst.LW::new, p, 0, "Load member-expr");
            }
            return frame.push(iInst.LW::new, object, pos, String.format("Load attribute %s", node.member.name));
        }

        private PR getMethod(PR object_ptr, ClassInfo classinfo, String method_name) {
            nullCheck(object_ptr);
            int table_idx = backend.getWordSize() * classinfo.getMethodIndex(method_name);
            int v_table_offset = 2 * backend.getWordSize();
            PR dispatch_table_ptr = frame.push(iInst.LW::new, object_ptr, v_table_offset, "get dispatch table");

            return frame.push(iInst.LW::new, dispatch_table_ptr, table_idx, "Load method address from table");
        }

        @Override
        public PR analyze(MethodCallExpr node) {
            ClassInfo classinfo = (ClassInfo) sym.get(node.method.object.getInferredType().className());

            PR object_ptr = node.method.object.dispatch(this);

            PR methodLocation = getMethod(object_ptr, classinfo, node.method.member.name);

            ArrayList<PR> params = node.args.stream().map(t -> t.dispatch(this))
                    .collect(Collectors.toCollection(ArrayList::new));

            params.add(0, object_ptr);
            // the object itself is the first parameter

            return frame.addRegisterCall(methodLocation, params);
        }

        @Override
        public PR analyze(NoneLiteral node) {
            return X_0;
        }

        @Override
        public PR analyze(NonLocalDecl node) {
            return null;
        }

        @Override
        public PR analyze(Identifier i) {
            PR result = frame.getVar(i.name);
            if (result == null) {
                StackVarInfo svi = (StackVarInfo) sym.get(i.name);
                return frame.getVar(svi);
                // throw new IndexOutOfBoundsException(String.format("couldn't find identifier
                // %s", i.name));
                // throw new IndexOutOfBoundsException(String.format("couldn't find identifier
                // %s", i.name));
            }
            return result;
        }

        @Override
        public PR analyze(IfExpr node) {
            PR result = new PR();
            frame.addIfElseStmt(node.condition.dispatch(this), () -> {
                frame.add(new MV(result, node.thenExpr.dispatch(this), "set then expr"));
            }, () -> {
                frame.add(new MV(result, node.elseExpr.dispatch(this), "set else expr"));
            });
            return result;
        }

        @Override
        public PR analyze(IfStmt node) {
            // remembers if predicate true or false, as it may be a mutated register
            PR path_not_taken = new PR();
            frame.add(new LI(path_not_taken, 1));

            frame.addIfStmt(node.condition.dispatch(this), () -> {
                node.thenBody.stream().forEach(stmt -> stmt.dispatch(this));
                frame.add(new LI(path_not_taken, 0));
            });

            frame.addIfStmt(path_not_taken, () -> {
                node.elseBody.stream().forEach(stmt -> stmt.dispatch(this));
            });

            return null;
        }

        private PR indexString(PR str, PR index) {
            PR ptr = frame.push(ADD::new, str, index, "Sum to get index position of char -4");
            PR char_value = frame.push(iInst.LBU::new, ptr, 4 * backend.getWordSize(), "index string");

            PR new_str = buildNewString(frame.push(LI::new, 1));
            frame.addInst(sInst.SB::new, char_value, new_str, 4 * backend.getWordSize(), "Store char to new string");

            return new_str;
        }

        private PR not(PR cond) {
            return frame.push(pseudoInst.SEQZ::new, cond, "Negation");
        }

        /**
         * Generate code for runtime check on a list index
         *
         * @param list_addr list to be checked
         * @param index     indexed to be read
         */
        private void boundsCheck(PR list_addr, PR index) {
            PR arrayLength = readLength(list_addr);

            /* Bounds check conditions */
            PR cond_1 = frame.push(SLT::new, index, X_0, "if index < 0");
            PR cond_2 = not(frame.push(SLT::new, index, arrayLength, "index < arrlen"));
            // not \implies index >= arrlen

            PR cond = frame.push(OR::new, cond_1, cond_2, "cond: Index < 0 || index >= arrlen");

            /* We use an if statement so that the jump to errorOOb is unconditional */
            frame.addIfStmt(cond, () -> frame.addInst(PREJAL::new, errorOob, "Error: Index < 0 || index >= arrlen"));
        }

        /** return the buffer inside the string object, which is the pointer +4 */
        private PR strDataPtr(PR string_object) {
            return frame.push(iInst.ADDI::new, string_object, 4 * backend.getWordSize(),
                    "add offset to get buffer start");
        }

        private void calL_memcpy(PR dest, PR src, PR len_bytes) {
            ArrayList<PR> parameters = new ArrayList<>();
            parameters.add(dest);
            parameters.add(src);
            parameters.add(len_bytes);
            frame.addFuncCall(StdLibrary.MEM_COPY.labelName, parameters);
        }

        /** This handles the case only where we are reading from the value */
        @Override
        public PR analyze(IndexExpr node) {
            PR list_addr = node.list.dispatch(this);
            PR index = node.index.dispatch(this);

            nullCheck(list_addr);
            boundsCheck(list_addr, index);

            if (node.list.getInferredType().equals(STR_TYPE)) {
                return indexString(list_addr, index);
            } else {
                return readMemoryWithWordOffset(list_addr, index);
            }
        }

        @Override
        public PR analyze(BooleanLiteral b) {
            return frame.push(LI::new, b.value ? 1 : 0);
        }

        /**
         * statement to insert a nullcheck
         * 
         * @param reg Checking if the register is 0
         */
        private void nullCheck(PR reg) {
            PR condition = frame.push(pseudoInst.SEQZ::new, reg, "if register is None");
            frame.addIfStmt(condition, () -> frame.addInst(PREJAL::new, errorNone, "register is None"));
        }

        @Override
        public PR analyze(AssignStmt node) {
            List<Expr> targets = node.targets;
            PR right = node.value.dispatch(this);

            for (Expr target : targets) {
                if (target instanceof Identifier) {
                    PR targetPR = frame.getVar(((Identifier) target).name);
                    if (targetPR == null)
                        targetPR = frame.getVar((StackVarInfo) sym.get(((Identifier) target).name));
                    frame.add(new pseudoInst.MV(targetPR, right, "Assign statement"));
                } else if (target instanceof IndexExpr) {
                                                         // indexpr before list
                    IndexExpr target_ie = (IndexExpr) target;
                    PR list_addr = target_ie.list.dispatch(this);

                    nullCheck(list_addr);
                    Type list_inner_type = target_ie.list.getInferredType().elementType();
                    PR index = target_ie.index.dispatch(this);
                    PR arrayLength = readLength(list_addr);

                    /* Box right side if left side is an object and right is bool/int */
                    PR boxed_value = box(right, node.value.getInferredType(), target_ie.list.getInferredType());

                    boundsCheck(list_addr, index);

                    writeMemoryWithWordOffset(list_addr, index, boxed_value);
                } else if (target instanceof MemberExpr) {
                    MemberExpr target_m = (MemberExpr) target;
                    PR obj_addr = target_m.object.dispatch(this);

                    nullCheck(obj_addr);

                    ClassInfo objInfo = (ClassInfo) sym.get(target_m.object.getInferredType().className());
                    int pos = backend.getWordSize()
                            * (objInfo.getAttributeIndex(target_m.member.name) + objectIndexOffset);

                    if (pos > 2047) {
                        PR p = frame.push(LI::new, pos);
                        p = frame.push(ADD::new, p, obj_addr,
                                String.format("get ptr for %s from object + offset", target_m.member.name));
                        frame.addInst(sInst.SW::new, right, p, 0, "Load member-expr");
                    }
                    frame.addInst(sInst.SW::new, right, obj_addr, pos,
                            String.format("Assign attribute %s", target_m.member.name));
                }
            }
            return null;
        }

        @Override
        public PR analyze(BinaryExpr e) {
            // Similar to project 2 this is the way to emit binary expressions
            PR result = new PR();
            switch (e.operator) {
                case "or":
                    frame.addOrStmt(e.left.dispatch(this), () -> {
                        frame.add(new MV(result, e.right.dispatch(this), "OrExpr full"));
                    }, () -> {
                        frame.add(new LI(result, 1));
                    });
                    return result;
                case "and":
                    frame.addAndStmt(e.left.dispatch(this), () -> {
                        frame.add(new MV(result, e.right.dispatch(this), "AndExpr full"));
                    }, () -> {
                        frame.add(new LI(result, 0));
                    });
                    return result;
            }
            PR left = e.left.dispatch(this);
            PR right = e.right.dispatch(this);
            switch (e.operator) {
                case "-":
                    return frame.push(SUB::new, left, right, "BinaryExpr -");
                case "*":
                    return frame.push(MUL::new, left, right, "BinaryExpr *");
                case "//":
                    frame.addIfStmt(frame.push(pseudoInst.SEQZ::new, right, "If not equal zero then true"), () -> {
                        frame.add(new CustomInstructions.PREJAL(errorDiv, "Div by 0 // jmp"));
                    });
                    return frame.push(DIV::new, left, right, "BinaryExpr //");
                case "%":
                    frame.addIfStmt(frame.push(pseudoInst.SEQZ::new, right, "If not equal zero then true"), () -> {
                        frame.add(new CustomInstructions.PREJAL(errorDiv, "Div by 0 % jmp"));
                    });
                    return frame.push(REM::new, left, right, "BinaryExpr %");
                case "+":
                    Type exp_type = e.getInferredType();

                    if (exp_type.isListType())
                        return concatList(left, right);

                    if (exp_type.equals(STR_TYPE))
                        return concatString(left, right);

                    return frame.push(ADD::new, left, right, "BinaryExpr + int");

                default:
                    assert false : "THIS SHOULD NOT BE REACHED";
                    return X_0;
            }
        }

        @Override
        public PR analyze(CompareExpr e) {
            PR temp = new PR();
            PR tempTwo = new PR();
            PR output = new PR();
            Type type = e.operands.get(0).getInferredType();
            // Iterating through each comparator
            for (int opPos = 0; opPos < e.operators.size(); opPos++) {
                PR left = e.operands.get(opPos).dispatch(this);
                PR right = e.operands.get(opPos + 1).dispatch(this);
                switch (e.operators.get(opPos)) {
                    case "<":
                        frame.addInst(SLT::new, temp, left, right, "CompareExpr <");
                        break;
                    case "<=":
                        PR contrapositive = frame.push(SLT::new, right, left, "Contrapositive > of comparexpr <=");
                        frame.addInst(SEQZ::new, temp, contrapositive, "not on contrapositive");
                        break;
                    case ">":
                        frame.addInst(SLT::new, temp, right, left, "CompareExpr >");
                        break;
                    case ">=":
                        contrapositive = frame.push(SLT::new, left, right, "Contrapositive < of >=");
                        frame.addInst(SEQZ::new, temp, contrapositive, "not on contrapositive");
                        break;
                    case "==":
                        if (INT_TYPE.equals(type) || BOOL_TYPE.equals(type)) {
                            PR diff = frame.push(SUB::new, left, right, "CompareExpr SUB of ==");
                            frame.add(new pseudoInst.SEQZ(temp, diff, "CompareExpr SEQZ of =="));
                        } else {
                            nullCheck(left);
                            nullCheck(right);
                            ArrayList<PR> params = Stream.of(left, right)
                                    .collect(Collectors.toCollection(ArrayList::new));
                            PR t = frame.addFuncCall(StdLibrary.STR_COMP.labelName, params);
                            frame.addInst(MV::new, temp, t, "Save result of call to string");
                        }
                        break;
                    case "!=":
                        if (INT_TYPE.equals(type) || BOOL_TYPE.equals(type)) {
                            frame.add(new rInst.SUB(temp, left, right, "CompareExpr SUB of !="));
                            frame.add(new pseudoInst.SNEZ(temp, temp, "CompareExpr SNEZ of !="));
                        } else {
                            nullCheck(left);
                            nullCheck(right);
                            ArrayList<PR> params = Stream.of(left, right)
                                    .collect(Collectors.toCollection(ArrayList::new));
                            PR t = frame.addFuncCall(StdLibrary.STR_COMP.labelName, params);
                            frame.addInst(pseudoInst.SEQZ::new, temp, t, "Save result of call to string");
                        }
                        break;
                    case "is":
                        frame.add(new rInst.SLT(temp, left, right, "CompareExpr < of is"));
                        frame.add(new rInst.SLT(tempTwo, right, left, "CompareExpr > of is"));
                        frame.add(new pseudoInst.SEQZ(temp, temp, "CompareExpr SEQZ1 of is"));
                        frame.add(new pseudoInst.SEQZ(tempTwo, tempTwo, "CompareExpr SEQZ2 of is"));
                        frame.add(new rInst.AND(temp, temp, tempTwo, "CompareExpr AND of is"));
                }
                if (opPos == 0) {
                    frame.add(new rInst.AND(output, temp, temp, "Concatenating CompareExpr first pass"));
                } else {
                    frame.add(new rInst.AND(output, output, temp, "Concatenating CompareExpr"));
                }
            }
            return output;
        }

        @Override
        public PR analyze(ReturnStmt stmt) {
            if (stmt.value == null) {
                frame.addReturn(X_0);
                return null;
            }
            PR regist = stmt.value.dispatch(this);
            regist = box(regist, stmt.value.getInferredType(), funcInfo.getReturnType());
            frame.addReturn(regist);
            return null;
        }

        @Override
        public PR analyze(StringLiteral node) {
            String s = node.value;
            int len = s.length();
            PR lenreg = frame.push(LI::new, len);
            byte[] data = Arrays.copyOf(s.getBytes(), len + 4);

            PR ret = buildNewString(lenreg);

            for (int i = 0; i <= len; i += 4) {
                PR word = frame.push(LI::new, (int) data[i] + ((int) data[i + 1] << 8) + ((int) data[i + 2] << 16)
                        + ((int) data[i + 3] << 24));
                writeMemoryWithWordOffset(ret, i / 4, word);
            }
            return ret;
        }

        // comes pre-null terminated! :)
        private PR buildNewString(PR lenreg) {
            PR label = frame.push(LA::new, strClass.getPrototypeLabel(), "load string prototype label");

            PR lendiv4 = frame.push(iInst.SRLI::new, lenreg, 2, "div 4");
            PR wordLength = frame.push(iInst.ADDI::new, lendiv4, 5, "The size of a string is k/4 + 1 + 4");

            ArrayList<PR> params = new ArrayList<>();
            params.add(label);

            params.add(wordLength);

            PR ret = frame.addFuncCall(alloc2Wrapper.labelName, params);

            writeMemoryWithWordOffset(ret, -1, lenreg);

            PR dest = frame.push(ADD::new, ret, lenreg, "add to get end - 4");

            // fixed bug
            writeMemoryWithByteOffset(ret, lendiv4, X_0);

            return ret;
        }

        @Override
        public PR analyze(UnaryExpr node) {
            PR inner = node.operand.dispatch(this);
            switch (node.operator) {
                case "not":
                    return frame.push(pseudoInst.SEQZ::new, inner, "UnaryExpr not");
                case "-":
                    return frame.push(SUB::new, X_0, inner, "UnaryExpr - (negative)");
            }
            return null;
        }

        @Override
        public PR analyze(WhileStmt node) {
            // We need to perform evaluation of the entire condition repeatedly, lambdas
            // make this easy api-wise.
            frame.addWhileStmt(() -> node.condition.dispatch(this), () -> {
                node.body.forEach(stmt -> stmt.dispatch(this));
            });
            return null;
        }

        private ArrayList<ValueType> typeSignature(FuncInfo f) {
            SymbolTable<SymbolInfo> tbl = f.getSymbolTable();
            return f.getParams().stream().map(s -> (VarInfo) tbl.get(s)).map(VarInfo::getVarType)
                    .collect(Collectors.toCollection(ArrayList::new));
        }

        private PR box(PR regist, Type given, Type expected) {
            if (given.equals(INT_TYPE) && expected.equals(OBJECT_TYPE)) {
                return frame.addFuncCall(intWrapper.labelName, new ArrayList<>(Arrays.asList(regist)));
            } else if (given.equals(BOOL_TYPE) && expected.equals(OBJECT_TYPE)) {
                return frame.addFuncCall(boolWrapper.labelName, new ArrayList<>(Arrays.asList(regist)));
            }
            return regist;
        }

        @Override
        public PR analyze(CallExpr ce) {
            if (sym.get(ce.function.name) instanceof ClassInfo) {
                if (ce.function.name.equals("int") || (ce.function.name.equals("bool"))) {
                    return X_0;
                }

                ClassInfo classInfo = (ClassInfo) sym.get(ce.function.name);
                PR prototype_adr = frame.push(LA::new, classInfo.getPrototypeLabel(),
                        "Create new object from class prototype");
                PR obj = frame.addFuncCall(newObject.labelName, new ArrayList<>(Arrays.asList(prototype_adr)));

                // call init function
                PR method = getMethod(obj, classInfo, "__init__");
                frame.addRegisterCall(method, new ArrayList<>(Arrays.asList(obj)));
                // sure is correct,
                // but we need to ensure. This doesn't seem to cause in change in # of passed
                // cases;
                // We were reading protoype; not object we created; this is important if
                // __init__ does mutation

                return obj;
            }
            FuncInfo f = (FuncInfo) sym.get(ce.function.name);
            ArrayList<ValueType> funcSig = typeSignature(f);

            // box method "boxes" int, and bool parameters into objects if need be (expected
            // == object)
            // otherwise it's the identity function
            ArrayList<PR> fixed_params = zipStream(ce.args, funcSig)
                    .map((arg, expected) -> box(arg.dispatch(this), arg.getInferredType(), expected))
                    .collect(Collectors.toCollection(ArrayList::new));

            return frame.addFuncCall(f.getCodeLabel().labelName, fixed_params, f.getDepth());
        }

        @Override
        public PR analyze(ExprStmt es) {
            es.expr.dispatch(this);
            return null;
        }

        @Override
        public PR analyze(ForStmt node) {
            PR list_address = node.iterable.dispatch(this);
            nullCheck(list_address);

            PR len = readLength(list_address);
            PR list_address_copy = new PR();
            frame.addInst(MV::new, list_address_copy, list_address, "Copy addr start of for stmt");

            PR index = frame.push(LI::new, 0);

            if (node.iterable.getInferredType().equals(STR_TYPE)) {
                frame.addWhileStmt(() -> {
                    return frame.push(SLT::new, index, len, "While index less than string len");
                }, () -> {
                    PR var = frame.getVar(node.identifier.name);
                    if (var == null) {
                        var = frame.getVar((StackVarInfo) sym.get(node.identifier.name));
                    }
                    frame.add(new MV(var, indexString(list_address_copy, index), "assign"));
                    node.body.forEach(i -> i.dispatch(this));
                    frame.add(new iInst.ADDI(index, index, 1, "increment index"));
                });

            } else {
                frame.addWhileStmt(() -> {
                    return frame.push(SLT::new, index, len, "While index less than list len");
                }, () -> {
                    PR var = frame.getVar(node.identifier.name);
                    if (var == null) {
                        var = frame.getVar((StackVarInfo) sym.get(node.identifier.name));
                    }
                    frame.add(new MV(var, readMemoryWithWordOffset(list_address_copy, index), "assign"));
                    node.body.forEach(i -> i.dispatch(this));
                    frame.add(new iInst.ADDI(index, index, 1, "increment index"));
                });
            }
            return null;
        }

        @Override
        public PR defaultAction(Node n) {
            throw new IndexOutOfBoundsException(
                    String.format("you didn't implement  %s in CodeGenIMpl ya dingus", n.getClass().getSimpleName()));
        }

        /**
         * Given strings objects pointed at by @param left and @param right, returns a
         * new string object which contains the concatenation of their contents
         */
        private PR concatString(PR left, PR right) {
            nullCheck(left);
            nullCheck(right);

            PR leftlen = readLength(left);
            PR rightlen = readLength(right);
            PR total = frame.push(ADD::new, leftlen, rightlen, "compute total string size");

            PR ret = buildNewString(total);
            PR new_str_buff = strDataPtr(ret);

            calL_memcpy(new_str_buff, strDataPtr(left), leftlen);

            PR new_dst = frame.push(ADD::new, new_str_buff, leftlen, "continuation for second point");

            calL_memcpy(new_dst, strDataPtr(right), rightlen);
            return ret;
        }

        /**
         * Given list objects pointed at by @param left and @param right, returns a new
         * string/list object which contains the concatenation of their contents
         */
        private PR concatList(PR left, PR right) {
            nullCheck(left);
            nullCheck(right);

            PR leftlen = readLength(left);
            PR rightlen = readLength(right);
            PR total = frame.push(ADD::new, leftlen, rightlen, "compute total string size");

            PR ret = buildNewList(total);
            PR new_str_buff = strDataPtr(ret);

            PR left_buffer = strDataPtr(left);
            PR right_buffer = strDataPtr(right);

            PR left_byte_len = frame.push(iInst.SLLI::new, leftlen, 2, "multiply by wordsize");
            PR right_byte_len = frame.push(iInst.SLLI::new, rightlen, 2, "multiply by wordsize");

            calL_memcpy(new_str_buff, left_buffer, left_byte_len);
            PR new_dst = frame.push(ADD::new, new_str_buff, left_byte_len, "continuation for second point");
            calL_memcpy(new_dst, right_buffer, right_byte_len);
            return ret;
        }

        public <W extends Node, X extends Node, V> PR eval(Frame.FourArgConstr<PR, PR, V> f, W param2, X param3,
                V param4) {
            return frame.push(f::constructor, param2.dispatch(this), param3.dispatch(this), param4);
        }

        public <W extends Node, V> PR eval(Frame.ThreeArgConstr<PR, V> f, W param2, V param4) {
            return frame.push(f::constructor, param2.dispatch(this), param4);
        }
    }

}
