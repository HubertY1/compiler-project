package chocopy.common.astnodes;

import chocopy.common.analysis.NodeAnalyzer;
import java_cup.runtime.ComplexSymbolFactory.Location;

import java.util.List;

/**
 * <operand> <comparison operator> <operand> <comparison operator> ....
 */
public class CompareExpr extends Expr {

    /**
     * Operands.
     */
    public final List<Expr> operands;
    /**
     * Operator names. operators[i] is the operator combining operand[i] and
     * operand[i+1].
     */
    public final List<String> operators;

    /**
     * An AST for comparisons of the form OPNDS[0] OPS[0] OPNDS[1] OPS[1] ... OPNDS[N]
     * from text in range [LEFTLOC..RIGHTLOC]. Here, the OPS are the comparison
     * operators "<", ">", "<=", ">=", "==", "!=", "is".
     */
    public CompareExpr(Location leftLoc, Location rightLoc,
                       List<Expr> opnds, List<String> ops) {
        super(leftLoc, rightLoc);
        operands = opnds;
        operators = ops;
    }

    public <T> T dispatch(NodeAnalyzer<T> analyzer) {
        return analyzer.analyze(this);
    }

}
