def foo():
    print("foo")
    return True

def bar():
    print("bar")
    return False

if foo():
    print("hello")
else:
    print("goodbye")


if bar():
    print("f")
else:
    print("h")


if foo():
    print("y")
elif foo():
    print("g")
else:
    print("q")


if foo():
    print("yyy")
elif bar():
    print("ggg")
else:
    print("qqq")


if bar():
    print("yy")
elif foo():
    print("gg")
else:
    print("qq")


if bar():
    print("y")
elif bar():
    print("g")
else:
    print("q")

if foo():
    print("hi")

if bar():
    print("bye")

print(foo() if foo() else bar())

print(foo() if bar() else bar())

print(bar() if foo() else foo())

print(bar() if bar() else foo())