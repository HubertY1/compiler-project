class foo(object):
    i:int = 0
    def goo(self):
        self.i = self.i + 1
    def moo(self):
        def rude():
            def dude():
                nonlocal self
                self = foo()
                print(self.i)
            print(self.i)
            self.goo()
            dude
        rude()

x:foo = None

def roo():
    global x
    x.goo()
    def moo():
        nonlocal x
        x.goo()



x = foo()
print(x)
foo.goo()
print(x)
roo()




