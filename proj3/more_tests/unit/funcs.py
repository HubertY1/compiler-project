
def foo()-> str:
    print(1)
    return "a"

def goo() -> str:
    print(2)
    return "b"

def moo() -> str:
    print(3)
    return "c"
z:object = None

def threeArg(x:str, y:str, g:str):
    print(x)
    print(y)
    print(z)

#expand the stack.
def implic_return_none()-> object:
    #expand the stack.
    print("yes")


threeArg(foo(), goo(), moo())

print(implic_return_none() is z)