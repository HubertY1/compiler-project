class A(object):
    x:int = 2
    def __init__(self:A):
        self.x = 10
        print(self)

class B(object):
    pass
a: A = None
b: B = None

a = A()
b = B()

print(a.x)

print(b.x)
