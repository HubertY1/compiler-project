class A(object):
    i:int = 3
    o:A = None
a:A = None
nun:A = None

def global_read():
    print(a.i)

def global_write():
    a.i = a.i + 1

def nonlocal_test():
    a:A = None
    def reader():
        print(a.i)
    def read_writer():
        print(a.i)
        q.i = q.i + 1
        print(a.i)
    def global_override(): #this is probably difficult
        global a
        print(a.i)
    a = A()
    a.i =  150
    reader()
    read_writer()


a= A()

print(a.i)

print(a.o is nun)

a.i = 10

print(a.i)

a.o = a

print(a is a.o)

non_local_read()

non_local_write()

print(a.i)
