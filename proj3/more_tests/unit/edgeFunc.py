x:int = 1

def foo():
    x:int = 100
    def bar():
        global x
        x = x+10
        print(x)
        def bar():
            nonlocal x
            print(x)