def foo()-> int:
    return 1


def bar() -> object:
    if False:
        return "hello"
    return


def f()-> object:
    return 1


def g() -> object:
    return False

print(f())
print(g())

print(bar() is None)

print(foo() == 1)