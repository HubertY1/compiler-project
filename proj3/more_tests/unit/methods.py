class A(object):
    def simple(self:A):
        print("simple")
    def simple_return(self:A)-> int:
            print("simple")
            return 3
    def order_eval(self: A, x:int, y:int, z:int):
        print(x+ y+ z)
    def override(self: A):
        print("original")

    def downCastInt(self) -> object:
        return 1
    def downCastBool(self) -> object:
        return False


class B(A):
    def override(self):
        print("not original")

def one()-> int:
    print("a")
    return 1

def two()-> int:
    print("b")
    return 2

def three()-> int:
    print("c")
    return 3

class C(B):
    pass



a :A  = None
b:B = None
c:C = None


a = A()

a.simple()

print(a.simple_return())
a.override()

b = B()

b.override()
b.simple()

a = b

b.simple()
b.override()

c = C()

c.override()
c.simple()

a = c

c.simple()

a.order_eval(one(), two(), three())

print(a.downCastBool())

print(a.downCastInt())