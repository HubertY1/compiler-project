def foo()-> bool:
    print("foo")
    return True

def bar() -> bool:
    print("bar")
    return False

print(not True)

print(not not True)


print(not False)

print(not not False)


print(foo() and bar())
print(bar() and foo())

print(foo() or bar())
print(bar() or foo())

print(foo() if True else bar())

print(bar() if True else foo())

print(foo() if False else bar())

print(bar() if False else foo())
