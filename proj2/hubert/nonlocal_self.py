def f(x:int) -> int:
    def g() -> int:
        nonlocal x
        return x
    return g()

class foo(object):
    def method(self: foo) -> foo:
        def nested() -> foo:
            nonlocal self
            return self
        return nested()

