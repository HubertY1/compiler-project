class a(object):
    pass
class b(a):
    pass
class c(b):
    pass

def f(A: a, B: b, C: c):
    pass

A:a = None
B:b = None
C:c = None

A = a()
B = b()
C = c()

f(C, C, C)
f(B, C, C)
f(B, C, C)
f(A, C, C)
f(A, B, C)
