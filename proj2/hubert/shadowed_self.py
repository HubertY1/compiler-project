class foo(object):
    def method(self: foo) -> int:
        def nested() -> int:
            self:int = 5
            return self
        return nested()

