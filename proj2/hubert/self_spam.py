class foo(object):
    self:int = 5
    def __init__(self: foo):
        print(self.method2(self.self))
    def method(self: foo) -> foo:
        def nested(self: foo) -> foo:
            return self
        return nested(self)
    def method2(this: foo, self: int) -> int:
        return this.self + self
self:foo = None
y:int = 5

self = self.method()
y = self.method2(self.self)

