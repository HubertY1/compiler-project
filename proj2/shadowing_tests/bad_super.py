class A(object):
    pass

class B(A):
    pass

a:[A] = None
b:[B] = None

def goo():
    pass

def foo() -> [object]:
    return [goo()]

def p():
    return [None] if True else []


x:int = 1
y:bool = True
z: str = "hi"

x = int()
y = bool()
z = str()

x = foo

