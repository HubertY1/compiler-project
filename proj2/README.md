# CS 164: Programming Assignment 2

[PA2 Specification]: http://inst.eecs.berkeley.edu/~cs164/fa20/hw/PA2.pdf
[ChocoPy Specification]: http://inst.eecs.berkeley.edu/~cs164/fa20/chocopy_language_reference.pdf

## Getting started

Run the following command to build your semantic analysis, and then run all the provided tests:

    mvn clean package

    java -cp "chocopy-ref.jar:target/assignment.jar" chocopy.ChocoPy \
        --pass=.s --dir src/test/data/pa2/sample/ --test


In the starter code, only two tests should pass. Your objective is to implement a semantic analysis that passes all the provided tests and meets the assignment specifications.

You can also run the semantic analysis on one input file at at time. In general, running the semantic analysis on a ChocoPy program is a two-step process. First, run the reference parser to get an AST JSON:


    java -cp "chocopy-ref.jar:target/assignment.jar" chocopy.ChocoPy \
        --pass=r <chocopy_input_file> --out <ast_json_file> 


Second, run the semantic analysis on the AST JSON to get a typed AST JSON:

    java -cp "chocopy-ref.jar:target/assignment.jar" chocopy.ChocoPy \
        -pass=.s  <ast_json_file> --out <typed_ast_json_file>


The `src/tests/data/pa2/sample` directory already contains the AST JSONs for the test programs (with extension `.ast`); therefore, you can skip the first step for the sample test programs.

To observe the output of the reference implementation of the semantic analysis, replace the second step with the following command:


    java -cp "chocopy-ref.jar:target/assignment.jar" chocopy.ChocoPy \
        --pass=.r <ast_json_file> --out <typed_ast_json_file>


In either step, you can omit the `--out <output_file>` argument to have the JSON be printed to standard output instead.

You can combine AST generation by the reference parser with your 
semantic analysis as well:

    java -cp "chocopy-ref.jar:target/assignment.jar" chocopy.ChocoPy \
        --pass=rs <chocopy_input_file> --out <typed_ast_json_file>


## Assignment specifications

See the [PA2 specification][] on the course
website for a detailed specification of the assignment.

Refer to the [ChocoPy Specification][] on the CS164 web site
for the specification of the ChocoPy language. 

## Receiving updates to this repository

Add the `upstream` repository remotes (you only need to do this once in your local clone):

    git remote add upstream https://github.com/cs164berkeley/pa2-chocopy-semantic-analysis.git


To sync with updates upstream:

    git pull upstream master

## Submission writeup

Team member 1: Richard Lettich

Team member 2: Matthew Nevling

Team member 3: Thomas Norell

Team member 4: Hubert Yuan

1. We perform 4 passes, each in their own source file. In order they are:
    - `DeclarationAnalyzer.java`: This fills out the symbol tables and checks for duplicate declarations and redefined attributes.
    - `TypeChecker.java`: This performs in-depth type checking, assigning inferred types to all AST nodes that resolve to a value. It verifies that all expressions follow type rules, such as ensuring both sides of an assignment statement have the same type.
    - `ReturnPathsChecker.java`: This checks that all functions which declare a return type return a value of the correct type in every execution path.
    - `ShadowChecker.java`: This checks that class names are not illegally shadowed.

2. The hardest component to implement was scoping. We needed to resolve identifiers appropriately within the environment of a function or class. We ran into a tricky bug with our symbol tables where we wanted read-only access to global and nonlocal symbol tables. In order to get around this we extended the symbol table class to support additional features. Scoping was not a singular issue, but rather a consistent challenge to overcome in the project that forced us to adapt our approach multiple times. Knowing what can or should be passed down in the scope was an interesting challenge.

    We came up with an interesting LIFO solution of the form
    runnWithSymtable(sym, () -> {
        // stuff
    });
    Which we thought was clever, as it prevented forgetting to restore the old state symboltable.

3. We must always recover by inferring the most specific type possible in order to avoid cascading errors. Consider this example where a `IfExpr` contains a `BinaryExpr`, as seen in `bad_types.py`  on line 4:
`x = 1 if True == 1 else 3`
In this case the `BinaryExpr` `True == 1` is ill-typed, so in order to recover we must infer that because the `BinaryExpr` uses the `==` operator and thus is a comparison, it should evaluate to a `bool`. If we treated it as type `object` the `IfExpr` would also fail because it is expecting a boolean condition. But `IfExpr` would be well-typed if the `BinaryExpr` was , so just treating ill-typed expressions as type `object` would result in incorrectly the propagation and cascatio of errors.

There is a class called autoextend which modifed a snippit from stack-overflow for extending methods, since we couldn't edit for a copy constructor. It was meant for general use, but was only used in one place.

Zero late hours were consumed.