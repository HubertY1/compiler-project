if [ "$#" -ne 1 ]; then
    echo "Usage: ./testify.sh dir"
fi

for x in ${1}/*.py; do
	name=$x
	astName="$x.ast"
	astTypedName="$x.ast.typed"
	echo $astName
	java -cp 'chocopy-ref.jar:target/assignment.jar' chocopy.ChocoPy --pass=r $name --out $astName
	java -cp 'chocopy-ref.jar:target/assignment.jar' chocopy.ChocoPy --pass=rr $name --out $astTypedName
	cp $name src/test/data/pa2/student_contributed
	cp $astName src/test/data/pa2/student_contributed
	cp $astTypedName src/test/data/pa2/student_contributed
	done
