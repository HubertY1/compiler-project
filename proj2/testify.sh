name=$1
astName="${name}.ast"
astTypedName="${name}.ast.typed"

echo $astName

java -cp 'chocopy-ref.jar:target/assignment.jar' chocopy.ChocoPy --pass=r $1 --out $astName
java -cp 'chocopy-ref.jar:target/assignment.jar' chocopy.ChocoPy --pass=rr $1 --out $astTypedName
mv $name src/test/data/pa2/student_contributed
mv $astName src/test/data/pa2/student_contributed
mv $astTypedName src/test/data/pa2/student_contributed