x:int = 1

# x not declared in scope
def foo():
    for x in ["hi"]:
        pass
# x \<\=\ iterable
for x in [1, "ello mate", None]:
    pass

#  x is not subtype of string

for x in "hellow world":
    pass

