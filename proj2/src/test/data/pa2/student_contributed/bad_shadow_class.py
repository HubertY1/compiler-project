class self(object):
    def __init__(self: self): #bad
        pass

def foo() -> int:
    self:int = 5 #bad
    return self

def bar(self: int) -> int: #bad
    return self
