x: [int] = None
y: [[int]] = None

x = [] #ok
y = [[]] # bad
y = [x] # ok

