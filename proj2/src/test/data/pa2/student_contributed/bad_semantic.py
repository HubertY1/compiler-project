#1 identifiers may not be redefined in the same scope

#11 type annotations should not refer to class names that are not defined
x:AAAA = None

x:BBB = None

X:XXRQ = None

x:int = 1
x:int = 2
def x(y:int, y:int, y:int) -> int:
    pass

class B(object):
    pass
#2 Variables and functions may not shadow class names
class A(object):
    A: int = None #kosher
    def A(self: A):
        A:object = None
        pass


def A():
    def B():
        B:int = 0
        pass
    pass

3 Nonlocal declarations may only apply to variable in outer scope

y:int = 0
def foo():
    nonlocal y
    nonlocal z
    nonlocal q
    global foo
    global A
    global g
    def f():
      nonlocal g
      pass
      pass
    pass


# In class definitions, the declared super-class must either be object or be a user-defined class
# that has been defined previously in the program. See bad class super.py.

class D(C):
    pass

class C(object):
    pass

class Q(int):
    pass

class ZZ(str):
    pass

class zrq(bool):
    pass


class RR(object):
    x:int = 0

class RR2(RR):
    x:int = 0

class RR2(RR):
    def x(self:RR2):
        pass

class RB(object):
    x:int = 0

class Z(RB):
    pass

class ZZ(Z):
    x:object = None

class RQQ(Z):
    def x(self:A) -> int:
        return 1



# In class definitions, methods must specify at least one formal parameter, and the first parameter
# must be of the same type as the enclosing class. See bad class method.py

class ZQR(object):
    def A(self: object):
        x:int = 0
        pass
    def B():
        pass


class ff(object):
    def A(self: object):
        x:int = 0
        pass
    def B():
        pass
    def g(i:int, self :A):
        pass




rqs:int = 0
x:int = 0
def foo():
    def go():
        x = 2
    for rqs in [1,2,4,5,6]:
        print(rqs)


class FFFF(object):
    def gg(self: FFFF):
        x = 2


#9. Functions or methods that return special types must have an explicit return statement along all
# paths. See bad return missing.py




def fggg() -> int:
    if True:
        return 1
    else:
         if True:
             return 2
         else:
             pass


def gggg() -> str:
    i: int = 0
    for i in [1,2,3,4,5,6]:
        i = i + 1
        if (True):
            print("boo")
            return "no"
        print (i)

def goo()-> bool:
    if False:
        return False
    else:
        print("hi")


#11 type annotations should not refer to class names that are not defined
#x:AAAA = None

#x:BBB = None

#X:XXRQ = None

#def goo(f:booo, g:moo, q: doo, p: food):





#10. Return statements must not appear at the top level outside function or method bodies. See
#bad return top.py.

return "hello"

return "no"

return 1

return object()

return goo()

