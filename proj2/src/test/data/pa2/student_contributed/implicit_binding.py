x:int = 5
def foo() -> int:
    y:str = "hello"
    def bar() -> int:
        return x + len(y)
    return bar() + x
