def foo() -> int:
	def goo() -> str:
		return 1
	def hoo() -> int:
		def koo() -> int:
			return None
		return koo()
	return goo()
