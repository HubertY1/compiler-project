class foo(object):
    foo: foo = None
    
class bar(object):
    def bar(self: bar) -> bar:
        return self.bar()
