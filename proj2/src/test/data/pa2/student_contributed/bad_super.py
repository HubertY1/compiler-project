class A(object):
    pass

class B(A):
    pass
mm:object = None
a:[A] = None
b:[B] = None
zz:int = None
def goo():
    pass

def zoo() -> int:
    return 1

def foo() -> [object]:
    return [goo()]

def p():
    return [None] if True else []


x:int = 1
y:bool = True
z: str = "hi"

x = int()
y = bool()
z = str()

x = foo
mm = foo
