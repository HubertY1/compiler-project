bx:int = 1
z:bool = True
w:str = "hi"
y:[[int]] = None
lst:[int] = None
otherLst:[str] = None
def foo(a:str, b:bool) -> int:
    return 1
def fi(a:str, b:bool) -> int:
    return False
class A(object):
    x:int = 1
    def bonk(self:A, b:bool) ->int:
        w = "potato"
        return 2
class B(A):
    z:int = 1
class C(A):
    b:int = 1
jail:A = None
ifEx:B = None
ifExp:C = None
jail = A()
x = 1 if True == 1 else 3
x + [1]
x.flourish()
jail.bonk()
jail.bonk(1)
input(x)
foo(x, z)
potato()
A = 1
b = A
x.flourish
jail.bonk
x = None
z = None
w = None
y = 5
lst = True
lst = 5
w[1] = "w"
lst = otherLst = [None]
1 > True
False == None
1 == True
b is 1
False + False
1 if 1 else 2
ifExp if True else ifEx
1 if True else "hi"
if 1:
    w = "new"
for i in 1:
    w
-True