b:AAAAAAA = None
def fi() -> AAAAAAA:
    return AAAAAAA()
class AAAAAAA(object):
    x:int = 1
    def fo(self:AAAAAAA):
        fi()

def fii() -> AA:
    return AA()
bb:AA = None
class AA(object):
    x:int = 1
    def fii(self:AA):
        fii()

bbb:AAA = None
class AAA(object):
    x:int = 1
    def fo(self:AAA):
        fiii()
def fiii() -> AAA:
    return AAA()


def fiiii() -> AAAA:
    return AAAA()
class AAAA(object):
    x:int = 1
    def fiiii(self:AAAA):
        fiiii()
bbbb:AAAA = None

class AAAAA(object):
    x:int = 1
    def fo(self:AAAAA):
        fiiiii()
def fiiiii() -> AAAAA:
    return AAAAA()
bbbbb:AAAAA = None

class AAAAAA(object):
    x:int = 1
    def fo(self:AAAAAA):
        fiiiiii()
bbbbbb:AAAAAA = None
def fiiiiii() -> AAAAAA:
    return AAAAAA()

class foo(object):
    foo: foo = None

class bar(object):
    def bar(self: bar) -> bar:
        return self.bar()

#class_attribute_shared_name
class foo1(object):
    foo1: foo1 = None

class bar1(object):
    def bar1(self: bar1) -> bar1:
        return self.bar1()

#class_hierarchy
class a2(object):
    pass
class b2(a2):
    pass
class c2(b2):
    pass

def f2(alph2: a2, bet2: b2, gam2: c2):
    pass

A2:a2 = None
B2:b2 = None
C2:c2 = None

#for_good
x4:object = None
y4:str = "hi"
z4:A4 = None

class A4(object):
    pass

#fun_shadow
def foo5():
    pass

def g5():
    def foo5():
        pass
    pass

#implicit_binding
x6:int = 5
def foo6() -> int:
    y6:str = "hello"
    def bar6() -> int:
        return x6 + len(y6)
    return bar6() + x6

#implicitNoneReturn
def foo7():
	pass

#list_nesting
x8: [[int]] = None
y8: [[[int]]] = None


#list_none_empty
class foo9(object):
    pass

x9:object = None
y9:foo9 = None

#list_ops
x10:[int] = None
y10:int = 5

#multipleConditionalsOneReturnPath
def foo11() -> int:
    if True:
        "hello"
    else:
        2
    return 1

#nonlocal_self
def f13(x:int) -> int:
    def g() -> int:
        nonlocal x
        return x
    return g()

class foo13(object):
    def method(self: foo13) -> foo13:
        def nested() -> foo13:
            nonlocal self
            return self
        return nested()

#recursion
def f14(x: int) -> int:
    return f14(f14(x)) + 1

#self_spam
class foo15(object):
    self:int = 5
    def __init__(self: foo15):
        print(self.method2(self.self))
    def method(self: foo15) -> foo15:
        def nested(self: foo15) -> foo15:
            return self
        return nested(self)
    def method2(this: foo15, self: int) -> int:
        return this.self + self
self:foo15 = None
y15:int = 5
x:int = 1
z:bool = True
w:str = "hi"
y:[[int]] = None
lst:[int] = None
otherLst:[str] = None
def fool(a:str, b:bool) -> int:
    return 1
def fill(a:str, b:bool) -> int:
    return 1
class A(object):
    x:int = 1
    def bonk(self:A, b:bool) ->int:
        w:str = "potato"
        return 2
class B(A):
    z:int = 1
class C(A):
    b:int = 1
jail:A = None
ifEx:B = None
ifExp:C = None
#shadowed_self
class foo16(object):
    def method(self: foo16) -> int:
        def nested() -> int:
            self:int = 5
            return self
        return nested()

#str_ops
x17:str = "hello"
y17:str = "world"


#class_hierarchy
A2 = a2()
B2 = b2()
C2 = c2()

f2(C2, C2, C2)
f2(B2, C2, C2)
f2(B2, C2, C2)
f2(A2, C2, C2)
f2(A2, B2, C2)

#for_good
for y4 in "hello world":
    pass

for x4 in "hello world":
    pass

for x4 in [A4(), A4(), A4()]:
    pass

for z4 in [A4(),A4()]:
    pass

#list_nesting
x8 = [None, [], [1], [2,3], [4,5]]
y8 = [None, [], [[1], [2,3]], x8]


#list_none_empty
x9 = [None] # [<None>]
x9 = [[]] # [<Empty>]
y9 = [None][0]
print([[]][0])

#list_ops
y10 = x10[0]
y10 = x10[0] + x10[y10]
y10 = x10[x10[y10]]
y10 = x10[x10[y10] + y10]
y10 = (x10+x10)[y10]
y10 = (x10+x10)[len(x10)]
y10 = x10[len(x10) + len(x10+[x10[y10]])]

#self_spam
self = self.method()
y15 = self.method2(self.self)

#str_ops
y17 = x17[0]
y17 = x17[0] + x17[0][0]
y17 = x17[len(x17)][len(y17)]
y17 = x17[len(x17[len(x17)] + y17)]
y17 = (x17+x17)[len(x17)]
y17 = x17[0][0][0][0][0][0][0][0]




jail = A()
x = 1 if True == True else 3
x + 2
jail.bonk(False)
w = input()
foo()
jail.bonk(True)
y = [[1], [2]]
lst = [2]
lst = []
lst = otherLst = []
1 > 0
False == True
1 == 2
1 if 1 == 1 else 2
ifExp if True else ifEx
