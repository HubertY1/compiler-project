


class B(object):
    x:int = 0

class C(object):
    def q(self: C) -> object:
        return B

__init__:int = 0
class D(B):
    def self(q:D):
        global __init__
        q.x = 2
        q.__init__()


