package chocopy.pa2.Types;

import chocopy.common.analysis.types.ClassValueType;
import chocopy.common.analysis.types.Type;

import java.util.Objects;

/**
 * A Type for types
 * <p>
 * e.g.
 * Let T(a) = type of a
 * <p>
 * let a:object = None
 * then:
 * T(a)  is  an Instance of  class value Type
 * T(object)  is an  Instance of  MetaType
 */
public class MetaType extends Type {
    public static final MetaType META_OBJ = new MetaType(Type.OBJECT_TYPE);
    public static final MetaType META_INT = new MetaType(Type.INT_TYPE, META_OBJ);
    public static final MetaType META_BOOL = new MetaType(Type.BOOL_TYPE, META_OBJ);
    public static final MetaType META_STR = new MetaType(Type.STR_TYPE, META_OBJ);
    public static final MetaType META_NONE = new MetaType(Type.NONE_TYPE, META_OBJ);
    private MetaType parent = null;
    private final ClassValueType describes_type;


    public MetaType(ClassValueType t, MetaType parent) {
        this.describes_type = t;
        this.parent = parent;
    }

    public MetaType(ClassValueType t) {
        this.describes_type = t;
    }

    public MetaType(String name) {
        this.describes_type = new ClassValueType(name);
    }

    public MetaType(String name, MetaType parent) {
        this.describes_type = new ClassValueType(name);
        this.parent = parent;
    }

    public ClassValueType getType() {
        return describes_type;
    }


    public ClassValueType getParentClass() {
        return parent == null ? null : parent.describes_type;
    }

    public MetaType getParentType() {
        return parent;
    }

    public void setParent(MetaType p) {
        this.parent = p;
    }

    @Override
    public boolean isSpecialType() {
        return this.equals(META_INT) ||
                this.equals(META_BOOL) ||
                this.equals(META_STR);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof MetaType))
            return false;
        return ((MetaType) o).describes_type.equals(this.describes_type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(describes_type);
    }

    @Override
    public String toString() {
        return this.describes_type.toString();
    }
}
