package chocopy.pa2;

import chocopy.common.analysis.types.ClassValueType;
import chocopy.common.analysis.types.ListValueType;
import chocopy.common.analysis.types.Type;

import java.util.ArrayList;
import java.util.Arrays;


public class TypeComparer {
    private TCNode root;

    // prevent bad construction with private definition
    private TypeComparer() {
    }

    // constructs TypeComparer with baseType as type everything inherits from
    public TypeComparer(Type baseType) {
        root = new TCNode(baseType);
        root.MakeChild(Type.BOOL_TYPE);
        root.MakeChild(Type.INT_TYPE);
        root.MakeChild(Type.STR_TYPE);
        root.MakeChild(Type.EMPTY_TYPE);
    }

    public boolean exists(Type t) {
        if (t == null) {
            System.out.println("logic error in TC.exists ERR");
        }
        if (t instanceof ClassValueType) {
            return this.containsType(t.className());
        }
        return false;
    }

    public Type getParent(Type t) {
        return root.parent(t);
    }

    public boolean containsType(String type_name) {
        ClassValueType t = new ClassValueType(type_name);
        return root.findDescendant(t) != null;
    }


    public Type join(Type... v) {
        if (v.length == 0)
            return Type.EMPTY_TYPE;
        if (v.length == 1)
            return v[0];
        Type tree_result = join(root, v);
        // handles lists and base case
        Type base_result = Arrays.stream(v)
                .reduce(v[0], (x, y) -> {
                    if (x == null || y == null)
                        throw new IndexOutOfBoundsException("Join passed Null type in array");
                    if (LTE(x, y)) {
                        return y;
                    }
                    if (LTE(y, x)) {
                        return x;
                    }
                    return Type.OBJECT_TYPE;
                });
        // base result is at least as specific as tree result in these cases.
        // combined they are comprehensive.
        return (tree_result == null || tree_result.equals(Type.OBJECT_TYPE)) ?
                base_result : tree_result;
    }

    private Type join(TCNode node, Type... v) {
        Type deeper_result = node.children.stream()
                .map(x -> this.join(x, v))
                .filter(x -> x != null)
                .findAny()
                .orElse(null);
        if (deeper_result != null)
            return deeper_result;

        for (Type t : v) {
            // if this is triggered, current item isn't a superclass of all, hence return null
            // so caller defaults to base case
            if (!LTE(t, node.item)) {
                return null;
            }
        }
        return node.item;
    }

    public boolean LTE(Type a, Type b) {

        if (a == null || b == null) {
            throw new IndexOutOfBoundsException("LTE was passed null values");
        }
        if (a.equals(b))
            return true;
        if ((a.isSpecialType() || a.isListType()) && b.equals(Type.OBJECT_TYPE))
            return true;
        if (a.isSpecialType() && b.isSpecialType())
            return false;

        if (a.equals(Type.NONE_TYPE))
            return !b.isSpecialType();
        if (b.isListType() && a.equals(Type.EMPTY_TYPE)) {
            return true;
        }

        if (b.isListType() && a.isListType()) {
            Type a_inner_type = ((ListValueType) a).elementType;
            Type b_inner_type = ((ListValueType) b).elementType;
            return a_inner_type.equals(Type.NONE_TYPE) && this.LTE(a_inner_type, b_inner_type);
        }

        TCNode b_node = root.findDescendant(b);
        if (b_node == null) {
            return false;
        }
        return b_node.GET(a);
    }

    public void addType(Type parentType, Type childType) {
        TCNode parent_node = root.findDescendant(parentType);
        if (parent_node == null) {
            throw new IndexOutOfBoundsException("TypeComparer could not find parent item when adding type");
        }
        if (childType == null) {
            throw new IndexOutOfBoundsException("Null Child Type was passed into TypeComparer");
        }
        parent_node.MakeChild(childType);
    }


    private class TCNode {
        Type item;
        ArrayList<TCNode> children;

        public TCNode(Type v) {
            item = v;
            children = new ArrayList<>();
        }

        public TCNode MakeChild(Type v) {
            TCNode child = new TCNode(v);
            this.children.add(child);
            return child;
        }

        public TCNode findDescendant(Type T) {
            if (this.item.equals(T)) {
                return this;
            }
            return this.children
                    .stream()
                    .map(x -> x.findDescendant(T))
                    .filter(x -> x != null)
                    .findAny()
                    .orElse(null);
        }

        // if parameter is subtype
        public boolean GET(Type type) {
            if (this.item.equals(type)) {
                return true;
            }
            return this.children
                    .stream()
                    .anyMatch(x -> x.GET(type));
        }

        //returns parent node
        public Type parent(Type t) {
            if (this.item.equals(t)) {
                return null;
            }
            Boolean result = this.children.stream()
                    .anyMatch(p -> p.item.equals(t));
            return result ? this.item : this.children.stream()
                    .map(c -> c.parent(t))
                    .filter((c -> c != null))
                    .findFirst().orElse(null);
        }
    }
}
