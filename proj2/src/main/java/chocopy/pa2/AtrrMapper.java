package chocopy.pa2;

import chocopy.common.analysis.types.ClassValueType;
import chocopy.common.analysis.types.FuncType;
import chocopy.common.analysis.types.Type;
import chocopy.common.analysis.types.ValueType;
import chocopy.pa2.Types.MetaType;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Analogous to M(C,t) object in Chocopy Spec
 */
public class AtrrMapper {
    private final HashMap<ClassValueType, SymbolTable2<Type>> M_table = new HashMap<>();

    public AtrrMapper() {
        // Initilze with map for Type.ObjectType, which contains just the __init__ method
        SymbolTable2<Type> o_j_type_map = new SymbolTable2<>();

        ArrayList<ValueType> params = new ArrayList<>();
        params.add(Type.OBJECT_TYPE);
        FuncType init_method = new FuncType(params, Type.NONE_TYPE);
        o_j_type_map.put("__init__", init_method);
        put(Type.OBJECT_TYPE, o_j_type_map);
    }

    public SymbolTable2<Type> get(ClassValueType cvt) {
        // better type safety than hashmap, which accepts objects
        return cvt == null ? null : M_table.get(cvt);
    }

    public SymbolTable2<Type> get(MetaType m) {
        // better type safety than hashmap, which accepts objects
        return m == null ? null : get(m.getType());
    }

    public Type get(MetaType m, String s) {
        SymbolTable2<Type> temp = get(m.getType());
        return temp == null ? null : temp.get(s);
    }

    public Type get(ClassValueType cvt, String s) {
        SymbolTable2<Type> temp = get(cvt);
        return temp == null ? null : temp.get(s);
    }

    public void put(ClassValueType c, SymbolTable2<Type> s) {
        M_table.put(c, s);
    }

    public void put(MetaType c, SymbolTable2<Type> s) {
        M_table.put(c.getType(), s);
    }

}
