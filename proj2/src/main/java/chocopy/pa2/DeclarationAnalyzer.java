package chocopy.pa2;

import chocopy.common.analysis.AbstractNodeAnalyzer;
import chocopy.common.analysis.types.FuncType;
import chocopy.common.analysis.types.Type;
import chocopy.common.analysis.types.ValueType;
import chocopy.common.astnodes.*;
import chocopy.pa2.Types.MetaType;

import java.util.ArrayList;
import java.util.List;

/**
 * Analyzes declarations to create a top-level symbol table.
 */
public class DeclarationAnalyzer extends AbstractNodeAnalyzer<Type> {

    /**
     * Receiver for semantic error messages.
     */
    private final Errors errors;
    // ^ constructed with OBJECT_TYPE as base type
    /**
     * TypeComparer data structure, keeps track of relations between included
     * and user defined types.
     */
    private final TypeComparer TC = new TypeComparer(Type.OBJECT_TYPE);
    /**
     * M(C,t) in documentation Chocopy Definition
     * takes Class Types to methods and fields of the class
     */
    private final AtrrMapper MMap = new AtrrMapper();
    /**
     * Current symbol table.  Changes with new declarative region.
     */
    private SymbolTable2<Type> sym = new SymbolTable2<>();
    /**
     * Global symbol table.
     */
    private final SymbolTable2<Type> globals = sym;
    private State in_state = State.normal;

    private MetaType enclosingClass;

    /**
     * A new declaration analyzer sending errors to ERRORS0.
     */
    public DeclarationAnalyzer(Errors errors0) {
        errors = errors0;
        ArrayList<ValueType> temp = new ArrayList<>();
        ArrayList<ValueType> empty = new ArrayList<>();
        temp.add(Type.OBJECT_TYPE);
        FuncType print = new FuncType(temp, Type.NONE_TYPE);
        FuncType len = new FuncType(temp, Type.INT_TYPE);
        FuncType input = new FuncType(empty, Type.STR_TYPE);
        this.globals.put("object", MetaType.META_OBJ);
        this.globals.put("<None>", MetaType.META_NONE);
        this.globals.put("int", MetaType.META_INT);
        this.globals.put("str", MetaType.META_STR);
        this.globals.put("bool", MetaType.META_BOOL);
        this.globals.put("print", print);
        this.globals.put("len", len);
        this.globals.put("input", input);
    }

    /**
     * Class Declarations differ slightly from
     * others in how we treat them. For this, we keep a global state.
     * <p>
     * This method is for setting that state S temporarily to run "runnable" f, which returns nothing
     * It ensures that we don't  drop the old state
     *
     * @param s state to run r in
     * @param f lambda  function () -> () to run
     */
    private void runWithState(State s, Runnable f) {
        State old = this.in_state;
        this.in_state = s;
        f.run();
        this.in_state = old;
    }

    /**
     * Runs function f if we are in state s.
     * prettier than if statement.
     *
     * @param s
     * @param f
     */
    private void runIfState(State s, Runnable f) {
        if (this.in_state.equals(s)) {
            f.run();
        }
    }

    /**
     * Runs param f setting enviorment to the enviroment/symtable to a new symtable
     *
     * @param f      what to run
     * @param parent parent symtable used in construction of new one
     * @return
     */
    private void runWithNewSymTable(SymbolTable2<Type> parent, Runnable f) {
        SymbolTable2<Type> old = this.sym;
        this.sym = new SymbolTable2(parent);
        f.run();
        this.sym = old;
    }

    public SymbolTable2<Type> getSym() {
        return sym;
    }

    public SymbolTable2<Type> getGlobals() {
        return globals;
    }

    public AtrrMapper getMMap() {
        return MMap;
    }

    public TypeComparer getTC() {
        return this.TC;
    }

    @Override
    public Type analyze(Program program) {
        checkDecls(getGlobals(), program.declarations);
        return null;
    }

    private void checkDecls(SymbolTable2<Type> sym, List<Declaration> ld) {
        for (int i = 0; i < ld.size(); i++) {
            if (ld.get(i) instanceof FuncDef) {
                // We store the Symbol table for funcdef's scope  in it.
                // however, we cannot modify the original FuncDef structure since the reference
                // Lexer/parser uses the old one. Hence we extend it,
                // The only difference is a field for the symbol table.
                chocopy.pa2.Modified.FuncDef replacement =
                        new chocopy.pa2.Modified.FuncDef((FuncDef) ld.get(i), new SymbolTable2<>());
                ld.set(i, replacement);
            }
            Declaration dec = ld.get(i);
            Identifier id = dec.getIdentifier();
            String name = id.name;

            Type type = dec.dispatch(this);
            if (type == null) {
                continue;
            }
            if (sym.declares(name)) {
                errors.semError(id,
                        "Duplicate declaration of identifier in same "
                                + "scope: %s",
                        name);
            } else {
                sym.put(name, type);
            }
        }
    }

    @Override
    public Type analyze(NonLocalDecl nl) {
        if (sym.declares(nl.variable.name)) {
            errors.semError(nl.variable,
                    "Duplicate declaration of identifier in same scope: %s",
                    nl.variable.name);
        }
        // we possibly don't know this for now, will fill in in typecomparer.
        sym.put(nl.variable.name, null);
        return null;
    }

    @Override
    public Type analyze(VarDef varDef) {
        SymbolTable2<Type> old = this.sym;
        if (this.in_state.equals(State.inClass)) {
            this.sym = globals;
        }
        this.sym = old;

        return ValueType.annotationToValueType(varDef.var.type);
    }

    @Override
    public Type analyze(FuncDef fd) {
        // extension of funcdef which has symtable, since we can't modify funcdef
        chocopy.pa2.Modified.FuncDef fdr = (chocopy.pa2.Modified.FuncDef) fd;

        // we prepare create a new table which inherits from parent_table, depending on
        // if we are in a class or not. We store the old one to restore at end of call.
        // Due to scoping, it depends on if we are in a class or not.
        // The parent table is hidden if we are in a class
        // e.g. self.x is required, we can't just write "x" for attributes of classes
        SymbolTable2<Type> parent_table = this.in_state.equals(State.inClass)
                ? globals
                : sym;

        ArrayList<ValueType> paramTypes = new ArrayList<>();
        ValueType rtnType = ValueType.annotationToValueType(fd.returnType);

        runWithNewSymTable(parent_table, () -> {
            // Store function symtable in  node for use in TypeChecker
            fdr.sym2 = sym;

            for (TypedVar tv : fd.params) {
                // check shadows. They are no longer name spaced, so we run in normal mode no matter what
                // this is a total hack, but is due to the reference implementation bug that we need to conform with
                // https://piazza.com/class/ke3tewrxmn57lj?cid=199
                overrideHack(tv, fd);

                if (sym.declares(tv.identifier.name)) {
                    dupError(tv.identifier);
                }
                ValueType v = ValueType.annotationToValueType(tv.type);
                sym.put(tv.identifier.name, v);
                paramTypes.add(v);
            }
            // Checks which only apply to methods
            runIfState(State.inClass, () -> classChecks(fd, paramTypes, rtnType));
            // inner declarations (including parameters) can shadow, so we the normal contextuse
            runWithState(State.normal, () -> {
                checkDecls(fdr.sym2, fd.declarations);
            });
        });

        return new FuncType(paramTypes, rtnType);
    }

    private MetaType verifyParent(ClassDef cd) {
        Type parent = this.sym.get(cd.superClass.name);
        if (parent == null) {
            errors.semError(cd.superClass, "Super-class not defined: %s", cd.superClass.name);
            return MetaType.META_OBJ;
        }
        if (!(parent instanceof MetaType)) {
            errors.semError(cd.superClass, "Super-class must be a class: %s", cd.superClass.name);
            return MetaType.META_OBJ;
        }
        if (parent.isSpecialType()) {
            errors.semError(cd.superClass, "Cannot extend special class: %s", cd.superClass.name);
            return MetaType.META_OBJ;
        }
        return (MetaType) parent;
    }

    @Override
    public Type analyze(ClassDef cd) {
        final MetaType parent = verifyParent(cd);

        // type of types.
        MetaType newType = new MetaType(cd.name.name, parent);
        // We now are in a class, sub-declarations need to know.
        this.enclosingClass = newType;

        if (this.sym.get(cd.name.name) != null) {
            dupError(cd.name);
        } else {
            this.TC.addType(parent.getType(), newType.getType());
            this.sym.put(cd.name.name, newType);
        }
        String name;
        for (Declaration d : cd.declarations) {
            name = d.getIdentifier().name;
            Type parent_instance = MMap.get(enclosingClass.getParentClass(), name);
            if (parent_instance != null && !(d instanceof FuncDef && parent_instance.isFuncType())) {
                errors.semError(d.getIdentifier(), "Cannot re-define attribute: %s", name);
            }
        }
        runWithState(State.inClass, () -> {
            // Use parent symtable to inherit  old types from parent class
            runWithNewSymTable(this.MMap.get(parent), () -> {
                // Record new symtable
                this.MMap.put(newType, this.sym);
                // Sub declarations
                this.checkDecls(this.sym, cd.declarations);
                this.enclosingClass = null;
            });
        });
        return null;
    }

    public void dupError(Identifier id) {
        errors.semError(id,
                "Duplicate declaration of identifier in same scope: %s",
                id.name
        );
    }

    private void classChecks(FuncDef fd, List<ValueType> paramTypes, ValueType rtnType) {
        MetaType parent_class = enclosingClass.getParentType();
        Type old_def = MMap.get(parent_class, fd.name.name);

        if (old_def != null && old_def.isFuncType()) {
            FuncType f_old_def = (FuncType) old_def;
            // Check that Type Signatures are congruent
            if (!isGoodFuncOverride(f_old_def, paramTypes, rtnType)) {
                errors.semError(fd.name,
                        "Method overridden with different type signature: %s",
                        fd.name.name);
            }
        }
        // Ensure first signature agrees with class.
        if (paramTypes.size() < 1 || !(paramTypes.get(0).equals(enclosingClass.getType())))
            errors.semError(fd.name,
                    "First parameter of the following method must be of the enclosing class: %s",
                    fd.name.name);
    }

    private boolean isGoodFuncOverride(FuncType f_old_def, List<ValueType> paramTypes, ValueType newRtnType) {
        if (f_old_def.parameters.size() != paramTypes.size() || !f_old_def.returnType.equals(newRtnType)) {
            return false;
        } else {
            //  i =1 -> skip first, as it may vary with class
            for (int i = 1; i < paramTypes.size(); i++) {

                if (!paramTypes.get(i).equals(f_old_def.parameters.get(i))) {
                    return false;
                }
            }
        }
        return true;
    }

    // see use in funcdef
    private void overrideHack(TypedVar tv, FuncDef fd) {
        if (tv.hasError())
            errors.semError(fd.name,
                    "Method overridden with different type signature: %s",
                    fd.name.name);
    }


    @Override
    public Type analyze(GlobalDecl gd) {
        if (sym.declares(gd.variable.name)) {
            errors.semError(gd.variable,
                    "Duplicate declaration of identifier in same scope: %s",
                    gd.variable.name);
        }
        // we possibly don't know this for now, will fill in in typecomparer.
        sym.put(gd.variable.name, null);
        return null;
    }


    private enum State {
        normal,
        inClass
    }
}

