package chocopy.pa2;

import chocopy.common.analysis.AbstractNodeAnalyzer;
import chocopy.common.analysis.types.Type;
import chocopy.common.astnodes.*;
import chocopy.pa2.Modified.FuncDef;

import java.util.List;

public class ShadowChecker extends AbstractNodeAnalyzer<Type> {

    TypeComparer TC;
    private final Errors errors;
    private final SymbolTable2 sym;

    public ShadowChecker(TypeComparer TC, Errors errors0, SymbolTable2 sym) {
        this.errors = errors0;
        this.TC = TC;
        this.sym = sym;
    }

    @Override
    public Type analyze(Program program) {
        for (Declaration decl : program.declarations) {
            if (decl instanceof chocopy.pa2.Modified.FuncDef) {
                shadowCheck((chocopy.pa2.Modified.FuncDef) decl);
            } else if (decl instanceof ClassDef) {
                shadowCheck((ClassDef) decl);
            }

        }
        return null;
    }

    private void shadowCheck(ClassDef cd) {

        for (Declaration declaration : cd.declarations) {
            if (declaration instanceof chocopy.pa2.Modified.FuncDef && ((FuncDef) declaration).name.name.equals("__init__")) {
                for (TypedVar param : ((FuncDef) declaration).params) {
                    if (this.sym.isShadowingClass(param.identifier)) {
                        errors.semError(((FuncDef) declaration).name, "Method overridden with different type signature: %s", declaration.getIdentifier().name);
                    }
                }
                shadowCheck((chocopy.pa2.Modified.FuncDef) declaration);

            }
        }
        //shadowCheck(cd.declarations);
    }

    private void shadowCheck(chocopy.pa2.Modified.FuncDef fd) {
        checkIdentifier(fd.getIdentifier());
        for (TypedVar param : fd.params) {
            checkIdentifier(param.identifier);
        }
        shadowCheck(fd.declarations);
    }

    private void shadowCheck(List<Declaration> declarations) {
        for (Declaration declaration : declarations) {
            checkIdentifier(declaration.getIdentifier());
            if (declaration instanceof chocopy.pa2.Modified.FuncDef) {
                shadowCheck((chocopy.pa2.Modified.FuncDef) declaration);
            }
        }
    }

    private void checkIdentifier(Identifier identifier) {
        if (this.sym.isShadowingClass(identifier)) {
            errors.semError(identifier, "Cannot shadow class name: %s", identifier.name);
        }
    }


}