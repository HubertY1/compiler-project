package chocopy.pa2;

import chocopy.common.analysis.AbstractNodeAnalyzer;
import chocopy.common.analysis.types.*;
import chocopy.common.astnodes.*;
import chocopy.pa2.Types.MetaType;

import java.util.Arrays;
import java.util.List;

import static chocopy.common.analysis.types.Type.*;

/**
 * Analyzer that performs ChocoPy type checks on all nodes.  Applied after
 * collecting declarations.
 */
public class TypeChecker extends AbstractNodeAnalyzer<Type> {

    /**
     * The current symbol table (changes depending on the function
     * being analyzed).
     */

    private SymbolTable2<Type> sym;
    private final SymbolTable2<Type> globals;
    /**
     * Collector for errors.
     */
    private final Errors errors;

    private final TypeComparer TC;
    /**
     * Map to types of attributes of methods,
     * analagous to M(C,a) in chocopy spec
     */
    private final AtrrMapper MMap;


    /**
     * Creates a type checker using GLOBALSYMBOLS for the initial global
     * symbol table and ERRORS0 to receive semantic errors.
     */
    public TypeChecker(SymbolTable2<Type> globalSymbols, TypeComparer TC, AtrrMapper MMap, Errors errors0) {
        this.sym = globalSymbols;
        this.globals = globalSymbols;
        this.errors = errors0;
        this.MMap = MMap;
        this.TC = TC;
    }

    /**
     * Inserts an error message in NODE if there isn't one already.
     * The message is constructed with MESSAGE and ARGS as for
     * String.format.
     */
    private void err(Node node, String message, Object... args) {
        errors.semError(node, message, args);
    }


    @Override
    public Type analyze(Program program) {
        dispatchAll(program.declarations);
        dispatchAll(program.statements);
        return null;
    }

    @Override
    public Type analyze(ExprStmt s) {
        s.expr.dispatch(this);
        return null;
    }


    @Override
    public Type analyze(MethodCallExpr c) {
        Type[] f_param_types = dispatchToTypeList(c.args);

        int received_params = f_param_types.length;

        Type obj_type = c.method.object.dispatch(this);
        Type known_type = MMap.get((ClassValueType) obj_type, c.method.member.name);
        c.method.setInferredType(known_type);
        if (known_type == null || !known_type.isFuncType()) {
            err(c, "There is no method named `%s` in class `%s`",
                    c.method.member.name,
                    c.method.object.getInferredType()
            );
            return c.setInferredType(OBJECT_TYPE);
        }

        FuncType known_f_type = (FuncType) known_type;
        int actual_param_nums = known_f_type.parameters.size() - 1;
        List<ValueType> expected_param_types = known_f_type.parameters;
        Type returnType = known_f_type.returnType;

        if (received_params != actual_param_nums) {
            err(c, "Expected %s arguments; got %s", actual_param_nums, received_params);
            return c.setInferredType(returnType);
        }
        for (int i = 0; i < received_params; i++) {
            Type actual_type = f_param_types[i];
            if (!TC.LTE(actual_type, expected_param_types.get(i + 1))) {
                err(c, "Expected type `%s`; got type `%s` in parameter %d",
                        expected_param_types.get(i + 1), actual_type, i + 1);
                break;
            }
        }
        return c.setInferredType(returnType);
    }

    @Override
    public Type analyze(CallExpr c) {
        Type[] f_param_types = dispatchToTypeList(c.args);

        int received_params = f_param_types.length;
        int actual_param_nums;
        Type inferred_type;
        Type[] expected_param_types;
        Type returnType;

        if (this.TC.containsType(c.function.name)) {
            actual_param_nums = 0;
            expected_param_types = new Type[]{};
            returnType = new ClassValueType(c.function.name);
        } else {
            Type f_type = sym.get(c.function.name);

            if (f_type == null || !f_type.isFuncType()) {
                err(c, "Not a function or class: %s", c.function.name);
                return c.setInferredType(NONE_TYPE);
            }

            c.function.setInferredType(f_type);
            FuncType ft_f_type = (FuncType) f_type;
            actual_param_nums = ft_f_type.parameters.size();
            expected_param_types = ft_f_type.parameters.toArray(new Type[0]);
            returnType = ft_f_type.returnType;
        }
        if (received_params != actual_param_nums) {
            err(c, "Expected %s arguments; got %s", actual_param_nums, received_params);
            return c.setInferredType(returnType);
        }
        for (int i = 0; i < received_params; i++) {
            Type actual_type = f_param_types[i];
            if (!TC.LTE(actual_type, expected_param_types[i])) {
                err(c, "Expected type `%s`; got type `%s` in parameter %d",
                        expected_param_types[i], actual_type, i);
                break;
            }
        }
        return c.setInferredType(returnType);
    }

    @Override
    public Type analyze(IntegerLiteral i) {
        return i.setInferredType(Type.INT_TYPE);
    }

    @Override
    public Type analyze(GlobalDecl gd) {
        Type result = globals.get(gd.variable.name);
        if (null == result || !result.isValueType()) {
            errors.semError(gd.variable,
                    "Not a global variable: %s",
                    gd.variable.name);
            return null;
        }
        sym.put(gd.variable.name, result);
        return result;
    }

    @Override
    public Type analyze(NonLocalDecl nl) {
        Type result = this.sym.nonlocal_get(nl.variable.name);
        if (null == result || !result.isValueType() || result instanceof MetaType) {
            errors.semError(nl.variable,
                    "Not a nonlocal variable: %s",
                    nl.variable.name);
            return null;
        }
        sym.put(nl.variable.name, result);
        return result;
    }


    @Override
    public Type analyze(ListExpr l) {
        // trivial case is different, don't want to box a second time
        if (l.elements.size() == 0)
            return l.setInferredType(EMPTY_TYPE);

        Type[] g = dispatchToTypeList(l.elements);
        return l.setInferredType(new ListValueType(TC.join(g)));
        // TC.join gives least upper bound
    }

    @Override
    public Type analyze(StringLiteral s) {
        return s.setInferredType(Type.STR_TYPE);
    }

    @Override
    public Type analyze(NoneLiteral n) {
        return n.setInferredType(NONE_TYPE);
    }

    @Override
    public Type analyze(BooleanLiteral b) {
        return b.setInferredType(BOOL_TYPE);
    }


    // gets type annotations of "TypeAnnotation" in AST. We do not want to set the inferred type, though
    @Override
    public Type analyze(ClassType ct) {
        Type res = sym.get(ct.className);
        if (res == null || !(res instanceof MetaType)) {
            errors.semError(ct, "Invalid type annotation; there is no class named: %s", ct.className);
        }
        return new ClassValueType(ct.className);
    }

    // gets type annotations of "TypeAnnotation" in AST. We do not want to set the inferred type, though
    @Override
    public Type analyze(ListType lt) {
        return new ListValueType(lt.elementType.dispatch(this));
    }


    @Override
    public Type analyze(ClassDef cd) {
        runWithNewSymTable(this.sym, () -> {
            dispatchAll(cd.declarations);
        });
        return null;
    }

    @Override
    public Type analyze(MemberExpr me) {
        Type obj_type = me.object.dispatch(this);
        if (!(obj_type instanceof ClassValueType)) {
            err(me, "Cannot access member of non-class type `%s`", obj_type);
            return me.setInferredType(obj_type);
        }

        Type result = MMap.get((ClassValueType) obj_type, me.member.name);
        if (result == null || result instanceof FuncType) {
            err(me, "There is no attribute named `%s` in class `%s`",
                    me.member.name,
                    obj_type.className()
            );
            return me.setInferredType(obj_type);
        }
        return me.setInferredType(result);
    }

    @Override
    public Type analyze(FuncDef fd) {
        chocopy.pa2.Modified.FuncDef fd2 = (chocopy.pa2.Modified.FuncDef) fd;

        fd2.params.stream().forEach(t -> t.type.dispatch(this));
        fd.returnType.dispatch(this);

        runWithSymTable(fd2.sym2, () -> {
            dispatchAll(fd2.declarations);
            dispatchAll(fd2.statements);
        });

        return null;
    }

    @Override
    public Type analyze(TypedVar t) {
        return t.type.dispatch(this);
    }


   @Override
    public Type analyze(VarDef v) {
        Type rvalue_type = v.value.dispatch(this);
        Type left = v.var.dispatch(this);

        if (!TC.LTE(rvalue_type, left)) {
            err(v, "Expected type `%s`; got type `%s`", left, rvalue_type);
        }
        return null;
    }

    @Override
    public Type analyze(AssignStmt astmt) {
        List<Expr> targets = astmt.targets;
        Type right = astmt.value.dispatch(this);

        boolean silent_error = false;
        for (Expr e : targets) {
            Type left = e.dispatch(this);
            if (e instanceof Identifier) {
                Identifier IDe = (Identifier) e;
                if (!sym.declares(IDe.name) ) {
                    err(IDe, "Cannot assign to variable that is not explicitly declared in this scope: %s", IDe.name);
                }
            }
            if (e instanceof IndexExpr && STR_TYPE.equals(left) && !silent_error) {
                err(e, "`str` is not a list type");
                silent_error = true;
            }
            if (!TC.LTE(right, left) && !silent_error) {
                err(astmt, "Expected type `%s`; got type `%s`", left, right);
                silent_error = true;
                //  break;
            }
        }
        if (targets.size() > 1 && right.isListType() && NONE_TYPE.equals(right.elementType())) {
            err(astmt, "Right-hand side of multiple assignment may not be [<None>]");
        }
        return null;
    }

    /**
     * Helper function for binary operators
     * that verifies both parameters are one of the expected types.
     * if true, then it sets and returns the valid type.
     * <p>
     * Else, we error, and return null --unless only one type was provided;
     * then it will be inferred and returned
     *
     * @param e        BinaryExpr to check
     * @param expected a number of valid types
     * @return the valid type, or null;
     */
    private Type checkBinOperatorTypes(BinaryExpr e, Type expected) {
        Type[] T = dispatchToTypeList(e.left, e.right);

        if (!(bothEqual(T[0], T[1], expected)))
            badOperatorError(e, T[0], T[1]);

        return e.setInferredType(expected);
    }

    private void badOperatorError(BinaryExpr e, Type T1, Type T2) {
        err(e, "Cannot apply operator `%s` on types `%s` and `%s`", e.operator, T1, T2);
    }

    /**
     * Helper function to check if compare expression is valid
     * if so, we error and return false
     *
     * @param c        Compare expression to report error on
     * @param left     type on left side  of operator
     * @param right    type on right side  of operator
     * @param operator string operator for error.
     * @param valid    variadic Type / list of valid types both operators can be
     */
    private boolean checkCompOperator(CompareExpr c, Type left, Type right, String operator, Type... valid) {
        for (Type t : valid) {
            if (bothEqual(left, right, t)) {
                return true;
            }
        }
        err(c, "Cannot apply operator `%s` on types `%s` and `%s`", operator, left, right);
        return true;
    }

    @Override
    public Type analyze(BinaryExpr e) {
        Type t1 = e.left.dispatch(this);
        Type t2 = e.right.dispatch(this);

        switch (e.operator) {
            case "or":
            case "and":
                return checkBinOperatorTypes(e, BOOL_TYPE);

            case "-":
            case "*":
            case "//":
            case "%":
                return checkBinOperatorTypes(e, INT_TYPE);

            case "+":
                if (bothEqual(t1, t2, INT_TYPE) || bothEqual(t1, t2, STR_TYPE))
                    return e.setInferredType(e.left.getInferredType());

                if (t1.isListType() && t2.isListType())  // Join is least upper bound
                    return e.setInferredType(new ListValueType(TC.join(t1.elementType(), t2.elementType())));

                badOperatorError(e, t1, t2);
                if (INT_TYPE.equals(t1) || INT_TYPE.equals(t2))
                    return e.setInferredType(INT_TYPE);
            default:
                return e.setInferredType(OBJECT_TYPE);
        }
    }

    @Override
    public Type analyze(CompareExpr e) {
        Type[] operandTypes = dispatchToTypeList(e.operands);
        //Iterating through each comparator
        for (int opPos = 0; opPos < e.operators.size(); opPos++) {
            Type left = operandTypes[opPos];
            Type right = operandTypes[opPos + 1];
            String operator = e.operators.get(opPos);

            switch (e.operators.get(opPos)) {
                case "<":
                case "<=":
                case ">":
                case ">=":
                    if (checkCompOperator(e, left, right, operator, INT_TYPE)) continue;
                    break;
                case "==":
                case "!=":
                    if (checkCompOperator(e, left, right, operator, INT_TYPE, BOOL_TYPE)) continue;
                    break;
                case "is":
                    if (!left.isSpecialType() && !right.isSpecialType()) continue;
                    break;
            }
            err(e, "Cannot apply operator `%s` on types `%s` and `%s`", operator, left, right);
            break;
        }
        return e.setInferredType(BOOL_TYPE);
    }

    @Override
    public Type analyze(IfExpr expr) {
        Type cond_type = expr.condition.dispatch(this);
        Type[] exec_types = dispatchToTypeList(expr.thenExpr, expr.elseExpr);

        if (!BOOL_TYPE.equals(cond_type)) {
            err(expr, "Condition expression cannot be of type `%s`", cond_type);
        }

        return expr.setInferredType(TC.join(exec_types));
        // gives LUB of types
    }

    @Override
    public Type analyze(WhileStmt wStmt) {
        Type cond = wStmt.condition.dispatch(this);
        if (!BOOL_TYPE.equals(cond))
            err(wStmt, "Condition expression cannot be of type `%s`", cond);

        dispatchAll(wStmt.body);

        return null;
    }


    @Override
    public Type analyze(IfStmt iStmt) {
        Type cond = iStmt.condition.dispatch(this);
        if (!BOOL_TYPE.equals(cond)) {
            err(iStmt, "Condition expression cannot be of type `%s`", cond);
        }

        dispatchAll(iStmt.thenBody);
        dispatchAll(iStmt.elseBody);

        return null;
    }

    @Override
    public Type analyze(ForStmt fStmt) {
        Type iter = fStmt.iterable.dispatch(this);
        Type identifier = fStmt.identifier.dispatch(this);
        //This is the case where it's a list that we iterate through
        if (!sym.declares(fStmt.identifier.name)) {
            err(fStmt.identifier,
                    "Cannot assign to variable that is not explicitly declared in this scope: %s",
                    fStmt.identifier.name);
        }

        if (!iter.isListType() && !STR_TYPE.equals(iter))
            err(fStmt, "Cannot iterate over value of type `%s`", iter);

        if (iter.isListType() && !TC.LTE(iter.elementType(), identifier))
            err(fStmt, "Expected type `%s`; got type `%s`", identifier, iter.elementType());

        if (STR_TYPE.equals(iter) && !TC.LTE(STR_TYPE, identifier))
            err(fStmt, "Expected type `%s`; got type `%s`", identifier, STR_TYPE);

        dispatchAll(fStmt.body);

        return null;
    }

    @Override
    public Type analyze(Identifier id) {
        String varName = id.name;
        Type varType = sym.get(varName);
        if (varType instanceof ValueType) {
            return id.setInferredType(varType);
        }

        err(id, "Not a variable: %s", varName);
        return id.setInferredType(ValueType.OBJECT_TYPE);
    }

    @Override
    public Type analyze(UnaryExpr ue) {
        Type eInnerType = ue.operand.dispatch(this);
        switch (ue.operator) {
            case "not":
                if (eInnerType.equals(BOOL_TYPE))
                    return ue.setInferredType(BOOL_TYPE);
                break;
            case "-":
                if (eInnerType.equals(INT_TYPE))
                    return ue.setInferredType(INT_TYPE);
                break;
        }
        err(ue, "Cannot apply operator `%s` on type `%s`", ue.operator, eInnerType);
        return ue.setInferredType(OBJECT_TYPE);
    }


    @Override
    public Type analyze(IndexExpr e) {
        Type arrayType = e.list.dispatch(this);
        Type indexType = e.index.dispatch(this);

        if (!INT_TYPE.equals(indexType))
            err(e, "Index is of non-integer type `%s`", indexType);


        if (!arrayType.isListType() && !STR_TYPE.equals(arrayType))
            err(e, "Cannot index into type `%s`", arrayType);

        if (STR_TYPE.equals(arrayType))
            return e.setInferredType(STR_TYPE);

        return e.setInferredType(arrayType.elementType());
    }

    @Override
    public Type analyze(ReturnStmt returnStmt) {
        if (returnStmt.value == null) {
            return null;
        }
        return returnStmt.value.setInferredType(returnStmt.value.dispatch(this));
    }

    /**
     * Runs param f setting enviorment to the enviroment/symtable to a new symtable
     *
     * @param f      what to run
     * @param parent parent symtable used in construction of new one
     * @return
     */
    private void runWithNewSymTable(SymbolTable2<Type> parent, Runnable f) {
        SymbolTable2<Type> old = this.sym;
        this.sym = new SymbolTable2(parent);
        f.run();
        this.sym = old;
    }


    /**
     * Runs param f temporary setting enviorment to the enviroment/symtable  passed
     *
     * @param f lambda function ran
     * @param s
     */
    private void runWithSymTable(SymbolTable2<Type> s, Runnable f) {
        SymbolTable2<Type> old = this.sym;
        this.sym = s;
        f.run();
        this.sym = old;
    }

    private <T extends Node> void dispatchAll(List<T> items) {
        items.stream()
                .forEach(it -> it.dispatch(this));
    }

    private <T extends Node> void dispatchAll(T... items) {
        Arrays.stream(items)
                .forEach(it -> it.dispatch(this));
    }

    private boolean bothEqual(Type t1, Type t2, Type expected) {
        return t1.equals(expected) && t2.equals(expected);
    }


    private <T extends Node> Type[] dispatchToTypeList(List<T> ln) {
        return ln.stream()
                .map(n -> n.dispatch(this))
                .toArray(Type[]::new);
    }

    private <T extends Node> Type[] dispatchToTypeList(T... ln) {
        return Arrays.stream(ln)
                .map(n -> n.dispatch(this))
                .toArray(Type[]::new);
    }
}
