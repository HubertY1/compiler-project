package chocopy.pa2;

/**
 * we would often have long  if else cases in this in this project. polymorphism  isn't often viable,
 * as theres a lot of things we'd have to clone or extend to change.
 * Hence we implement a new helper, Exclusive case. eq is for equallity
 * <p>
 * <p>
 * ExclusiveCase(item)
 * .eq(f , - { do this }
 * .eq( else ,  t-> t{} hat
 */
public class Match {
    boolean done = false;

    public void eq(boolean b, Runnable r) {
        if (b && !done) {
            r.run();
            done = true;
        }
    }

}
