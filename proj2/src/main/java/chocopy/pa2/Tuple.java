package chocopy.pa2;

import java.util.function.Function;

public class Tuple<A, B> {
    public A i1;
    public B i2;

    Tuple(A first, B second) {
        i1 = first;
        i2 = second;
    }

    public <RA, RB> Tuple<RA, RB> map(Function<A, RA> f1, Function<B, RB> f2) {
        return new Tuple(f1.apply(this.i1), f2.apply(this.i2));
    }
}
