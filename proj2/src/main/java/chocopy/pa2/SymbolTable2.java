package chocopy.pa2;

import chocopy.common.astnodes.Identifier;
import chocopy.pa2.Types.MetaType;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A block-structured symbol table a mapping identifiers to information
 * about them of type T in a given declarative region.
 */
public class SymbolTable2<T> {

    /**
     * Contents of the current (innermost) region.
     */
    private final Map<String, T> tab = new HashMap<>();
    /**
     * Enclosing block.
     */
    private final SymbolTable2<T> parent;

    /**
     * A table representing a region nested in that represented by
     * PARENT0.
     */
    public SymbolTable2(SymbolTable2<T> parent0) {
        parent = parent0;
    }


    /**
     * A top-level symbol table.
     */
    public SymbolTable2() {
        this.parent = null;
    }

    /**
     * Returns the mapping of NAME in the innermost nested region
     * containing this one.
     */
    public T get(String name) {
        if (tab.containsKey(name)) {
            return tab.get(name);
        } else if (parent != null) {
            return parent.get(name);
        } else {
            return null;
        }
    }

    /**
     * Adds a new mapping of NAME -> VALUE to the current region, possibly
     * shadowing mappings in the enclosing parent. Returns modified table.
     */
    public SymbolTable2<T> put(String name, T value) {
        tab.put(name, value);
        return this;
    }

    public T nonlocal_get(String name) {
        if (this.parent == null) {
            return null;
        } else {
            return this.parent.nonlocal_get_inductive(name);
        }

    }

    private T nonlocal_get_inductive(String name) {
        T result;
        // this is the global case, hence we use null to signal failure (variable isn' nonlocal)
        if (this.parent == null)
            return null;
        if ((result = this.get(name)) != null)
            return result;
        return this.parent.nonlocal_get_inductive(name);
    }

    @Override
    public String toString() {
        return "self's hashmap :\n"
                + tab.toString() + "\n Parent: \n"
                + (parent == null ? "null" : parent.toString());
    }

    /**
     * Returns whether NAME has a mapping in this region (ignoring
     * enclosing regions.
     */
    public boolean declares(String name) {
        return tab.containsKey(name);
    }

    /**
     * Returns all the names declared this region (ignoring enclosing
     * regions).
     */
    public Set<String> getDeclaredSymbols() {
        return tab.keySet();
    }

    /**
     * Returns the parent, or null if this is the top level.
     */
    public SymbolTable2<T> getParent() {
        return this.parent;
    }

    public boolean isShadowingClass(Identifier i) {
        return this.tab.containsKey(i.name) && this.tab.get(i.name) instanceof MetaType;
    }

}
