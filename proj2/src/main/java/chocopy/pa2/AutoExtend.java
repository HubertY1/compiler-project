package chocopy.pa2;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class AutoExtend {

    //  @Cite  modified from https://stackoverflow.com/a/60751923
    public static <T, MT extends T> void ElevateType(T parent, MT target) {
        try {
            for (Field field : parent.getClass().getDeclaredFields()) {
                if (!Modifier.isStatic(field.getModifiers())) {
                    field.setAccessible(true);
                    field.set(target, field.get(parent));
                }
            }
        } catch (Exception e) {
            System.out.println("Failed to elevate type in autoextend from" + parent.getClass().getName() + " to "
                    + target.getClass().getName());
        }
    }
}
