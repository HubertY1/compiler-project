package chocopy.pa2.Modified;

import chocopy.common.analysis.types.Type;
import chocopy.pa2.AutoExtend;
import chocopy.pa2.SymbolTable2;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class FuncDef extends chocopy.common.astnodes.FuncDef {
    @JsonIgnore
    public SymbolTable2<Type> sym2;

    public FuncDef(chocopy.common.astnodes.FuncDef fd, SymbolTable2<Type> s) {
        super(null, null, null, null, null, null, null);
        this.setLocation(fd.getLocation());
        AutoExtend.ElevateType(fd, this);
        this.sym2 = s;
    }

}
