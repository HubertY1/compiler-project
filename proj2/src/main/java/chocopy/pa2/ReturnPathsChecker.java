package chocopy.pa2;

import chocopy.common.analysis.AbstractNodeAnalyzer;
import chocopy.common.analysis.types.Type;
import chocopy.common.analysis.types.ValueType;
import chocopy.common.astnodes.*;

import java.util.List;

public class ReturnPathsChecker extends AbstractNodeAnalyzer<Type> {

    TypeComparer TC;
    private final Errors errors;

    public ReturnPathsChecker(TypeComparer TC, Errors errors0) {
        this.errors = errors0;
        this.TC = TC;
    }

    @Override
    public Type analyze(Program program) {
        for (Stmt stmt : program.statements) {
            if (stmt instanceof ReturnStmt) {
                errors.semError(stmt, "Return statement cannot appear at the top level");
            }
        }
        for (Declaration decl : program.declarations) {
            if (decl instanceof chocopy.pa2.Modified.FuncDef) {
                returnCheck((chocopy.pa2.Modified.FuncDef) decl);
            } else {
                decl.dispatch(this);
            }
        }
        return null;
    }

    @Override
    public Type analyze(ClassDef cdc) {
        for (Declaration d : cdc.declarations) {
            if (d instanceof chocopy.pa2.Modified.FuncDef) {
                returnCheck((chocopy.pa2.Modified.FuncDef) d);
            }
        }
        return null;
    }

    private boolean returnCheck(chocopy.pa2.Modified.FuncDef fd) {
        for (Declaration decl : fd.declarations) {
            if (decl instanceof chocopy.pa2.Modified.FuncDef) {
                returnCheck((chocopy.pa2.Modified.FuncDef) decl);
            } else {
                decl.dispatch(this);
            }
        }
        return returnCheck(fd.statements, fd, false);
    }

    private boolean returnCheck(List<Stmt> statements, chocopy.pa2.Modified.FuncDef fd, boolean isConditionalBranch) {
        /*
        For a list of statements (presumably within a function definition or an if-statement),
        we correctly return something (or correctly do not) when at least one of:
            1. The statements themselves contain a return statement
            2. Both the 'then' and the 'else' branch of some conditional always returns
            3. Neither of these conditions are met, but the function doesn't have a return type
         */
        boolean containsReturn = false;

        Type functionReturnType = ValueType.annotationToValueType(fd.returnType);
        for (Stmt stmt : statements) {
            if (stmt instanceof ReturnStmt) {
                if (((ReturnStmt) stmt).value != null) {
                    Type rType = ((ReturnStmt) stmt).value.getInferredType();
                    rType = rType == null ? Type.NONE_TYPE : rType;

                    if (!TC.LTE(rType, functionReturnType)) {
                        errors.semError(stmt, "Expected type `%s`; got type `%s`", functionReturnType, rType);
                    }
                } else if (!TC.LTE(Type.NONE_TYPE, functionReturnType)) {
                    //This happens if our return statement doesn't have an expression but we are supposed to return something
                    errors.semError(stmt, "Expected type `%s`; got `None`", functionReturnType);
                }
                containsReturn = true;
            } else if (stmt instanceof IfStmt) {
                boolean thenReturns = returnCheck(((IfStmt) stmt).thenBody, fd, true);
                boolean elseReturns = returnCheck(((IfStmt) stmt).elseBody, fd, true);
                if (thenReturns && elseReturns) {
                    containsReturn = true;
                }
            }
        }

        if (!containsReturn && !TC.LTE(Type.NONE_TYPE, functionReturnType)) {
            if (!isConditionalBranch) { //We don't want to report an error on a conditional branch, because it might not be an issue
                errors.semError(fd.name, "All paths in this function/method must have a return statement: %s", fd.name.name);
            }
            return false;
        }
        return true;
    }
}