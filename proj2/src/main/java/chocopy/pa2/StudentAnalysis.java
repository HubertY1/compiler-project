package chocopy.pa2;

import chocopy.common.analysis.types.Type;
import chocopy.common.astnodes.Program;

/**
 * Top-level class for performing semantic analysis.
 */
public class StudentAnalysis {

    /**
     * Perform semantic analysis on PROGRAM, adding error messages and
     * type annotations. Provide debugging output iff DEBUG. Returns modified
     * tree.
     */
    public static Program process(Program program, boolean debug) {
        if (program.hasErrors()) {
            return program;
        }

        DeclarationAnalyzer declarationAnalyzer =
                new DeclarationAnalyzer(program.errors);
        program.dispatch(declarationAnalyzer);
        SymbolTable2<Type> globalSym =
                declarationAnalyzer.getGlobals();
        TypeComparer TC = declarationAnalyzer.getTC();
        AtrrMapper MMap = declarationAnalyzer.getMMap();

        TypeChecker typeChecker =
                new TypeChecker(globalSym, TC, MMap, program.errors);
        program.dispatch(typeChecker);

        ReturnPathsChecker returnPathsChecker =
                new ReturnPathsChecker(TC, program.errors);
        program.dispatch(returnPathsChecker);

        ShadowChecker shadowChecker =
                new ShadowChecker(TC, program.errors, globalSym);
        program.dispatch(shadowChecker);

        return program;
    }
}
