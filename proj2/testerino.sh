name=$1
astName="${name}.ast"
astTypedName="${name}.ast.typed"

echo $astName

java -cp 'chocopy-ref.jar:target/assignment.jar' chocopy.ChocoPy --pass=r $1 --out $astName
java -cp 'chocopy-ref.jar:target/assignment.jar' chocopy.ChocoPy --pass=rr $1 --out $astTypedName

java -cp "chocopy-ref.jar:target/assignment.jar" chocopy.ChocoPy --pass=.s $1 --test
