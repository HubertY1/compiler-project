x:[object] = None

x = ["hello", 1] #should pass
x = ["hello"] #should fail
x = [1, 2] #should fail
x = ["Hello", "world"] # should fail


